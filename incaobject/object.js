﻿new IncaOffline({
    emulate:{
        "inca": {
            "scriptSupport": {},
            "services": {
                "infoText": {},
                "report": {},
                "viewTemplate": {},
                "logging": {},
                "documentation": {
                    "registerDocumentation": {},
                    "formDocumentation": {},
                    "registerVariableDocumentation": {}
                }
            },
            "user": {
                "id": 8985,
                "firstName": "Mats",
                "lastName": "Bernerfalk",
                "username": "oc2bemat",
                "email": "mats-olov.bernerfalk@akademiska.se",
                "phoneNumber": "070-3190137",
                "forcedRestrictions": [],
                "role": {
                    "id": 43,
                    "name": "Inrapportör (Patientöversikt)",
                    "isReviewer": false
                },
                "position": {
                    "id": 1288,
                    "name": "Demo Onkologen",
                    "code": "741",
                    "fullNameWithCode": "OC Demo (0) - KS Solna (01100) - Demo Onkologen (741)"
                },
                "region": {
                    "id": 7,
                    "name": "Region Demo"
                },
                "previousLogin": "2017-10-24T11:36:11.617Z"
            },
            "serverDate": "2018-06-01T13:26:56.817Z",
            "form": {
                "metadata": {
                    "Uppföljning": {
                        "subTables": [
                            "Bilddiagnostik",
                            "Labbprov",
                            "Besök",
                            "SRE",
                            "Läkemedel",
                            "Strålbehandling",
                            "Bidragande",
                            "Biobank"
                        ],
                        "regvars": {
                            "U_datum": {
                                "term": 1002,
                                "description": "Lagrar senaste datum då registret uppdaterades",
                                "technicalName": "T1123082",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "U_crpcdatum": {
                                "term": 1002,
                                "description": "Datum för kastrationsresistent sjukdom",
                                "technicalName": "T1166816",
                                "sortOrder": 0,
                                "label": "Datum för kastrationsresistent sjukdom",
                                "compareDescription": "Datum för kastrationsresistent sjukdom",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "U_metastasdatum": {
                                "term": 1002,
                                "description": "1:a metastas datum",
                                "technicalName": "T1166818",
                                "sortOrder": 0,
                                "label": "1:a metastas datum",
                                "compareDescription": "1:a metastas datum",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "U_hormonstartdatum": {
                                "term": 1002,
                                "description": "Datum för start av primär hormonbehandling",
                                "technicalName": "T1166820",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "U_psadatum": {
                                "term": 1002,
                                "description": "Datum för PSA recidiv",
                                "technicalName": "T1166817",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "U_pallHemSjkvrd": {
                                "term": 1002,
                                "description": "Datum för palliativ hemsjukvård",
                                "technicalName": "T1312662",
                                "sortOrder": 0,
                                "label": "Datum för palliativ hemsjukvård",
                                "compareDescription": "Datum för palliativ hemsjukvård",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "U_brytpunktssamtalsdatum": {
                                "term": 1002,
                                "description": "Datum för brytpunktssamtal",
                                "technicalName": "T1782001",
                                "sortOrder": 0,
                                "label": "Datum för brytpunktssamtal",
                                "compareDescription": "Datum för brytpunktssamtal",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "U_AMslutdatum": {
                                "term": 1002,
                                "description": "Datum för stopp av Aktiv monitorering",
                                "technicalName": "T1782946",
                                "sortOrder": 0,
                                "label": "Datum för stopp av Aktiv monitorering",
                                "compareDescription": "Datum för stopp av Aktiv monitorering",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "U_AMstartdatum": {
                                "term": 1002,
                                "description": "Datum för start av Aktiv monitorering",
                                "technicalName": "T1782947",
                                "sortOrder": 0,
                                "label": "Datum för start av Aktiv monitorering",
                                "compareDescription": "Datum för start av Aktiv monitorering",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "U_EnkatVD": {
                                "term": 7323,
                                "description": "",
                                "technicalName": "T1782957",
                                "sortOrder": 0,
                                "label": "ProstataProcess_Enkat",
                                "compareDescription": "ProstataProcess_Enkat",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "U_PromEnkatkod": {
                                "term": 4075,
                                "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                                "technicalName": "T1782958",
                                "sortOrder": 0,
                                "label": "Text(8000)",
                                "compareDescription": "Text(8000)",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "U_diadat": {
                                "term": 1002,
                                "description": "Diagnosdatum för prostatacancer",
                                "technicalName": "T1788257",
                                "sortOrder": 0,
                                "label": "Diagnosdatum för prostatacancer",
                                "compareDescription": "Diagnosdatum för prostatacancer",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "U_rpOpdat": {
                                "term": 1002,
                                "description": "Datum för radikal prostatektomi",
                                "technicalName": "T1788258",
                                "sortOrder": 0,
                                "label": "Datum för radikal prostatektomi",
                                "compareDescription": "Datum för radikal prostatektomi",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "U_rtKurStartdat": {
                                "term": 1002,
                                "description": "Datum för start av kurativ strålbehandling",
                                "technicalName": "T1788259",
                                "sortOrder": 0,
                                "label": "Datum för start av kurativ strålbehandling",
                                "compareDescription": "Datum för start av kurativ strålbehandling",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "U_rtPostopStartdat": {
                                "term": 1002,
                                "description": "Datum för start av postoperativ strålbehandling",
                                "technicalName": "T1788260",
                                "sortOrder": 0,
                                "label": "Datum för start av postoperativ strålbehandling",
                                "compareDescription": "Datum för start av postoperativ strålbehandling",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "U_gleassa": {
                                "term": 1040,
                                "description": "Gleasonscore vid diagnos",
                                "technicalName": "T1788261",
                                "sortOrder": 0,
                                "label": "Gleasonscore vid diagnos",
                                "compareDescription": "Gleasonscore vid diagnos",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "U_tstadDia": {
                                "term": 7721,
                                "description": "T-stadium vid diagnos",
                                "technicalName": "T1788701",
                                "sortOrder": 0,
                                "label": "T-stadium vid diagnos",
                                "compareDescription": "T-stadium vid diagnos",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "U_nstadDia": {
                                "term": 7722,
                                "description": "N-stadium vid diagnos",
                                "technicalName": "T1788700",
                                "sortOrder": 0,
                                "label": "N-stadium vid diagnos",
                                "compareDescription": "N-stadium vid diagnos",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "U_gleassaGrad2": {
                                "term": 1040,
                                "description": "Gleasonscore vid diagnos grad 2",
                                "technicalName": "T1788698",
                                "sortOrder": 0,
                                "label": "Gleasonscore vid diagnos grad 2",
                                "compareDescription": "Gleasonscore vid diagnos grad 2",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "U_gleassaGrad1": {
                                "term": 1040,
                                "description": "Gleasonscore vid diagnos grad 1",
                                "technicalName": "T1788697",
                                "sortOrder": 0,
                                "label": "Gleasonscore vid diagnos grad 1",
                                "compareDescription": "Gleasonscore vid diagnos grad 1",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "U_mstadDia": {
                                "term": 7723,
                                "description": "M-stadium vid diagnos",
                                "technicalName": "T1788699",
                                "sortOrder": 0,
                                "label": "M-stadium vid diagnos",
                                "compareDescription": "M-stadium vid diagnos",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "U_rtKurSlutdat": {
                                "term": 1002,
                                "description": "Datum för slut på kurativ strålbehandling",
                                "technicalName": "T1788694",
                                "sortOrder": 0,
                                "label": "Datum för slut på kurativ strålbehandling",
                                "compareDescription": "Datum för slut på kurativ strålbehandling",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "U_rtPostopSlutdat": {
                                "term": 1002,
                                "description": "Datum för slut på postoperativ strålbehandling",
                                "technicalName": "T1788695",
                                "sortOrder": 0,
                                "label": "Datum för slut på postoperativ strålbehandling",
                                "compareDescription": "Datum för slut på postoperativ strålbehandling",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "U_spsa": {
                                "term": 4962,
                                "description": "PSA vid diagnos",
                                "technicalName": "T1788264",
                                "sortOrder": 0,
                                "label": "PSA vid diagnos",
                                "compareDescription": "PSA vid diagnos",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "U_rtTyp": {
                                "term": 7725,
                                "description": "Kurativ strålbehandling",
                                "technicalName": "T1788705",
                                "sortOrder": 0,
                                "label": "Kurativ strålbehandling",
                                "compareDescription": "Kurativ strålbehandling",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "U_datagranskat": {
                                "term": 1002,
                                "description": "",
                                "technicalName": "T1790455",
                                "sortOrder": 0,
                                "label": "Data i översikten senast granskat",
                                "compareDescription": "Data i översikten senast granskat",
                                "isComparable": true,
                                "serversideOnly": false
                            }
                        },
                        "vdlists": {},
                        "terms": {
                            "1002": {
                                "name": "Datum",
                                "shortName": "Datum",
                                "description": "",
                                "dataType": "datetime",
                                "includeTime": false
                            },
                            "1040": {
                                "name": "Heltal",
                                "shortName": "Heltal",
                                "description": "",
                                "dataType": "integer",
                                "min": null,
                                "max": null
                            },
                            "4075": {
                                "name": "Text(8000)",
                                "shortName": "TEXT",
                                "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                                "dataType": "string",
                                "maxLength": 8000,
                                "validCharacters": null
                            },
                            "4962": {
                                "name": "Decimal3",
                                "shortName": "Decimal3",
                                "description": "Decimaltal med tre decimaler",
                                "dataType": "decimal",
                                "precision": 3,
                                "min": null,
                                "max": null
                            },
                            "7323": {
                                "name": "ProstataProcess_Enkat",
                                "shortName": "ProstataProcess_Enka",
                                "description": null,
                                "dataType": "vd"
                            },
                            "7721": {
                                "name": "U_tstaddia",
                                "shortName": "U_tstaddia",
                                "description": null,
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 24981,
                                        "text": "T1a",
                                        "value": "1",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24982,
                                        "text": "T1b",
                                        "value": "2",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24983,
                                        "text": "T1c",
                                        "value": "3",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24984,
                                        "text": "T2",
                                        "value": "4",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24985,
                                        "text": "T3",
                                        "value": "5",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24986,
                                        "text": "T4",
                                        "value": "6",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24987,
                                        "text": "TX",
                                        "value": "7",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            },
                            "7722": {
                                "name": "U_nstaddia",
                                "shortName": "U_nstaddia",
                                "description": null,
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 24988,
                                        "text": "N0",
                                        "value": "1",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24989,
                                        "text": "N1",
                                        "value": "2",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24990,
                                        "text": "NX",
                                        "value": "9",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            },
                            "7723": {
                                "name": "U_mstaddia",
                                "shortName": "U_mstaddia",
                                "description": null,
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 24991,
                                        "text": "M0",
                                        "value": "1",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24992,
                                        "text": "M1",
                                        "value": "2",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            },
                            "7725": {
                                "name": "ProstataProcess_RtTyp",
                                "shortName": "ProstataProcess_RtTy",
                                "description": null,
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 24996,
                                        "text": "Extern radioterapi",
                                        "value": "1",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24997,
                                        "text": "Kombinationsradioterapi (brachyterapi + extern radioterapi)",
                                        "value": "2",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24998,
                                        "text": "Monobrachyterapi LDR",
                                        "value": "3",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24999,
                                        "text": "Monobrachyterapi HDR",
                                        "value": "4",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            }
                        },
                        "treeLevel": 1
                    },
                    "Bilddiagnostik": {
                        "subTables": [
                            "BD_Metastas"
                        ],
                        "regvars": {
                            "BD_datum": {
                                "term": 1002,
                                "description": "Datum för bilddiagnostik",
                                "technicalName": "T1123083",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "BD_radbed": {
                                "term": 5652,
                                "description": "Radiologisk bedömning",
                                "technicalName": "T1166632",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "BD_radundtypannan": {
                                "term": 4075,
                                "description": "Annan typ av radiologisk undersökning",
                                "technicalName": "T1502990",
                                "sortOrder": 0,
                                "label": "Annan typ av radiologisk undersökning",
                                "compareDescription": "Annan typ av radiologisk undersökning",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "BD_radundertyp": {
                                "term": 6268,
                                "description": "Typ av radiologisk undersökning",
                                "technicalName": "T1502989",
                                "sortOrder": 0,
                                "label": "Typ av radiologisk undersökning",
                                "compareDescription": "Typ av radiologisk undersökning",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "BD_bsi": {
                                "term": 1460,
                                "description": "Bone Scan Index för skelettskintigrafi",
                                "technicalName": "T1520379",
                                "sortOrder": 0,
                                "label": "Bone Scan Index för skelettskintigrafi",
                                "compareDescription": "Bone Scan Index för skelettskintigrafi",
                                "isComparable": false,
                                "serversideOnly": false
                            }
                        },
                        "vdlists": {},
                        "terms": {
                            "1002": {
                                "name": "Datum",
                                "shortName": "Datum",
                                "description": "",
                                "dataType": "datetime",
                                "includeTime": false
                            },
                            "1460": {
                                "name": "Decimal1",
                                "shortName": "Decimal1",
                                "description": "Decimal tal med 1 decimal",
                                "dataType": "decimal",
                                "precision": 1,
                                "min": null,
                                "max": null
                            },
                            "4075": {
                                "name": "Text(8000)",
                                "shortName": "TEXT",
                                "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                                "dataType": "string",
                                "maxLength": 8000,
                                "validCharacters": null
                            },
                            "5652": {
                                "name": "ProstataProcess_RadiologiskBedömning",
                                "shortName": "PP_RadBedömning",
                                "description": "",
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 17668,
                                        "text": "Inga metastaser",
                                        "value": "3",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 25958,
                                        "text": "1:a metastas",
                                        "value": "6",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17665,
                                        "text": "Regress",
                                        "value": "0",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17666,
                                        "text": "Stationärt",
                                        "value": "1",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 18001,
                                        "text": "Blandad respons",
                                        "value": "4",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17667,
                                        "text": "Progress",
                                        "value": "2",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 21099,
                                        "text": "Ej möjligt att bedöma",
                                        "value": "5",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            },
                            "6268": {
                                "name": "ProstataProcess_RadiologiskUndersökningTyp",
                                "shortName": "ProstataProcess_Radi",
                                "description": "",
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 19896,
                                        "text": "Skelettskintigrafi",
                                        "value": "1",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 19897,
                                        "text": "CT",
                                        "value": "2",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 20150,
                                        "text": "CT buk",
                                        "value": "21",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 20151,
                                        "text": "CT thorax",
                                        "value": "22",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 20955,
                                        "text": "CT buk/thorax",
                                        "value": "24",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 20152,
                                        "text": "CT skalle",
                                        "value": "23",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 19898,
                                        "text": "MR",
                                        "value": "3",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 20153,
                                        "text": "MR buk/bäcken",
                                        "value": "31",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 20154,
                                        "text": "MR rygg",
                                        "value": "32",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 25957,
                                        "text": "MR Helrygg/bäcken",
                                        "value": "34",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 20155,
                                        "text": "MR skalle",
                                        "value": "33",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 19899,
                                        "text": "PET-CT",
                                        "value": "4",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 19900,
                                        "text": "Annan",
                                        "value": "9",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            }
                        },
                        "treeLevel": 2
                    },
                    "BD_Metastas": {
                        "subTables": [],
                        "regvars": {
                            "BDM_metastaslokalann": {
                                "term": 4075,
                                "description": "Nytillkomna metastaser i bilddiagnostik, Vilken annan lokal?",
                                "technicalName": "T1143283",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "BDM_metastaslokal": {
                                "term": 5799,
                                "description": "Nytillkomna metastaser i bilddiagnostik",
                                "technicalName": "T1218964",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            }
                        },
                        "vdlists": {},
                        "terms": {
                            "4075": {
                                "name": "Text(8000)",
                                "shortName": "TEXT",
                                "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                                "dataType": "string",
                                "maxLength": 8000,
                                "validCharacters": null
                            },
                            "5799": {
                                "name": "ProstataProcess_BDLokal",
                                "shortName": "PP_bdlokal",
                                "description": "",
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 18002,
                                        "text": "Skelett",
                                        "value": "0",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 18003,
                                        "text": "Lymfkörtlar",
                                        "value": "1",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 18004,
                                        "text": "Lunga",
                                        "value": "2",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 18005,
                                        "text": "Lever",
                                        "value": "3",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 18006,
                                        "text": "Hjärna",
                                        "value": "4",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 18007,
                                        "text": "Annan",
                                        "value": "5",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            }
                        },
                        "treeLevel": 3
                    },
                    "Labbprov": {
                        "subTables": [],
                        "regvars": {
                            "LP_datum": {
                                "term": 1002,
                                "description": "Datum för labbprov",
                                "technicalName": "T1123087",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "LP_hb": {
                                "term": 1040,
                                "description": "Hb(g/L)",
                                "technicalName": "T1123088",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "LP_psa": {
                                "term": 1242,
                                "description": "PSA(ng/mL)",
                                "technicalName": "T1127493",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "LP_krea": {
                                "term": 1040,
                                "description": "Krea(µmol/L)",
                                "technicalName": "T1127494",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "LP_alp": {
                                "term": 1460,
                                "description": "ALP(µkat/L)",
                                "technicalName": "T1130686",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "LP_testosteron": {
                                "term": 1242,
                                "description": "Testosteron (nmol/L)",
                                "technicalName": "T1206136",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "LP_neutrofila": {
                                "term": 1460,
                                "description": "Neutrofila (10^9/L)",
                                "technicalName": "T1785066",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "LP_lpk": {
                                "term": 1460,
                                "description": "LPK (10^9/L)",
                                "technicalName": "T1785065",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "LP_tpk": {
                                "term": 1040,
                                "description": "TPK (10^9/L)",
                                "technicalName": "T1785067",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": true,
                                "serversideOnly": false
                            }
                        },
                        "vdlists": {},
                        "terms": {
                            "1002": {
                                "name": "Datum",
                                "shortName": "Datum",
                                "description": "",
                                "dataType": "datetime",
                                "includeTime": false
                            },
                            "1040": {
                                "name": "Heltal",
                                "shortName": "Heltal",
                                "description": "",
                                "dataType": "integer",
                                "min": null,
                                "max": null
                            },
                            "1242": {
                                "name": "Decimal2",
                                "shortName": "DecimalTal2",
                                "description": "Decimal tal med 2 decimaler",
                                "dataType": "decimal",
                                "precision": 2,
                                "min": null,
                                "max": null
                            },
                            "1460": {
                                "name": "Decimal1",
                                "shortName": "Decimal1",
                                "description": "Decimal tal med 1 decimal",
                                "dataType": "decimal",
                                "precision": 1,
                                "min": null,
                                "max": null
                            }
                        },
                        "treeLevel": 2
                    },
                    "Besök": {
                        "subTables": [],
                        "regvars": {
                            "B_besokdatum": {
                                "term": 1002,
                                "description": "Datum för återbesök",
                                "technicalName": "T1127265",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "B_ecogwho": {
                                "term": 5584,
                                "description": "ECOG-WHO",
                                "technicalName": "T1127267",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "B_studie": {
                                "term": 5583,
                                "description": "Patienten ingår i studie",
                                "technicalName": "T1127271",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "B_klinbed": {
                                "term": 5585,
                                "description": "Sammantagen klinisk bedömning",
                                "technicalName": "T1127268",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "B_lakare": {
                                "term": 4075,
                                "description": "Undersökande läkare för återbesök",
                                "technicalName": "T1127266",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "B_studietxt": {
                                "term": 4075,
                                "description": "Patienten ingår i studie, Vilken studie?",
                                "technicalName": "T1155627",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "B_multikonf": {
                                "term": 5583,
                                "description": "Multidisciplinär konferens",
                                "technicalName": "T1166633",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "B_crpc": {
                                "term": 5583,
                                "description": "Kastrationsresistent sjukdom",
                                "technicalName": "T1169219",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "B_palliativvard": {
                                "term": 5583,
                                "description": "Patienten är inskriven i palliativ hemsjukvård",
                                "technicalName": "T1218976",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "B_sskbesok": {
                                "term": 1004,
                                "description": "Sjuksköterskebesök",
                                "technicalName": "T1312664",
                                "sortOrder": 0,
                                "label": "Sjuksköterskebesök",
                                "compareDescription": "Sjuksköterskebesök",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "B_brytpunktsbesok": {
                                "term": 1004,
                                "description": "Är detta ett brytpunktsbesök i patientens vård",
                                "technicalName": "T1690652",
                                "sortOrder": 0,
                                "label": "Är detta ett brytpunktsbesök i patientens vård",
                                "compareDescription": "Är detta ett brytpunktsbesök i patientens vård",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "B_telefonbesok": {
                                "term": 1004,
                                "description": "Är detta ett telefonbesök",
                                "technicalName": "T1690653",
                                "sortOrder": 0,
                                "label": "Är detta ett telefonbesök",
                                "compareDescription": "Är detta ett telefonbesök",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "B_ejecogwho": {
                                "term": 1004,
                                "description": "ECOG-WHO ej bedömbart",
                                "technicalName": "T1785053",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            }
                        },
                        "vdlists": {},
                        "terms": {
                            "1002": {
                                "name": "Datum",
                                "shortName": "Datum",
                                "description": "",
                                "dataType": "datetime",
                                "includeTime": false
                            },
                            "1004": {
                                "name": "Kryssruta",
                                "shortName": "Kryssruta",
                                "description": "",
                                "dataType": "boolean"
                            },
                            "4075": {
                                "name": "Text(8000)",
                                "shortName": "TEXT",
                                "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                                "dataType": "string",
                                "maxLength": 8000,
                                "validCharacters": null
                            },
                            "5583": {
                                "name": "ProstataProcess_NejJa",
                                "shortName": "PP_NejJa",
                                "description": "",
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 17406,
                                        "text": "Nej",
                                        "value": "0",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17407,
                                        "text": "Ja",
                                        "value": "1",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            },
                            "5584": {
                                "name": "ProstataProcess_ECOG-WHO",
                                "shortName": "PP_ECOGWHO",
                                "description": "",
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 17412,
                                        "text": "Klarar all normal aktivitet utan begränsning.",
                                        "value": "0",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17413,
                                        "text": "Klarar inte fysiskt krävande aktivitet men är uppegående och i stånd till lättare arbete.",
                                        "value": "1",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17414,
                                        "text": "Är uppegående och kan sköta sig själv men klarar inte att arbeta. Är uppe och i rörelse mer än 50% av dygnets vakna timmar.",
                                        "value": "2",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17415,
                                        "text": "Kan endast delvis sköta sig själv. Är bunden till säng eller stol mer än 50% av dygnets vakna timmar.",
                                        "value": "3",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17416,
                                        "text": "Klarar inte någonting. Kan inte sköta sig själv. Är bunden till säng eller stol.",
                                        "value": "4",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            },
                            "5585": {
                                "name": "ProstataProcess_KliniskBedömning",
                                "shortName": "PP_KliniskBedömning",
                                "description": "",
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 18264,
                                        "text": "Komplett respons",
                                        "value": "4",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24209,
                                        "text": "Respons",
                                        "value": "6",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17408,
                                        "text": "Partiell respons",
                                        "value": "0",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17409,
                                        "text": "Stabil sjukdom",
                                        "value": "1",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17410,
                                        "text": "Progress",
                                        "value": "2",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17411,
                                        "text": "Avvakta bedömning",
                                        "value": "3",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 21100,
                                        "text": "Ej bedömt",
                                        "value": "5",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            }
                        },
                        "treeLevel": 2
                    },
                    "SRE": {
                        "subTables": [
                            "SRE_Händelse"
                        ],
                        "regvars": {
                            "SRE_datum": {
                                "term": 1002,
                                "description": "Datum för SRE",
                                "technicalName": "T1127281",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            }
                        },
                        "vdlists": {},
                        "terms": {
                            "1002": {
                                "name": "Datum",
                                "shortName": "Datum",
                                "description": "",
                                "dataType": "datetime",
                                "includeTime": false
                            }
                        },
                        "treeLevel": 2
                    },
                    "SRE_Händelse": {
                        "subTables": [],
                        "regvars": {
                            "SREH_handelse": {
                                "term": 5586,
                                "description": "Vilken SRE Händelse",
                                "technicalName": "T1143273",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            }
                        },
                        "vdlists": {},
                        "terms": {
                            "5586": {
                                "name": "ProstataProcess_SRE",
                                "shortName": "PP_SRE",
                                "description": "",
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 17417,
                                        "text": "Medullakompression",
                                        "value": "0",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17418,
                                        "text": "Patologisk fraktur",
                                        "value": "1",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17419,
                                        "text": "Strålbehandling mot skelett",
                                        "value": "2",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17420,
                                        "text": "Kirurgisk åtgärd",
                                        "value": "3",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            }
                        },
                        "treeLevel": 3
                    },
                    "Läkemedel": {
                        "subTables": [
                            "LM_Insättning"
                        ],
                        "regvars": {
                            "LM_studielkmtxt": {
                                "term": 4075,
                                "description": "Vilket Studieläkemedel",
                                "technicalName": "T1155661",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "LM_typ": {
                                "term": 5629,
                                "description": "Läkemedelstyp",
                                "technicalName": "T1155662",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "LM_substans": {
                                "term": 5190,
                                "description": "Endokrin terapisubstanser. Värdedomänskoppling till Substans/Kategori register",
                                "technicalName": "T1169225",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "LM_studielkm": {
                                "term": 1004,
                                "description": "Läkemedel, studieläkemedel",
                                "technicalName": "T1204427",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "LM_ablatiotestis": {
                                "term": 1004,
                                "description": "Läkemedel, ablatio testis",
                                "technicalName": "T1204426",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "LM_ablatiotestisdatum": {
                                "term": 1002,
                                "description": "Läkemedel, ablatio testis datum",
                                "technicalName": "T1204425",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            }
                        },
                        "vdlists": {},
                        "terms": {
                            "1002": {
                                "name": "Datum",
                                "shortName": "Datum",
                                "description": "",
                                "dataType": "datetime",
                                "includeTime": false
                            },
                            "1004": {
                                "name": "Kryssruta",
                                "shortName": "Kryssruta",
                                "description": "",
                                "dataType": "boolean"
                            },
                            "4075": {
                                "name": "Text(8000)",
                                "shortName": "TEXT",
                                "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                                "dataType": "string",
                                "maxLength": 8000,
                                "validCharacters": null
                            },
                            "5190": {
                                "name": "SubstansKategori_VD",
                                "shortName": "substanskategori_vd",
                                "description": "Innehåller värdedomän till tabellen Substans i registret Substans/Kategori",
                                "dataType": "vd"
                            },
                            "5629": {
                                "name": "ProstataProcess_Läkemedelstyp",
                                "shortName": "PP_lakemedelstyp",
                                "description": "",
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 17531,
                                        "text": "Endokrin terapi",
                                        "value": "1",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17530,
                                        "text": "Cytostatika",
                                        "value": "0",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17735,
                                        "text": "Isotopterapi",
                                        "value": "3",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17734,
                                        "text": "Benstärkande terapi",
                                        "value": "2",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17736,
                                        "text": "Övriga cancerläkemedel",
                                        "value": "4",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            }
                        },
                        "treeLevel": 2
                    },
                    "LM_Insättning": {
                        "subTables": [
                            "LMI_Händelse",
                            "LMI_Utsättning"
                        ],
                        "regvars": {
                            "LMI_indikation": {
                                "term": 5588,
                                "description": "Indikation för insättning",
                                "technicalName": "T1127292",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            }
                        },
                        "vdlists": {},
                        "terms": {
                            "5588": {
                                "name": "ProstataProcess_Indikation",
                                "shortName": "PP_Indikation",
                                "description": "",
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 17426,
                                        "text": "Palliativ",
                                        "value": "0",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            }
                        },
                        "treeLevel": 3
                    },
                    "LMI_Händelse": {
                        "subTables": [],
                        "regvars": {
                            "LMIH_handelse": {
                                "term": 5589,
                                "description": "Vilken händelse",
                                "technicalName": "T1127294",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "LMIH_handelsedatum": {
                                "term": 1002,
                                "description": "Datum för händelse",
                                "technicalName": "T1127293",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "LMIH_handelseorsakann": {
                                "term": 4075,
                                "description": "Orsak till händelse, Annan",
                                "technicalName": "T1143285",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "LMIH_handelseorsak": {
                                "term": 5590,
                                "description": "Orsak till händelse",
                                "technicalName": "T1218970",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            }
                        },
                        "vdlists": {},
                        "terms": {
                            "1002": {
                                "name": "Datum",
                                "shortName": "Datum",
                                "description": "",
                                "dataType": "datetime",
                                "includeTime": false
                            },
                            "4075": {
                                "name": "Text(8000)",
                                "shortName": "TEXT",
                                "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                                "dataType": "string",
                                "maxLength": 8000,
                                "validCharacters": null
                            },
                            "5589": {
                                "name": "ProstataProcess_Händelse",
                                "shortName": "PP_handelse",
                                "description": "",
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 17427,
                                        "text": "Full dos",
                                        "value": "0",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17428,
                                        "text": "Reducerad dos",
                                        "value": "1",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17429,
                                        "text": "Doseskalering",
                                        "value": "2",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17430,
                                        "text": "Paus",
                                        "value": "3",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            },
                            "5590": {
                                "name": "ProstataProcess_HändelseOrsak",
                                "shortName": "PP_handelseorsak",
                                "description": "",
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 17465,
                                        "text": "Nyinsättning",
                                        "value": "5",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 20514,
                                        "text": "Återinsättning",
                                        "value": "6",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17431,
                                        "text": "Biverkning",
                                        "value": "0",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17432,
                                        "text": "Planerat stopp",
                                        "value": "1",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17434,
                                        "text": "Patientens önskemål",
                                        "value": "3",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 18687,
                                        "text": "Strålbehandling",
                                        "value": "2",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17435,
                                        "text": "Annan orsak",
                                        "value": "4",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            }
                        },
                        "treeLevel": 4
                    },
                    "LMI_Utsättning": {
                        "subTables": [],
                        "regvars": {
                            "LMIU_utsattningsdatum": {
                                "term": 1002,
                                "description": "Läkemedel, utsättningsdatum",
                                "technicalName": "T1127296",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "LMIU_utsattningsorsak": {
                                "term": 5591,
                                "description": "Läkemedel, utsättningsorsak",
                                "technicalName": "T1127297",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "LMIU_utsattningsorsakann": {
                                "term": 4075,
                                "description": "Läkemedel, utsättningsorsak annan",
                                "technicalName": "T1143289",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "LMIU_antalbeh": {
                                "term": 1040,
                                "description": "",
                                "technicalName": "T1790456",
                                "sortOrder": 0,
                                "label": "Antal givna behandlingar",
                                "compareDescription": "Antal givna behandlingar",
                                "isComparable": true,
                                "serversideOnly": false
                            }
                        },
                        "vdlists": {},
                        "terms": {
                            "1002": {
                                "name": "Datum",
                                "shortName": "Datum",
                                "description": "",
                                "dataType": "datetime",
                                "includeTime": false
                            },
                            "1040": {
                                "name": "Heltal",
                                "shortName": "Heltal",
                                "description": "",
                                "dataType": "integer",
                                "min": null,
                                "max": null
                            },
                            "4075": {
                                "name": "Text(8000)",
                                "shortName": "TEXT",
                                "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                                "dataType": "string",
                                "maxLength": 8000,
                                "validCharacters": null
                            },
                            "5591": {
                                "name": "ProstataProcess_UtsättningOrsak",
                                "shortName": "PP_utsattningorsak",
                                "description": "",
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 17436,
                                        "text": "Tumörprogress",
                                        "value": "0",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17437,
                                        "text": "Biverkning",
                                        "value": "1",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17438,
                                        "text": "Planerat stopp/skifte",
                                        "value": "2",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17439,
                                        "text": "Patientens önskemål",
                                        "value": "3",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17664,
                                        "text": "Dödsfall",
                                        "value": "5",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17440,
                                        "text": "Annan orsak",
                                        "value": "4",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            }
                        },
                        "treeLevel": 4
                    },
                    "Strålbehandling": {
                        "subTables": [
                            "SB_Grupp"
                        ],
                        "regvars": {
                            "SB_slutdatum": {
                                "term": 1002,
                                "description": "Slutdatum för strålbehandling",
                                "technicalName": "T1123084",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "SB_frakdos2": {
                                "term": 1040,
                                "description": "Dos per fraktion (Gy) för strålbehandling",
                                "technicalName": "T1123086",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "SB_slutdos2": {
                                "term": 1040,
                                "description": "Slutdos (Gy) för strålbehandling",
                                "technicalName": "T1123085",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            }
                        },
                        "vdlists": {},
                        "terms": {
                            "1002": {
                                "name": "Datum",
                                "shortName": "Datum",
                                "description": "",
                                "dataType": "datetime",
                                "includeTime": false
                            },
                            "1040": {
                                "name": "Heltal",
                                "shortName": "Heltal",
                                "description": "",
                                "dataType": "integer",
                                "min": null,
                                "max": null
                            }
                        },
                        "treeLevel": 2
                    },
                    "SB_Grupp": {
                        "subTables": [
                            "SBG_Lokal"
                        ],
                        "regvars": {
                            "SBG_slutdos": {
                                "term": 1040,
                                "description": "Slutdos (Gy) för strålbehandling",
                                "technicalName": "T1266632",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "SBG_frakdos": {
                                "term": 1040,
                                "description": "Strålbehandling, dos per fraktion",
                                "technicalName": "T1266633",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "SBL_lokal2": {
                                "term": 5621,
                                "description": "Lokal för strålbehandling",
                                "technicalName": "T1155630",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "SBL_lokalann2": {
                                "term": 4075,
                                "description": "Lokal för strålbehandling, Vilken annan lokal?",
                                "technicalName": "T1143284",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "SBL_lokalskelett2": {
                                "term": 4075,
                                "description": "Lokal för strålbehandling, Vilken skelett lokal?",
                                "technicalName": "T1155626",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            }
                        },
                        "vdlists": {},
                        "terms": {
                            "1040": {
                                "name": "Heltal",
                                "shortName": "Heltal",
                                "description": "",
                                "dataType": "integer",
                                "min": null,
                                "max": null
                            },
                            "4075": {
                                "name": "Text(8000)",
                                "shortName": "TEXT",
                                "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                                "dataType": "string",
                                "maxLength": 8000,
                                "validCharacters": null
                            },
                            "5621": {
                                "name": "ProstataProcess_SBLokal",
                                "shortName": "PP_sblokal",
                                "description": "",
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 17505,
                                        "text": "Skelett",
                                        "value": "2",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17504,
                                        "text": "Prostata",
                                        "value": "3",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17501,
                                        "text": "Blåsa",
                                        "value": "4",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17503,
                                        "text": "Lymfkörtlar",
                                        "value": "6",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17502,
                                        "text": "Hjärna",
                                        "value": "0",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17506,
                                        "text": "Annan",
                                        "value": "5",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            }
                        },
                        "treeLevel": 3
                    },
                    "SBG_Lokal": {
                        "subTables": [],
                        "regvars": {
                            "SBGL_lokal": {
                                "term": 5621,
                                "description": "Lokal för strålbehandling",
                                "technicalName": "T1266637",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "SBGL_lokalann": {
                                "term": 4075,
                                "description": "Lokal för strålbehandling, Vilken annan lokal?",
                                "technicalName": "T1266634",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "SBGL_lokalskelett": {
                                "term": 4075,
                                "description": "Lokal för strålbehandling, Vilken skelettlokal?",
                                "technicalName": "T1266635",
                                "sortOrder": 0,
                                "label": "",
                                "compareDescription": "",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "SBGL_lokallymf": {
                                "term": 4075,
                                "description": "Lokal för strålbehandling, Vilken lymfkörtellokal?",
                                "technicalName": "T1788754",
                                "sortOrder": 0,
                                "label": "Vilken lymfkörtellokal?",
                                "compareDescription": "Vilken lymfkörtellokal?",
                                "isComparable": true,
                                "serversideOnly": false
                            }
                        },
                        "vdlists": {},
                        "terms": {
                            "4075": {
                                "name": "Text(8000)",
                                "shortName": "TEXT",
                                "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                                "dataType": "string",
                                "maxLength": 8000,
                                "validCharacters": null
                            },
                            "5621": {
                                "name": "ProstataProcess_SBLokal",
                                "shortName": "PP_sblokal",
                                "description": "",
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 17505,
                                        "text": "Skelett",
                                        "value": "2",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17504,
                                        "text": "Prostata",
                                        "value": "3",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17501,
                                        "text": "Blåsa",
                                        "value": "4",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17503,
                                        "text": "Lymfkörtlar",
                                        "value": "6",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17502,
                                        "text": "Hjärna",
                                        "value": "0",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 17506,
                                        "text": "Annan",
                                        "value": "5",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            }
                        },
                        "treeLevel": 4
                    },
                    "Bidragande": {
                        "subTables": [],
                        "regvars": {
                            "BI_datum": {
                                "term": 1002,
                                "description": "Datum då ansökan för bidragande klinik gjorts",
                                "technicalName": "T1421910",
                                "sortOrder": 0,
                                "label": "Datum då ansökan för bidragande klinik gjorts",
                                "compareDescription": "Datum då ansökan för bidragande klinik gjorts",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "BI_godkannande": {
                                "term": 1004,
                                "description": "Godkännande av att lagar och regler kring patientsäkerhet har getts",
                                "technicalName": "T1421911",
                                "sortOrder": 0,
                                "label": "Godkännande av att lagar och regler kring patientsäkerhet har getts",
                                "compareDescription": "Godkännande av att lagar och regler kring patientsäkerhet har getts",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "BI_inrappenh": {
                                "term": 4075,
                                "description": "Inrapporterande enhet som gör ansökan om bidragande klinik",
                                "technicalName": "T1421912",
                                "sortOrder": 0,
                                "label": "Inrapporterande enhet som gör ansökan om bidragande klinik",
                                "compareDescription": "Inrapporterande enhet som gör ansökan om bidragande klinik",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "BI_inrapp": {
                                "term": 4075,
                                "description": "Inrapportör som gör ansökan för kliniken för att bli bidragande klinik",
                                "technicalName": "T1421913",
                                "sortOrder": 0,
                                "label": "Inrapportör som gör ansökan för kliniken för att bli bidragande klinik",
                                "compareDescription": "Inrapportör som gör ansökan för kliniken för att bli bidragande klinik",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "BI_kommentar": {
                                "term": 4075,
                                "description": "Övriga kommentarer/upplysningar",
                                "technicalName": "T1421914",
                                "sortOrder": 0,
                                "label": "Övriga kommentarer/upplysningar",
                                "compareDescription": "Övriga kommentarer/upplysningar",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "BI_sjukklin": {
                                "term": 4075,
                                "description": "Klinikkod för bidragande klinik",
                                "technicalName": "T1598049",
                                "sortOrder": 0,
                                "label": "Klinikkod för bidragande klinik",
                                "compareDescription": "Klinikkod för bidragande klinik",
                                "isComparable": false,
                                "serversideOnly": false
                            },
                            "BI_sjukkod": {
                                "term": 4075,
                                "description": "Sjukhuskod för bidragande klinik",
                                "technicalName": "T1598050",
                                "sortOrder": 0,
                                "label": "Sjukhuskod för bidragande klinik",
                                "compareDescription": "Sjukhuskod för bidragande klinik",
                                "isComparable": false,
                                "serversideOnly": false
                            }
                        },
                        "vdlists": {},
                        "terms": {
                            "1002": {
                                "name": "Datum",
                                "shortName": "Datum",
                                "description": "",
                                "dataType": "datetime",
                                "includeTime": false
                            },
                            "1004": {
                                "name": "Kryssruta",
                                "shortName": "Kryssruta",
                                "description": "",
                                "dataType": "boolean"
                            },
                            "4075": {
                                "name": "Text(8000)",
                                "shortName": "TEXT",
                                "description": "Textvariabel med möjlighet att lagra maximalt antal tecken (8000).",
                                "dataType": "string",
                                "maxLength": 8000,
                                "validCharacters": null
                            }
                        },
                        "treeLevel": 2
                    },
                    "Biobank": {
                        "subTables": [
                            "BB_Biobanksprov"
                        ],
                        "regvars": {
                            "BB_universitetssjukhus": {
                                "term": 7540,
                                "description": "",
                                "technicalName": "T1785453",
                                "sortOrder": 0,
                                "label": "ProstataProcess_Universitetssjukhus",
                                "compareDescription": "ProstataProcess_Universitetssjukhus",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "BB_biobanksdatum": {
                                "term": 1002,
                                "description": "",
                                "technicalName": "T1785444",
                                "sortOrder": 0,
                                "label": "Datum",
                                "compareDescription": "Datum",
                                "isComparable": true,
                                "serversideOnly": false
                            }
                        },
                        "vdlists": {},
                        "terms": {
                            "1002": {
                                "name": "Datum",
                                "shortName": "Datum",
                                "description": "",
                                "dataType": "datetime",
                                "includeTime": false
                            },
                            "7540": {
                                "name": "ProstataProcess_Universitetssjukhus",
                                "shortName": "PP_universitestssjuk",
                                "description": null,
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 24269,
                                        "text": "Akademiska sjukhuset",
                                        "value": "1",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24791,
                                        "text": "Capio S:t Görans sjukhus",
                                        "value": "8",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24880,
                                        "text": "Falu lasarett",
                                        "value": "10",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24881,
                                        "text": "Hallands sjukhus Halmstad",
                                        "value": "11",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24270,
                                        "text": "Karolinska universitetssjukhuset",
                                        "value": "2",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24271,
                                        "text": "Norrlands universitetssjukhus",
                                        "value": "3",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24272,
                                        "text": "Sahlgrenska Universitetssjukhuset",
                                        "value": "4",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24273,
                                        "text": "Skånes universitetssjukhus",
                                        "value": "5",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24882,
                                        "text": "Uddevalla Sjukhus",
                                        "value": "12",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24792,
                                        "text": "Södersjukhuset",
                                        "value": "9",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24274,
                                        "text": "Universitetssjukhuset i Linköping",
                                        "value": "6",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24275,
                                        "text": "Universitetssjukhuset Örebro",
                                        "value": "7",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            }
                        },
                        "treeLevel": 2
                    },
                    "BB_Biobanksprov": {
                        "subTables": [],
                        "regvars": {
                            "BBP_biobankprovtyp": {
                                "term": 7539,
                                "description": "",
                                "technicalName": "T1785454",
                                "sortOrder": 0,
                                "label": "ProstataProcess_Biobanksprovtyp",
                                "compareDescription": "ProstataProcess_Biobanksprovtyp",
                                "isComparable": true,
                                "serversideOnly": false
                            },
                            "BBP_typavvavnad": {
                                "term": 7538,
                                "description": "",
                                "technicalName": "T1785455",
                                "sortOrder": 0,
                                "label": "ProstataProcess_TypAvVavnad",
                                "compareDescription": "ProstataProcess_TypAvVavnad",
                                "isComparable": true,
                                "serversideOnly": false
                            }
                        },
                        "vdlists": {},
                        "terms": {
                            "7538": {
                                "name": "ProstataProcess_TypAvVavnad",
                                "shortName": "ProstataProcess_TypA",
                                "description": null,
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 24278,
                                        "text": "Prostata",
                                        "value": "1",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24279,
                                        "text": "Lymfkörtel",
                                        "value": "2",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24280,
                                        "text": "Benmärg",
                                        "value": "3",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24281,
                                        "text": "Skelett",
                                        "value": "4",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24282,
                                        "text": "Lever",
                                        "value": "5",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24283,
                                        "text": "Lunga",
                                        "value": "6",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24284,
                                        "text": "Benbiopsi",
                                        "value": "7",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24285,
                                        "text": "Lgll",
                                        "value": "8",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24286,
                                        "text": "Annat",
                                        "value": "9",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            },
                            "7539": {
                                "name": "ProstataProcess_Biobanksprovtyp",
                                "shortName": "PP_Biobanksprovtyp",
                                "description": null,
                                "dataType": "list",
                                "listValues": [
                                    {
                                        "id": 24276,
                                        "text": "Blod",
                                        "value": "1",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    },
                                    {
                                        "id": 24277,
                                        "text": "Vävnad",
                                        "value": "2",
                                        "validFrom": "0001-01-01T00:00:00",
                                        "validTo": "9999-12-31T00:00:00"
                                    }
                                ]
                            }
                        },
                        "treeLevel": 3
                    }
                },
                "designedFormId": 7298,
                "data": {
                    "id": 54588,
                    "subTables": {
                        "Bilddiagnostik": [],
                        "Labbprov": [],
                        "Besök": [],
                        "SRE": [],
                        "Läkemedel": [],
                        "Strålbehandling": [],
                        "Bidragande": [
                            {
                                "id": 64462,
                                "subTables": {},
                                "regvars": {
                                    "BI_datum": {
                                        "value": "2018-06-01",
                                        "include": true
                                    },
                                    "BI_godkannande": {
                                        "value": true,
                                        "include": true
                                    },
                                    "BI_inrappenh": {
                                        "value": "OC Demo (0) - SU/Sahlgrenska (500010) - Urol klin (361)",
                                        "include": true
                                    },
                                    "BI_inrapp": {
                                        "value": "Mats Bernerfalk",
                                        "include": true
                                    },
                                    "BI_kommentar": {
                                        "value": "Uppföljningen startas av kliniken",
                                        "include": true
                                    },
                                    "BI_sjukklin": {
                                        "value": "361",
                                        "include": true
                                    },
                                    "BI_sjukkod": {
                                        "value": "500010",
                                        "include": true
                                    }
                                },
                                "vdlists": {},
                                "registerRecordId": 12306628
                            }
                        ],
                        "Biobank": []
                    },
                    "regvars": {
                        "U_datum": {
                            "value": null,
                            "include": true
                        },
                        "U_crpcdatum": {
                            "value": null,
                            "include": true
                        },
                        "U_metastasdatum": {
                            "value": null,
                            "include": true
                        },
                        "U_hormonstartdatum": {
                            "value": null,
                            "include": true
                        },
                        "U_psadatum": {
                            "value": null,
                            "include": true
                        },
                        "U_pallHemSjkvrd": {
                            "value": null,
                            "include": true
                        },
                        "U_brytpunktssamtalsdatum": {
                            "value": null,
                            "include": true
                        },
                        "U_AMslutdatum": {
                            "value": null,
                            "include": true
                        },
                        "U_AMstartdatum": {
                            "value": null,
                            "include": true
                        },
                        "U_EnkatVD": {
                            "value": null,
                            "include": true
                        },
                        "U_PromEnkatkod": {
                            "value": "6igkxj7",
                            "include": true
                        },
                        "U_diadat": {
                            "value": null,
                            "include": true
                        },
                        "U_rpOpdat": {
                            "value": null,
                            "include": true
                        },
                        "U_rtKurStartdat": {
                            "value": null,
                            "include": true
                        },
                        "U_rtPostopStartdat": {
                            "value": null,
                            "include": true
                        },
                        "U_gleassa": {
                            "include": true
                        },
                        "U_tstadDia": {
                            "value": null,
                            "include": true
                        },
                        "U_nstadDia": {
                            "value": null,
                            "include": true
                        },
                        "U_gleassaGrad2": {
                            "value": null,
                            "include": true
                        },
                        "U_gleassaGrad1": {
                            "value": null,
                            "include": true
                        },
                        "U_mstadDia": {
                            "value": null,
                            "include": true
                        },
                        "U_rtKurSlutdat": {
                            "value": null,
                            "include": true
                        },
                        "U_rtPostopSlutdat": {
                            "value": null,
                            "include": true
                        },
                        "U_spsa": {
                            "value": null,
                            "include": true
                        },
                        "U_rtTyp": {
                            "value": null,
                            "include": true
                        },
                        "U_datagranskat": {
                            "value": null,
                            "include": true
                        }
                    },
                    "vdlists": {},
                    "registerRecordId": 12306627
                },
                "listValuesValidFrom": "0001-01-01",
                "listValuesValidTo": "0001-01-01",
                "env": {
                    "_INREPNAME": "",
                    "_INUNITNAME": "",
                    "_REPORTERNAME": "",
                    "_ADDRESS": "MINTSMAKEN 6",
                    "_AVREGDATUM": "",
                    "_AVREGORSAK": "",
                    "_BEFOLKNINGSREGISTER": "",
                    "_CITY": "LANDVETTER",
                    "_DATEOFDEATH": "",
                    "_DISTRICT": "",
                    "_DODSORSAK": "",
                    "_DODSORSAKSBESKR": "",
                    "_FIRSTNAME": "RAGNAR",
                    "_FODELSEDATUM": "1288-06-07",
                    "_FORSAMLING": "02",
                    "_KOMMUN": "01",
                    "_LAN": "14",
                    "_LAND": "Sweden",
                    "_LKF": "140102",
                    "_NAVETDATUM": "2017-04-10",
                    "_PATIENTID": "742662",
                    "_PERSNR": "12880607-9318",
                    "_POSTALCODE": "43838",
                    "_SECRET": "False",
                    "_SEX": "M",
                    "_SURNAME": "MINT"
                },
                "isReadOnly": false,
                "useHtmlLayout": true,
                "width": 1000,
                "name": "Uppdatering",
                "record": {}
            },
            "overview": {
                "supportsTabs": false
            },
            "restrictedAccessForCurrentPatient": []
        },
        "recording": {
            "getValueDomainValues": {
                "[[\"error\",null],[\"parameters\",[[\"kategori\",\"ProstataProcess_OC\"]]],[\"success\",null],[\"vdlist\",\"LM_substans\"]]": [
                    [
                        {
                            "text": "betametason",
                            "id": 1503,
                            "data": {
                                "sub_pk": 1503,
                                "sub_subnamnrek": "betametason",
                                "sub_atc": null,
                                "sub_cas": "378-44-9",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POCAU9DR0VERT1",
                                "sub_snomed": "116571008",
                                "sub_srsunii": "9842X06Q6M",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#da70d6"
                            }
                        },
                        {
                            "text": "dexametason",
                            "id": 995,
                            "data": {
                                "sub_pk": 995,
                                "sub_subnamnrek": "dexametason",
                                "sub_atc": null,
                                "sub_cas": "50-02-2",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POBSU8ZR8VERT1",
                                "sub_snomed": "372584003",
                                "sub_srsunii": "7S5I7G3JQL",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#d15fee"
                            }
                        },
                        {
                            "text": "prednisolon",
                            "id": 1005,
                            "data": {
                                "sub_pk": 1005,
                                "sub_subnamnrek": "prednisolon",
                                "sub_atc": null,
                                "sub_cas": "50-24-8",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POBSU8ZVEVERT1",
                                "sub_snomed": "116601002",
                                "sub_srsunii": "9PHQ9Y1OLM",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#cd69c9"
                            }
                        }
                    ]
                ],
                "[[\"error\",null],[\"parameters\",[[\"kategori\",\"ProstataProcess_IS\"]]],[\"success\",null],[\"vdlist\",\"LM_substans\"]]": [
                    [
                        {
                            "text": "radium(Ra-223)diklorid",
                            "id": 5107,
                            "data": {
                                "sub_pk": 5107,
                                "sub_subnamnrek": "radium(Ra-223)diklorid",
                                "sub_atc": "V10XX03",
                                "sub_cas": "444811-40-9",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "ID6349643612217999",
                                "sub_snomed": null,
                                "sub_srsunii": "RJ00KV3VTG",
                                "sub_versiondtm": "2014-02-12",
                                "kat_substansfarg": "#ffd700"
                            }
                        },
                        {
                            "text": "samarium (Sm-153) lexidronampentanatrium",
                            "id": 963,
                            "data": {
                                "sub_pk": 963,
                                "sub_subnamnrek": "samarium (Sm-153) lexidronampentanatrium",
                                "sub_atc": "V10BX02",
                                "sub_cas": "160369-78-8",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDA0MX49OEWLPCMSRV",
                                "sub_snomed": null,
                                "sub_srsunii": "7389YR3OOV",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#d9d919"
                            }
                        },
                        {
                            "text": "strontium (Sr-89) klorid",
                            "id": 3021,
                            "data": {
                                "sub_pk": 3021,
                                "sub_subnamnrek": "strontium (Sr-89) klorid",
                                "sub_atc": "V10BX01",
                                "sub_cas": "38270-90-5",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POI4UC66RVERT1",
                                "sub_snomed": "125701003",
                                "sub_srsunii": "5R78837D4A",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#ffd700"
                            }
                        }
                    ]
                ],
                "[[\"error\",null],[\"parameters\",[[\"kategori\",\"ProstataProcess_ET\"]]],[\"success\",null],[\"vdlist\",\"LM_substans\"]]": [
                    [
                        {
                            "text": "abirateron",
                            "id": 3469,
                            "data": {
                                "sub_pk": 3469,
                                "sub_subnamnrek": "abirateron",
                                "sub_atc": "L02BX03",
                                "sub_cas": "154229-19-3",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDL68G8E1D4IM7BSRV",
                                "sub_snomed": null,
                                "sub_srsunii": "G819A456D0",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#ee9572"
                            }
                        },
                        {
                            "text": "bikalutamid",
                            "id": 835,
                            "data": {
                                "sub_pk": 835,
                                "sub_subnamnrek": "bikalutamid",
                                "sub_atc": "L02BB03",
                                "sub_cas": "90357-06-5",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "ID3C4L4D1YC7USBMSR",
                                "sub_snomed": "386908000",
                                "sub_srsunii": "A0Z3NAU9DP",
                                "sub_versiondtm": "2013-06-18",
                                "kat_substansfarg": "#ee9a49"
                            }
                        },
                        {
                            "text": "buserelin",
                            "id": 2482,
                            "data": {
                                "sub_pk": 2482,
                                "sub_subnamnrek": "buserelin",
                                "sub_atc": "L02AE01",
                                "sub_cas": "57982-77-1",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POF6UARMDVERT1",
                                "sub_snomed": "395744006",
                                "sub_srsunii": "PXW8U3YXDV",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#d98719"
                            }
                        },
                        {
                            "text": "cyproteronacetat",
                            "id": 1509,
                            "data": {
                                "sub_pk": 1509,
                                "sub_subnamnrek": "cyproteronacetat",
                                "sub_atc": null,
                                "sub_cas": "427-51-0",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POCAU9DXYVERT1",
                                "sub_snomed": "126120000",
                                "sub_srsunii": "4KM2BN5JHF",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#ffa07a"
                            }
                        },
                        {
                            "text": "degarelix",
                            "id": 667,
                            "data": {
                                "sub_pk": 667,
                                "sub_subnamnrek": "degarelix",
                                "sub_atc": "L02BX02",
                                "sub_cas": "214766-78-6",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "ID1QKOM5K1CBHWLCSR",
                                "sub_snomed": "441864003",
                                "sub_srsunii": "SX0XJI3A11",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#cdaa7d"
                            }
                        },
                        {
                            "text": "enzalutamid",
                            "id": 5105,
                            "data": {
                                "sub_pk": 5105,
                                "sub_subnamnrek": "enzalutamid",
                                "sub_atc": "L02BB04",
                                "sub_cas": "915087-33-1",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "ID6347656300001275",
                                "sub_snomed": null,
                                "sub_srsunii": "93T0T9GKNU",
                                "sub_versiondtm": "2015-02-23",
                                "kat_substansfarg": "#ee9a49"
                            }
                        },
                        {
                            "text": "flutamid",
                            "id": 2093,
                            "data": {
                                "sub_pk": 2093,
                                "sub_subnamnrek": "flutamid",
                                "sub_atc": "L02BB01",
                                "sub_cas": "13311-84-7",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POEGUA7IJVERT1",
                                "sub_snomed": "387587007",
                                "sub_srsunii": "76W6J0943E",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#db9370"
                            }
                        },
                        {
                            "text": "goserelin",
                            "id": 2546,
                            "data": {
                                "sub_pk": 2546,
                                "sub_subnamnrek": "goserelin",
                                "sub_atc": "L02AE03",
                                "sub_cas": "65807-02-5",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POFAUAUR0VERT1",
                                "sub_snomed": "108771008",
                                "sub_srsunii": "0F65R8P09N",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#cdad00"
                            }
                        },
                        {
                            "text": "histrelin",
                            "id": 2635,
                            "data": {
                                "sub_pk": 2635,
                                "sub_subnamnrek": "histrelin",
                                "sub_atc": "L02AE05",
                                "sub_cas": "76712-82-8",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POFFUAYCXVERT1",
                                "sub_snomed": "109049002",
                                "sub_srsunii": "H50H3S3W74",
                                "sub_versiondtm": "2013-02-11",
                                "kat_substansfarg": "#cd8162"
                            }
                        },
                        {
                            "text": "ketokonazol",
                            "id": 2545,
                            "data": {
                                "sub_pk": 2545,
                                "sub_subnamnrek": "ketokonazol",
                                "sub_atc": null,
                                "sub_cas": "65277-42-1",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POFAUAUHUVERT1",
                                "sub_snomed": "387216007",
                                "sub_srsunii": "R9400W927I",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#8c7853"
                            }
                        },
                        {
                            "text": "leuprorelin",
                            "id": 2430,
                            "data": {
                                "sub_pk": 2430,
                                "sub_subnamnrek": "leuprorelin",
                                "sub_atc": "L02AE02",
                                "sub_cas": "53714-56-0",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POF2UAONTVERT1",
                                "sub_snomed": "397198002",
                                "sub_srsunii": "EFY6W0M8TG",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#ee9A00"
                            }
                        },
                        {
                            "text": "polyestradiolfosfat",
                            "id": 2289,
                            "data": {
                                "sub_pk": 2289,
                                "sub_subnamnrek": "polyestradiolfosfat",
                                "sub_atc": "L02AA02",
                                "sub_cas": "28014-46-2",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POETUAHBTVERT1",
                                "sub_snomed": "126084009",
                                "sub_srsunii": "P14877CDX2",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#b87333"
                            }
                        },
                        {
                            "text": "triptorelin",
                            "id": 2478,
                            "data": {
                                "sub_pk": 2478,
                                "sub_subnamnrek": "triptorelin",
                                "sub_atc": "L02AE04",
                                "sub_cas": "57773-63-4",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POF6UARH2VERT1",
                                "sub_snomed": "395915003",
                                "sub_srsunii": "9081Y98W2V",
                                "sub_versiondtm": "2016-03-04",
                                "kat_substansfarg": "#eeb422"
                            }
                        }
                    ],
                    [
                        {
                            "text": "abirateron",
                            "id": 3469,
                            "data": {
                                "sub_pk": 3469,
                                "sub_subnamnrek": "abirateron",
                                "sub_atc": "L02BX03",
                                "sub_cas": "154229-19-3",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDL68G8E1D4IM7BSRV",
                                "sub_snomed": null,
                                "sub_srsunii": "G819A456D0",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#ee9572"
                            }
                        },
                        {
                            "text": "bikalutamid",
                            "id": 835,
                            "data": {
                                "sub_pk": 835,
                                "sub_subnamnrek": "bikalutamid",
                                "sub_atc": "L02BB03",
                                "sub_cas": "90357-06-5",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "ID3C4L4D1YC7USBMSR",
                                "sub_snomed": "386908000",
                                "sub_srsunii": "A0Z3NAU9DP",
                                "sub_versiondtm": "2013-06-18",
                                "kat_substansfarg": "#ee9a49"
                            }
                        },
                        {
                            "text": "buserelin",
                            "id": 2482,
                            "data": {
                                "sub_pk": 2482,
                                "sub_subnamnrek": "buserelin",
                                "sub_atc": "L02AE01",
                                "sub_cas": "57982-77-1",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POF6UARMDVERT1",
                                "sub_snomed": "395744006",
                                "sub_srsunii": "PXW8U3YXDV",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#d98719"
                            }
                        },
                        {
                            "text": "cyproteronacetat",
                            "id": 1509,
                            "data": {
                                "sub_pk": 1509,
                                "sub_subnamnrek": "cyproteronacetat",
                                "sub_atc": null,
                                "sub_cas": "427-51-0",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POCAU9DXYVERT1",
                                "sub_snomed": "126120000",
                                "sub_srsunii": "4KM2BN5JHF",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#ffa07a"
                            }
                        },
                        {
                            "text": "degarelix",
                            "id": 667,
                            "data": {
                                "sub_pk": 667,
                                "sub_subnamnrek": "degarelix",
                                "sub_atc": "L02BX02",
                                "sub_cas": "214766-78-6",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "ID1QKOM5K1CBHWLCSR",
                                "sub_snomed": "441864003",
                                "sub_srsunii": "SX0XJI3A11",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#cdaa7d"
                            }
                        },
                        {
                            "text": "enzalutamid",
                            "id": 5105,
                            "data": {
                                "sub_pk": 5105,
                                "sub_subnamnrek": "enzalutamid",
                                "sub_atc": "L02BB04",
                                "sub_cas": "915087-33-1",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "ID6347656300001275",
                                "sub_snomed": null,
                                "sub_srsunii": "93T0T9GKNU",
                                "sub_versiondtm": "2015-02-23",
                                "kat_substansfarg": "#ee9a49"
                            }
                        },
                        {
                            "text": "flutamid",
                            "id": 2093,
                            "data": {
                                "sub_pk": 2093,
                                "sub_subnamnrek": "flutamid",
                                "sub_atc": "L02BB01",
                                "sub_cas": "13311-84-7",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POEGUA7IJVERT1",
                                "sub_snomed": "387587007",
                                "sub_srsunii": "76W6J0943E",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#db9370"
                            }
                        },
                        {
                            "text": "goserelin",
                            "id": 2546,
                            "data": {
                                "sub_pk": 2546,
                                "sub_subnamnrek": "goserelin",
                                "sub_atc": "L02AE03",
                                "sub_cas": "65807-02-5",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POFAUAUR0VERT1",
                                "sub_snomed": "108771008",
                                "sub_srsunii": "0F65R8P09N",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#cdad00"
                            }
                        },
                        {
                            "text": "histrelin",
                            "id": 2635,
                            "data": {
                                "sub_pk": 2635,
                                "sub_subnamnrek": "histrelin",
                                "sub_atc": "L02AE05",
                                "sub_cas": "76712-82-8",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POFFUAYCXVERT1",
                                "sub_snomed": "109049002",
                                "sub_srsunii": "H50H3S3W74",
                                "sub_versiondtm": "2013-02-11",
                                "kat_substansfarg": "#cd8162"
                            }
                        },
                        {
                            "text": "ketokonazol",
                            "id": 2545,
                            "data": {
                                "sub_pk": 2545,
                                "sub_subnamnrek": "ketokonazol",
                                "sub_atc": null,
                                "sub_cas": "65277-42-1",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POFAUAUHUVERT1",
                                "sub_snomed": "387216007",
                                "sub_srsunii": "R9400W927I",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#8c7853"
                            }
                        },
                        {
                            "text": "leuprorelin",
                            "id": 2430,
                            "data": {
                                "sub_pk": 2430,
                                "sub_subnamnrek": "leuprorelin",
                                "sub_atc": "L02AE02",
                                "sub_cas": "53714-56-0",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POF2UAONTVERT1",
                                "sub_snomed": "397198002",
                                "sub_srsunii": "EFY6W0M8TG",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#ee9A00"
                            }
                        },
                        {
                            "text": "polyestradiolfosfat",
                            "id": 2289,
                            "data": {
                                "sub_pk": 2289,
                                "sub_subnamnrek": "polyestradiolfosfat",
                                "sub_atc": "L02AA02",
                                "sub_cas": "28014-46-2",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POETUAHBTVERT1",
                                "sub_snomed": "126084009",
                                "sub_srsunii": "P14877CDX2",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#b87333"
                            }
                        },
                        {
                            "text": "triptorelin",
                            "id": 2478,
                            "data": {
                                "sub_pk": 2478,
                                "sub_subnamnrek": "triptorelin",
                                "sub_atc": "L02AE04",
                                "sub_cas": "57773-63-4",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POF6UARH2VERT1",
                                "sub_snomed": "395915003",
                                "sub_srsunii": "9081Y98W2V",
                                "sub_versiondtm": "2016-03-04",
                                "kat_substansfarg": "#eeb422"
                            }
                        }
                    ],
                    [
                        {
                            "text": "abirateron",
                            "id": 3469,
                            "data": {
                                "sub_pk": 3469,
                                "sub_subnamnrek": "abirateron",
                                "sub_atc": "L02BX03",
                                "sub_cas": "154229-19-3",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDL68G8E1D4IM7BSRV",
                                "sub_snomed": null,
                                "sub_srsunii": "G819A456D0",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#ee9572"
                            }
                        },
                        {
                            "text": "bikalutamid",
                            "id": 835,
                            "data": {
                                "sub_pk": 835,
                                "sub_subnamnrek": "bikalutamid",
                                "sub_atc": "L02BB03",
                                "sub_cas": "90357-06-5",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "ID3C4L4D1YC7USBMSR",
                                "sub_snomed": "386908000",
                                "sub_srsunii": "A0Z3NAU9DP",
                                "sub_versiondtm": "2013-06-18",
                                "kat_substansfarg": "#ee9a49"
                            }
                        },
                        {
                            "text": "buserelin",
                            "id": 2482,
                            "data": {
                                "sub_pk": 2482,
                                "sub_subnamnrek": "buserelin",
                                "sub_atc": "L02AE01",
                                "sub_cas": "57982-77-1",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POF6UARMDVERT1",
                                "sub_snomed": "395744006",
                                "sub_srsunii": "PXW8U3YXDV",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#d98719"
                            }
                        },
                        {
                            "text": "cyproteronacetat",
                            "id": 1509,
                            "data": {
                                "sub_pk": 1509,
                                "sub_subnamnrek": "cyproteronacetat",
                                "sub_atc": null,
                                "sub_cas": "427-51-0",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POCAU9DXYVERT1",
                                "sub_snomed": "126120000",
                                "sub_srsunii": "4KM2BN5JHF",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#ffa07a"
                            }
                        },
                        {
                            "text": "degarelix",
                            "id": 667,
                            "data": {
                                "sub_pk": 667,
                                "sub_subnamnrek": "degarelix",
                                "sub_atc": "L02BX02",
                                "sub_cas": "214766-78-6",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "ID1QKOM5K1CBHWLCSR",
                                "sub_snomed": "441864003",
                                "sub_srsunii": "SX0XJI3A11",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#cdaa7d"
                            }
                        },
                        {
                            "text": "enzalutamid",
                            "id": 5105,
                            "data": {
                                "sub_pk": 5105,
                                "sub_subnamnrek": "enzalutamid",
                                "sub_atc": "L02BB04",
                                "sub_cas": "915087-33-1",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "ID6347656300001275",
                                "sub_snomed": null,
                                "sub_srsunii": "93T0T9GKNU",
                                "sub_versiondtm": "2015-02-23",
                                "kat_substansfarg": "#ee9a49"
                            }
                        },
                        {
                            "text": "flutamid",
                            "id": 2093,
                            "data": {
                                "sub_pk": 2093,
                                "sub_subnamnrek": "flutamid",
                                "sub_atc": "L02BB01",
                                "sub_cas": "13311-84-7",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POEGUA7IJVERT1",
                                "sub_snomed": "387587007",
                                "sub_srsunii": "76W6J0943E",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#db9370"
                            }
                        },
                        {
                            "text": "goserelin",
                            "id": 2546,
                            "data": {
                                "sub_pk": 2546,
                                "sub_subnamnrek": "goserelin",
                                "sub_atc": "L02AE03",
                                "sub_cas": "65807-02-5",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POFAUAUR0VERT1",
                                "sub_snomed": "108771008",
                                "sub_srsunii": "0F65R8P09N",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#cdad00"
                            }
                        },
                        {
                            "text": "histrelin",
                            "id": 2635,
                            "data": {
                                "sub_pk": 2635,
                                "sub_subnamnrek": "histrelin",
                                "sub_atc": "L02AE05",
                                "sub_cas": "76712-82-8",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POFFUAYCXVERT1",
                                "sub_snomed": "109049002",
                                "sub_srsunii": "H50H3S3W74",
                                "sub_versiondtm": "2013-02-11",
                                "kat_substansfarg": "#cd8162"
                            }
                        },
                        {
                            "text": "ketokonazol",
                            "id": 2545,
                            "data": {
                                "sub_pk": 2545,
                                "sub_subnamnrek": "ketokonazol",
                                "sub_atc": null,
                                "sub_cas": "65277-42-1",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POFAUAUHUVERT1",
                                "sub_snomed": "387216007",
                                "sub_srsunii": "R9400W927I",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#8c7853"
                            }
                        },
                        {
                            "text": "leuprorelin",
                            "id": 2430,
                            "data": {
                                "sub_pk": 2430,
                                "sub_subnamnrek": "leuprorelin",
                                "sub_atc": "L02AE02",
                                "sub_cas": "53714-56-0",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POF2UAONTVERT1",
                                "sub_snomed": "397198002",
                                "sub_srsunii": "EFY6W0M8TG",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#ee9A00"
                            }
                        },
                        {
                            "text": "polyestradiolfosfat",
                            "id": 2289,
                            "data": {
                                "sub_pk": 2289,
                                "sub_subnamnrek": "polyestradiolfosfat",
                                "sub_atc": "L02AA02",
                                "sub_cas": "28014-46-2",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POETUAHBTVERT1",
                                "sub_snomed": "126084009",
                                "sub_srsunii": "P14877CDX2",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#b87333"
                            }
                        },
                        {
                            "text": "triptorelin",
                            "id": 2478,
                            "data": {
                                "sub_pk": 2478,
                                "sub_subnamnrek": "triptorelin",
                                "sub_atc": "L02AE04",
                                "sub_cas": "57773-63-4",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POF6UARH2VERT1",
                                "sub_snomed": "395915003",
                                "sub_srsunii": "9081Y98W2V",
                                "sub_versiondtm": "2016-03-04",
                                "kat_substansfarg": "#eeb422"
                            }
                        }
                    ],
                    [
                        {
                            "text": "abirateron",
                            "id": 3469,
                            "data": {
                                "sub_pk": 3469,
                                "sub_subnamnrek": "abirateron",
                                "sub_atc": "L02BX03",
                                "sub_cas": "154229-19-3",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDL68G8E1D4IM7BSRV",
                                "sub_snomed": null,
                                "sub_srsunii": "G819A456D0",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#ee9572"
                            }
                        },
                        {
                            "text": "bikalutamid",
                            "id": 835,
                            "data": {
                                "sub_pk": 835,
                                "sub_subnamnrek": "bikalutamid",
                                "sub_atc": "L02BB03",
                                "sub_cas": "90357-06-5",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "ID3C4L4D1YC7USBMSR",
                                "sub_snomed": "386908000",
                                "sub_srsunii": "A0Z3NAU9DP",
                                "sub_versiondtm": "2013-06-18",
                                "kat_substansfarg": "#ee9a49"
                            }
                        },
                        {
                            "text": "buserelin",
                            "id": 2482,
                            "data": {
                                "sub_pk": 2482,
                                "sub_subnamnrek": "buserelin",
                                "sub_atc": "L02AE01",
                                "sub_cas": "57982-77-1",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POF6UARMDVERT1",
                                "sub_snomed": "395744006",
                                "sub_srsunii": "PXW8U3YXDV",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#d98719"
                            }
                        },
                        {
                            "text": "cyproteronacetat",
                            "id": 1509,
                            "data": {
                                "sub_pk": 1509,
                                "sub_subnamnrek": "cyproteronacetat",
                                "sub_atc": null,
                                "sub_cas": "427-51-0",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POCAU9DXYVERT1",
                                "sub_snomed": "126120000",
                                "sub_srsunii": "4KM2BN5JHF",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#ffa07a"
                            }
                        },
                        {
                            "text": "degarelix",
                            "id": 667,
                            "data": {
                                "sub_pk": 667,
                                "sub_subnamnrek": "degarelix",
                                "sub_atc": "L02BX02",
                                "sub_cas": "214766-78-6",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "ID1QKOM5K1CBHWLCSR",
                                "sub_snomed": "441864003",
                                "sub_srsunii": "SX0XJI3A11",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#cdaa7d"
                            }
                        },
                        {
                            "text": "enzalutamid",
                            "id": 5105,
                            "data": {
                                "sub_pk": 5105,
                                "sub_subnamnrek": "enzalutamid",
                                "sub_atc": "L02BB04",
                                "sub_cas": "915087-33-1",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "ID6347656300001275",
                                "sub_snomed": null,
                                "sub_srsunii": "93T0T9GKNU",
                                "sub_versiondtm": "2015-02-23",
                                "kat_substansfarg": "#ee9a49"
                            }
                        },
                        {
                            "text": "flutamid",
                            "id": 2093,
                            "data": {
                                "sub_pk": 2093,
                                "sub_subnamnrek": "flutamid",
                                "sub_atc": "L02BB01",
                                "sub_cas": "13311-84-7",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POEGUA7IJVERT1",
                                "sub_snomed": "387587007",
                                "sub_srsunii": "76W6J0943E",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#db9370"
                            }
                        },
                        {
                            "text": "goserelin",
                            "id": 2546,
                            "data": {
                                "sub_pk": 2546,
                                "sub_subnamnrek": "goserelin",
                                "sub_atc": "L02AE03",
                                "sub_cas": "65807-02-5",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POFAUAUR0VERT1",
                                "sub_snomed": "108771008",
                                "sub_srsunii": "0F65R8P09N",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#cdad00"
                            }
                        },
                        {
                            "text": "histrelin",
                            "id": 2635,
                            "data": {
                                "sub_pk": 2635,
                                "sub_subnamnrek": "histrelin",
                                "sub_atc": "L02AE05",
                                "sub_cas": "76712-82-8",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POFFUAYCXVERT1",
                                "sub_snomed": "109049002",
                                "sub_srsunii": "H50H3S3W74",
                                "sub_versiondtm": "2013-02-11",
                                "kat_substansfarg": "#cd8162"
                            }
                        },
                        {
                            "text": "ketokonazol",
                            "id": 2545,
                            "data": {
                                "sub_pk": 2545,
                                "sub_subnamnrek": "ketokonazol",
                                "sub_atc": null,
                                "sub_cas": "65277-42-1",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POFAUAUHUVERT1",
                                "sub_snomed": "387216007",
                                "sub_srsunii": "R9400W927I",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#8c7853"
                            }
                        },
                        {
                            "text": "leuprorelin",
                            "id": 2430,
                            "data": {
                                "sub_pk": 2430,
                                "sub_subnamnrek": "leuprorelin",
                                "sub_atc": "L02AE02",
                                "sub_cas": "53714-56-0",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POF2UAONTVERT1",
                                "sub_snomed": "397198002",
                                "sub_srsunii": "EFY6W0M8TG",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#ee9A00"
                            }
                        },
                        {
                            "text": "polyestradiolfosfat",
                            "id": 2289,
                            "data": {
                                "sub_pk": 2289,
                                "sub_subnamnrek": "polyestradiolfosfat",
                                "sub_atc": "L02AA02",
                                "sub_cas": "28014-46-2",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POETUAHBTVERT1",
                                "sub_snomed": "126084009",
                                "sub_srsunii": "P14877CDX2",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#b87333"
                            }
                        },
                        {
                            "text": "triptorelin",
                            "id": 2478,
                            "data": {
                                "sub_pk": 2478,
                                "sub_subnamnrek": "triptorelin",
                                "sub_atc": "L02AE04",
                                "sub_cas": "57773-63-4",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POF6UARH2VERT1",
                                "sub_snomed": "395915003",
                                "sub_srsunii": "9081Y98W2V",
                                "sub_versiondtm": "2016-03-04",
                                "kat_substansfarg": "#eeb422"
                            }
                        }
                    ],
                    [
                        {
                            "text": "abirateron",
                            "id": 3469,
                            "data": {
                                "sub_pk": 3469,
                                "sub_subnamnrek": "abirateron",
                                "sub_atc": "L02BX03",
                                "sub_cas": "154229-19-3",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDL68G8E1D4IM7BSRV",
                                "sub_snomed": null,
                                "sub_srsunii": "G819A456D0",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#ee9572"
                            }
                        },
                        {
                            "text": "bikalutamid",
                            "id": 835,
                            "data": {
                                "sub_pk": 835,
                                "sub_subnamnrek": "bikalutamid",
                                "sub_atc": "L02BB03",
                                "sub_cas": "90357-06-5",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "ID3C4L4D1YC7USBMSR",
                                "sub_snomed": "386908000",
                                "sub_srsunii": "A0Z3NAU9DP",
                                "sub_versiondtm": "2013-06-18",
                                "kat_substansfarg": "#ee9a49"
                            }
                        },
                        {
                            "text": "buserelin",
                            "id": 2482,
                            "data": {
                                "sub_pk": 2482,
                                "sub_subnamnrek": "buserelin",
                                "sub_atc": "L02AE01",
                                "sub_cas": "57982-77-1",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POF6UARMDVERT1",
                                "sub_snomed": "395744006",
                                "sub_srsunii": "PXW8U3YXDV",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#d98719"
                            }
                        },
                        {
                            "text": "cyproteronacetat",
                            "id": 1509,
                            "data": {
                                "sub_pk": 1509,
                                "sub_subnamnrek": "cyproteronacetat",
                                "sub_atc": null,
                                "sub_cas": "427-51-0",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POCAU9DXYVERT1",
                                "sub_snomed": "126120000",
                                "sub_srsunii": "4KM2BN5JHF",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#ffa07a"
                            }
                        },
                        {
                            "text": "degarelix",
                            "id": 667,
                            "data": {
                                "sub_pk": 667,
                                "sub_subnamnrek": "degarelix",
                                "sub_atc": "L02BX02",
                                "sub_cas": "214766-78-6",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "ID1QKOM5K1CBHWLCSR",
                                "sub_snomed": "441864003",
                                "sub_srsunii": "SX0XJI3A11",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#cdaa7d"
                            }
                        },
                        {
                            "text": "enzalutamid",
                            "id": 5105,
                            "data": {
                                "sub_pk": 5105,
                                "sub_subnamnrek": "enzalutamid",
                                "sub_atc": "L02BB04",
                                "sub_cas": "915087-33-1",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "ID6347656300001275",
                                "sub_snomed": null,
                                "sub_srsunii": "93T0T9GKNU",
                                "sub_versiondtm": "2015-02-23",
                                "kat_substansfarg": "#ee9a49"
                            }
                        },
                        {
                            "text": "flutamid",
                            "id": 2093,
                            "data": {
                                "sub_pk": 2093,
                                "sub_subnamnrek": "flutamid",
                                "sub_atc": "L02BB01",
                                "sub_cas": "13311-84-7",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POEGUA7IJVERT1",
                                "sub_snomed": "387587007",
                                "sub_srsunii": "76W6J0943E",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#db9370"
                            }
                        },
                        {
                            "text": "goserelin",
                            "id": 2546,
                            "data": {
                                "sub_pk": 2546,
                                "sub_subnamnrek": "goserelin",
                                "sub_atc": "L02AE03",
                                "sub_cas": "65807-02-5",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POFAUAUR0VERT1",
                                "sub_snomed": "108771008",
                                "sub_srsunii": "0F65R8P09N",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#cdad00"
                            }
                        },
                        {
                            "text": "histrelin",
                            "id": 2635,
                            "data": {
                                "sub_pk": 2635,
                                "sub_subnamnrek": "histrelin",
                                "sub_atc": "L02AE05",
                                "sub_cas": "76712-82-8",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POFFUAYCXVERT1",
                                "sub_snomed": "109049002",
                                "sub_srsunii": "H50H3S3W74",
                                "sub_versiondtm": "2013-02-11",
                                "kat_substansfarg": "#cd8162"
                            }
                        },
                        {
                            "text": "ketokonazol",
                            "id": 2545,
                            "data": {
                                "sub_pk": 2545,
                                "sub_subnamnrek": "ketokonazol",
                                "sub_atc": null,
                                "sub_cas": "65277-42-1",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POFAUAUHUVERT1",
                                "sub_snomed": "387216007",
                                "sub_srsunii": "R9400W927I",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#8c7853"
                            }
                        },
                        {
                            "text": "leuprorelin",
                            "id": 2430,
                            "data": {
                                "sub_pk": 2430,
                                "sub_subnamnrek": "leuprorelin",
                                "sub_atc": "L02AE02",
                                "sub_cas": "53714-56-0",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POF2UAONTVERT1",
                                "sub_snomed": "397198002",
                                "sub_srsunii": "EFY6W0M8TG",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#ee9A00"
                            }
                        },
                        {
                            "text": "polyestradiolfosfat",
                            "id": 2289,
                            "data": {
                                "sub_pk": 2289,
                                "sub_subnamnrek": "polyestradiolfosfat",
                                "sub_atc": "L02AA02",
                                "sub_cas": "28014-46-2",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POETUAHBTVERT1",
                                "sub_snomed": "126084009",
                                "sub_srsunii": "P14877CDX2",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#b87333"
                            }
                        },
                        {
                            "text": "triptorelin",
                            "id": 2478,
                            "data": {
                                "sub_pk": 2478,
                                "sub_subnamnrek": "triptorelin",
                                "sub_atc": "L02AE04",
                                "sub_cas": "57773-63-4",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POF6UARH2VERT1",
                                "sub_snomed": "395915003",
                                "sub_srsunii": "9081Y98W2V",
                                "sub_versiondtm": "2016-03-04",
                                "kat_substansfarg": "#eeb422"
                            }
                        }
                    ]
                ],
                "[[\"error\",null],[\"parameters\",[[\"kategori\",\"ProstataProcess_CY\"]]],[\"success\",null],[\"vdlist\",\"LM_substans\"]]": [
                    [
                        {
                            "text": "cisplatin",
                            "id": 2139,
                            "data": {
                                "sub_pk": 2139,
                                "sub_subnamnrek": "cisplatin",
                                "sub_atc": "L01XA01",
                                "sub_cas": "15663-27-1",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POEKUAA72VERT1",
                                "sub_snomed": "387318005",
                                "sub_srsunii": "Q20Q21Q62J",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#7b68ee"
                            }
                        },
                        {
                            "text": "cyklofosfamid (vattenfri)",
                            "id": 1002,
                            "data": {
                                "sub_pk": 1002,
                                "sub_subnamnrek": "cyklofosfamid (vattenfri)",
                                "sub_atc": "L01AA01",
                                "sub_cas": "50-18-0",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POBSU8ZUAVERT1",
                                "sub_snomed": null,
                                "sub_srsunii": "6UXW23996M",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#00bfff"
                            }
                        },
                        {
                            "text": "docetaxel (vattenfri)",
                            "id": 3121,
                            "data": {
                                "sub_pk": 3121,
                                "sub_subnamnrek": "docetaxel (vattenfri)",
                                "sub_atc": "L01CD02",
                                "sub_cas": "114977-28-5",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POIAUCB20VERT1",
                                "sub_snomed": null,
                                "sub_srsunii": "699121PHCA",
                                "sub_versiondtm": "2017-03-10",
                                "kat_substansfarg": "#3299cc"
                            }
                        },
                        {
                            "text": "doxorubicin",
                            "id": 2230,
                            "data": {
                                "sub_pk": 2230,
                                "sub_subnamnrek": "doxorubicin",
                                "sub_atc": "L01DB01",
                                "sub_cas": "23214-92-8",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POEPUAELMVERT1",
                                "sub_snomed": "372817009",
                                "sub_srsunii": "80168379AG",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#87cefa"
                            }
                        },
                        {
                            "text": "estramustin",
                            "id": 1786,
                            "data": {
                                "sub_pk": 1786,
                                "sub_subnamnrek": "estramustin",
                                "sub_atc": "L01XX11",
                                "sub_cas": "2998-57-4",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POCSU9RHVVERT1",
                                "sub_snomed": "386909008",
                                "sub_srsunii": "35LT29625A",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#a2b5cd"
                            }
                        },
                        {
                            "text": "etoposid",
                            "id": 2325,
                            "data": {
                                "sub_pk": 2325,
                                "sub_subnamnrek": "etoposid",
                                "sub_atc": "L01CB01",
                                "sub_cas": "33419-42-0",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POEVUAJ75VERT1",
                                "sub_snomed": "387316009",
                                "sub_srsunii": "6PLQ3CP4P3",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#7ac5cd"
                            }
                        },
                        {
                            "text": "fluorouracil",
                            "id": 1029,
                            "data": {
                                "sub_pk": 1029,
                                "sub_subnamnrek": "fluorouracil",
                                "sub_atc": "L01BC02",
                                "sub_cas": "51-21-8",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POBSU9087VERT1",
                                "sub_snomed": "387172005",
                                "sub_srsunii": "U3P01618RT",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#a4d3ee"
                            }
                        },
                        {
                            "text": "gemcitabin",
                            "id": 2748,
                            "data": {
                                "sub_pk": 2748,
                                "sub_subnamnrek": "gemcitabin",
                                "sub_atc": "L01BC05",
                                "sub_cas": "95058-81-4",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POFLUB3A4VERT1",
                                "sub_snomed": "386920008",
                                "sub_srsunii": "B76N6SBZ8R",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#6959cd"
                            }
                        },
                        {
                            "text": "kabazitaxel",
                            "id": 107,
                            "data": {
                                "sub_pk": 107,
                                "sub_subnamnrek": "kabazitaxel",
                                "sub_atc": "L01CD04",
                                "sub_cas": "183133-96-2",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "ID16FSULLC436YIMSR",
                                "sub_snomed": "446706007",
                                "sub_srsunii": "51F690397J",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#00c5cd"
                            }
                        },
                        {
                            "text": "karboplatin",
                            "id": 2377,
                            "data": {
                                "sub_pk": 2377,
                                "sub_subnamnrek": "karboplatin",
                                "sub_atc": "L01XA02",
                                "sub_cas": "41575-94-4",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POEZUAM6JVERT1",
                                "sub_snomed": "386905002",
                                "sub_srsunii": "BG3F62OND5",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#7a67ee"
                            }
                        },
                        {
                            "text": "metotrexat",
                            "id": 1147,
                            "data": {
                                "sub_pk": 1147,
                                "sub_subnamnrek": "metotrexat",
                                "sub_atc": "L01BA01",
                                "sub_cas": "59-05-2",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POBVU9210VERT1",
                                "sub_snomed": "387381009",
                                "sub_srsunii": "YL5FZ2Y5U1",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": null
                            }
                        },
                        {
                            "text": "mitoxantron",
                            "id": 2544,
                            "data": {
                                "sub_pk": 2544,
                                "sub_subnamnrek": "mitoxantron",
                                "sub_atc": "L01DB07",
                                "sub_cas": "65271-80-9",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POFAUAUHKVERT1",
                                "sub_snomed": "386913001",
                                "sub_srsunii": "BZ114NVM5P",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#1c86ee"
                            }
                        },
                        {
                            "text": "paklitaxel",
                            "id": 3056,
                            "data": {
                                "sub_pk": 3056,
                                "sub_subnamnrek": "paklitaxel",
                                "sub_atc": "L01CD01",
                                "sub_cas": "33069-62-4",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POI6UC7RSVERT1",
                                "sub_snomed": "387374002",
                                "sub_srsunii": "P88XT4IS4D",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#5f9ea0"
                            }
                        }
                    ],
                    [
                        {
                            "text": "cisplatin",
                            "id": 2139,
                            "data": {
                                "sub_pk": 2139,
                                "sub_subnamnrek": "cisplatin",
                                "sub_atc": "L01XA01",
                                "sub_cas": "15663-27-1",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POEKUAA72VERT1",
                                "sub_snomed": "387318005",
                                "sub_srsunii": "Q20Q21Q62J",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#7b68ee"
                            }
                        },
                        {
                            "text": "cyklofosfamid (vattenfri)",
                            "id": 1002,
                            "data": {
                                "sub_pk": 1002,
                                "sub_subnamnrek": "cyklofosfamid (vattenfri)",
                                "sub_atc": "L01AA01",
                                "sub_cas": "50-18-0",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POBSU8ZUAVERT1",
                                "sub_snomed": null,
                                "sub_srsunii": "6UXW23996M",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#00bfff"
                            }
                        },
                        {
                            "text": "docetaxel (vattenfri)",
                            "id": 3121,
                            "data": {
                                "sub_pk": 3121,
                                "sub_subnamnrek": "docetaxel (vattenfri)",
                                "sub_atc": "L01CD02",
                                "sub_cas": "114977-28-5",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POIAUCB20VERT1",
                                "sub_snomed": null,
                                "sub_srsunii": "699121PHCA",
                                "sub_versiondtm": "2017-03-10",
                                "kat_substansfarg": "#3299cc"
                            }
                        },
                        {
                            "text": "doxorubicin",
                            "id": 2230,
                            "data": {
                                "sub_pk": 2230,
                                "sub_subnamnrek": "doxorubicin",
                                "sub_atc": "L01DB01",
                                "sub_cas": "23214-92-8",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POEPUAELMVERT1",
                                "sub_snomed": "372817009",
                                "sub_srsunii": "80168379AG",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#87cefa"
                            }
                        },
                        {
                            "text": "estramustin",
                            "id": 1786,
                            "data": {
                                "sub_pk": 1786,
                                "sub_subnamnrek": "estramustin",
                                "sub_atc": "L01XX11",
                                "sub_cas": "2998-57-4",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POCSU9RHVVERT1",
                                "sub_snomed": "386909008",
                                "sub_srsunii": "35LT29625A",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#a2b5cd"
                            }
                        },
                        {
                            "text": "etoposid",
                            "id": 2325,
                            "data": {
                                "sub_pk": 2325,
                                "sub_subnamnrek": "etoposid",
                                "sub_atc": "L01CB01",
                                "sub_cas": "33419-42-0",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POEVUAJ75VERT1",
                                "sub_snomed": "387316009",
                                "sub_srsunii": "6PLQ3CP4P3",
                                "sub_versiondtm": "2012-09-25",
                                "kat_substansfarg": "#7ac5cd"
                            }
                        },
                        {
                            "text": "fluorouracil",
                            "id": 1029,
                            "data": {
                                "sub_pk": 1029,
                                "sub_subnamnrek": "fluorouracil",
                                "sub_atc": "L01BC02",
                                "sub_cas": "51-21-8",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POBSU9087VERT1",
                                "sub_snomed": "387172005",
                                "sub_srsunii": "U3P01618RT",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#a4d3ee"
                            }
                        },
                        {
                            "text": "gemcitabin",
                            "id": 2748,
                            "data": {
                                "sub_pk": 2748,
                                "sub_subnamnrek": "gemcitabin",
                                "sub_atc": "L01BC05",
                                "sub_cas": "95058-81-4",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POFLUB3A4VERT1",
                                "sub_snomed": "386920008",
                                "sub_srsunii": "B76N6SBZ8R",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#6959cd"
                            }
                        },
                        {
                            "text": "kabazitaxel",
                            "id": 107,
                            "data": {
                                "sub_pk": 107,
                                "sub_subnamnrek": "kabazitaxel",
                                "sub_atc": "L01CD04",
                                "sub_cas": "183133-96-2",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "ID16FSULLC436YIMSR",
                                "sub_snomed": "446706007",
                                "sub_srsunii": "51F690397J",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#00c5cd"
                            }
                        },
                        {
                            "text": "karboplatin",
                            "id": 2377,
                            "data": {
                                "sub_pk": 2377,
                                "sub_subnamnrek": "karboplatin",
                                "sub_atc": "L01XA02",
                                "sub_cas": "41575-94-4",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POEZUAM6JVERT1",
                                "sub_snomed": "386905002",
                                "sub_srsunii": "BG3F62OND5",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#7a67ee"
                            }
                        },
                        {
                            "text": "metotrexat",
                            "id": 1147,
                            "data": {
                                "sub_pk": 1147,
                                "sub_subnamnrek": "metotrexat",
                                "sub_atc": "L01BA01",
                                "sub_cas": "59-05-2",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POBVU9210VERT1",
                                "sub_snomed": "387381009",
                                "sub_srsunii": "YL5FZ2Y5U1",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": null
                            }
                        },
                        {
                            "text": "mitoxantron",
                            "id": 2544,
                            "data": {
                                "sub_pk": 2544,
                                "sub_subnamnrek": "mitoxantron",
                                "sub_atc": "L01DB07",
                                "sub_cas": "65271-80-9",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POFAUAUHKVERT1",
                                "sub_snomed": "386913001",
                                "sub_srsunii": "BZ114NVM5P",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#1c86ee"
                            }
                        },
                        {
                            "text": "paklitaxel",
                            "id": 3056,
                            "data": {
                                "sub_pk": 3056,
                                "sub_subnamnrek": "paklitaxel",
                                "sub_atc": "L01CD01",
                                "sub_cas": "33069-62-4",
                                "sub_narkotikaklass": "0",
                                "sub_nslid": "IDE4POI6UC7RSVERT1",
                                "sub_snomed": "387374002",
                                "sub_srsunii": "P88XT4IS4D",
                                "sub_versiondtm": "2016-02-29",
                                "kat_substansfarg": "#5f9ea0"
                            }
                        }
                    ]
                ],
                "[[\"error\",null],[\"parameters\",[[\"deltagarkod\",\"6igkxj7\"]]],[\"success\",null],[\"vdlist\",\"U_EnkatVD\"]]": [
                    [
                        {
                            "text": "{\"ParticipantCode\" : \"6IGKXJ7\", \"EventCode\" : \"\", \"FolderId\" : \"563\", \"Started\" : \"2016-12-15 15:51:45\", \"Finished\" : \"2016-12-15 15:52:57\", \"CurrentQuestionId\" : \"6667\", \"FurthestQuestionId\" : \"6667\", \"fr1\" : \"2\", \"fr2\" : \"2\", \"fr3\" : \"2\", \"fr4\" : \"2\", \"fr5\" : \"2\", \"fr6\" : \"2\", \"fr7\" : \"2\", \"fr8\" : \"2\", \"fr9\" : \"2\", \"fr10\" : \"2\", \"fr11\" : \"2\", \"fr12\" : \"2\", \"fr13\" : \"2\", \"fr14\" : \"2\", \"fr15\" : \"6\", \"fr16\" : \"3\", \"fr17\" : \"2\"} 6IGKXJ7 6IGKXJ7 73  2016-12-15 15:52:57 5110056 2016-12-15 15:51:45",
                            "id": 30,
                            "data": {
                                "R2612T32660_ID": 30,
                                "EnkatdataJSON": "{\"ParticipantCode\" : \"6IGKXJ7\", \"EventCode\" : \"\", \"FolderId\" : \"563\", \"Started\" : \"2016-12-15 15:51:45\", \"Finished\" : \"2016-12-15 15:52:57\", \"CurrentQuestionId\" : \"6667\", \"FurthestQuestionId\" : \"6667\", \"fr1\" : \"2\", \"fr2\" : \"2\", \"fr3\" : \"2\", \"fr4\" : \"2\", \"fr5\" : \"2\", \"fr6\" : \"2\", \"fr7\" : \"2\", \"fr8\" : \"2\", \"fr9\" : \"2\", \"fr10\" : \"2\", \"fr11\" : \"2\", \"fr12\" : \"2\", \"fr13\" : \"2\", \"fr14\" : \"2\", \"fr15\" : \"6\", \"fr16\" : \"3\", \"fr17\" : \"2\"}",
                                "Deltagarkod": "6IGKXJ7",
                                "DeltagarkodForalder": "6IGKXJ7",
                                "EnkatId": 73,
                                "EnkatNamn": null,
                                "KlarTidpunkt": "2016-12-15 15:52:57",
                                "REGISTERRECORD_ID": 5110056,
                                "StartadTidpunkt": "2016-12-15 15:51:45"
                            }
                        },
                        {
                            "text": "{\"ParticipantCode\" : \"MVZ5H6\", \"EventCode\" : \"6IGKXJ7\", \"FolderId\" : \"563\", \"Started\" : \"2016-12-15 15:54:28\", \"Finished\" : \"2016-12-15 15:59:48\", \"CurrentQuestionId\" : \"6667\", \"FurthestQuestionId\" : \"6667\", \"fr1\" : \"\", \"fr2\" : \"\", \"fr3\" : \"\", \"fr4\" : \"\", \"fr5\" : \"\", \"fr6\" : \"\", \"fr7\" : \"\", \"fr8\" : \"\", \"fr9\" : \"\", \"fr10\" : \"\", \"fr11\" : \"\", \"fr12\" : \"\", \"fr13\" : \"\", \"fr14\" : \"\", \"fr15\" : \"\", \"fr16\" : \"\", \"fr17\" : \"\"} MVZ5H6 6IGKXJ7 73  2016-12-15 15:59:48 5110072 2016-12-15 15:54:28",
                            "id": 32,
                            "data": {
                                "R2612T32660_ID": 32,
                                "EnkatdataJSON": "{\"ParticipantCode\" : \"MVZ5H6\", \"EventCode\" : \"6IGKXJ7\", \"FolderId\" : \"563\", \"Started\" : \"2016-12-15 15:54:28\", \"Finished\" : \"2016-12-15 15:59:48\", \"CurrentQuestionId\" : \"6667\", \"FurthestQuestionId\" : \"6667\", \"fr1\" : \"\", \"fr2\" : \"\", \"fr3\" : \"\", \"fr4\" : \"\", \"fr5\" : \"\", \"fr6\" : \"\", \"fr7\" : \"\", \"fr8\" : \"\", \"fr9\" : \"\", \"fr10\" : \"\", \"fr11\" : \"\", \"fr12\" : \"\", \"fr13\" : \"\", \"fr14\" : \"\", \"fr15\" : \"\", \"fr16\" : \"\", \"fr17\" : \"\"}",
                                "Deltagarkod": "MVZ5H6",
                                "DeltagarkodForalder": "6IGKXJ7",
                                "EnkatId": 73,
                                "EnkatNamn": null,
                                "KlarTidpunkt": "2016-12-15 15:59:48",
                                "REGISTERRECORD_ID": 5110072,
                                "StartadTidpunkt": "2016-12-15 15:54:28"
                            }
                        },
                        {
                            "text": "{\"ParticipantCode\" : \"1BZ51T\", \"EventCode\" : \"6IGKXJ7\", \"FolderId\" : \"563\", \"Started\" : \"2016-12-20 08:01:02\", \"Finished\" : \"2016-12-20 08:02:14\", \"CurrentQuestionId\" : \"6667\", \"FurthestQuestionId\" : \"6667\", \"fr1\" : \"4\", \"fr2\" : \"1\", \"fr3\" : \"4\", \"fr4\" : \"1\", \"fr5\" : \"4\", \"fr6\" : \"1\", \"fr7\" : \"4\", \"fr8\" : \"1\", \"fr9\" : \"4\", \"fr10\" : \"1\", \"fr11\" : \"4\", \"fr12\" : \"1\", \"fr13\" : \"4\", \"fr14\" : \"1\", \"fr15\" : \"6\", \"fr16\" : \"3\", \"fr17\" : \"3\"} 1BZ51T 6IGKXJ7 73  2016-12-20 08:02:14 5114293 2016-12-20 08:01:02",
                            "id": 52,
                            "data": {
                                "R2612T32660_ID": 52,
                                "EnkatdataJSON": "{\"ParticipantCode\" : \"1BZ51T\", \"EventCode\" : \"6IGKXJ7\", \"FolderId\" : \"563\", \"Started\" : \"2016-12-20 08:01:02\", \"Finished\" : \"2016-12-20 08:02:14\", \"CurrentQuestionId\" : \"6667\", \"FurthestQuestionId\" : \"6667\", \"fr1\" : \"4\", \"fr2\" : \"1\", \"fr3\" : \"4\", \"fr4\" : \"1\", \"fr5\" : \"4\", \"fr6\" : \"1\", \"fr7\" : \"4\", \"fr8\" : \"1\", \"fr9\" : \"4\", \"fr10\" : \"1\", \"fr11\" : \"4\", \"fr12\" : \"1\", \"fr13\" : \"4\", \"fr14\" : \"1\", \"fr15\" : \"6\", \"fr16\" : \"3\", \"fr17\" : \"3\"}",
                                "Deltagarkod": "1BZ51T",
                                "DeltagarkodForalder": "6IGKXJ7",
                                "EnkatId": 73,
                                "EnkatNamn": null,
                                "KlarTidpunkt": "2016-12-20 08:02:14",
                                "REGISTERRECORD_ID": 5114293,
                                "StartadTidpunkt": "2016-12-20 08:01:02"
                            }
                        },
                        {
                            "text": "{\"ParticipantCode\" : \"KN46K3\", \"EventCode\" : \"6IGKXJ7\", \"FolderId\" : \"563\", \"Started\" : \"2017-01-24 09:54:16\", \"Finished\" : \"2017-01-27 13:34:33\", \"CurrentQuestionId\" : \"6667\", \"FurthestQuestionId\" : \"6667\", \"fr1\" : \"1\", \"fr2\" : \"2\", \"fr3\" : \"2\", \"fr4\" : \"3\", \"fr5\" : \"3\", \"fr6\" : \"2\", \"fr7\" : \"1\", \"fr8\" : \"1\", \"fr9\" : \"2\", \"fr10\" : \"1\", \"fr11\" : \"3\", \"fr12\" : \"1\", \"fr13\" : \"1\", \"fr14\" : \"2\", \"fr15\" : \"2\", \"fr16\" : \"3\", \"fr17\" : \"1\"} KN46K3 6IGKXJ7 73  2017-01-27 13:34:33 5279700 2017-01-24 09:54:16",
                            "id": 64,
                            "data": {
                                "R2612T32660_ID": 64,
                                "EnkatdataJSON": "{\"ParticipantCode\" : \"KN46K3\", \"EventCode\" : \"6IGKXJ7\", \"FolderId\" : \"563\", \"Started\" : \"2017-01-24 09:54:16\", \"Finished\" : \"2017-01-27 13:34:33\", \"CurrentQuestionId\" : \"6667\", \"FurthestQuestionId\" : \"6667\", \"fr1\" : \"1\", \"fr2\" : \"2\", \"fr3\" : \"2\", \"fr4\" : \"3\", \"fr5\" : \"3\", \"fr6\" : \"2\", \"fr7\" : \"1\", \"fr8\" : \"1\", \"fr9\" : \"2\", \"fr10\" : \"1\", \"fr11\" : \"3\", \"fr12\" : \"1\", \"fr13\" : \"1\", \"fr14\" : \"2\", \"fr15\" : \"2\", \"fr16\" : \"3\", \"fr17\" : \"1\"}",
                                "Deltagarkod": "KN46K3",
                                "DeltagarkodForalder": "6IGKXJ7",
                                "EnkatId": 73,
                                "EnkatNamn": null,
                                "KlarTidpunkt": "2017-01-27 13:34:33",
                                "REGISTERRECORD_ID": 5279700,
                                "StartadTidpunkt": "2017-01-24 09:54:16"
                            }
                        },
                        {
                            "text": "{\"ParticipantCode\" : \"5U56FV\", \"EventCode\" : \"6IGKXJ7\", \"FolderId\" : \"563\", \"Started\" : \"2017-02-01 16:07:25\", \"Finished\" : \"2017-02-01 16:09:19\", \"CurrentQuestionId\" : \"6667\", \"FurthestQuestionId\" : \"6667\", \"fr1\" : \"1\", \"fr2\" : \"1\", \"fr3\" : \"1\", \"fr4\" : \"1\", \"fr5\" : \"1\", \"fr6\" : \"2\", \"fr7\" : \"3\", \"fr8\" : \"2\", \"fr9\" : \"3\", \"fr10\" : \"1\", \"fr11\" : \"3\", \"fr12\" : \"3\", \"fr13\" : \"1\", \"fr14\" : \"3\", \"fr15\" : \"6\", \"fr16\" : \"5\", \"fr17\" : \"2\"} 5U56FV 6IGKXJ7 73  2017-02-01 16:09:19 5291110 2017-02-01 16:07:25",
                            "id": 72,
                            "data": {
                                "R2612T32660_ID": 72,
                                "EnkatdataJSON": "{\"ParticipantCode\" : \"5U56FV\", \"EventCode\" : \"6IGKXJ7\", \"FolderId\" : \"563\", \"Started\" : \"2017-02-01 16:07:25\", \"Finished\" : \"2017-02-01 16:09:19\", \"CurrentQuestionId\" : \"6667\", \"FurthestQuestionId\" : \"6667\", \"fr1\" : \"1\", \"fr2\" : \"1\", \"fr3\" : \"1\", \"fr4\" : \"1\", \"fr5\" : \"1\", \"fr6\" : \"2\", \"fr7\" : \"3\", \"fr8\" : \"2\", \"fr9\" : \"3\", \"fr10\" : \"1\", \"fr11\" : \"3\", \"fr12\" : \"3\", \"fr13\" : \"1\", \"fr14\" : \"3\", \"fr15\" : \"6\", \"fr16\" : \"5\", \"fr17\" : \"2\"}",
                                "Deltagarkod": "5U56FV",
                                "DeltagarkodForalder": "6IGKXJ7",
                                "EnkatId": 73,
                                "EnkatNamn": null,
                                "KlarTidpunkt": "2017-02-01 16:09:19",
                                "REGISTERRECORD_ID": 5291110,
                                "StartadTidpunkt": "2017-02-01 16:07:25"
                            }
                        },
                        {
                            "text": "{\"ParticipantCode\" : \"BG5629\", \"EventCode\" : \"6IGKXJ7\", \"FolderId\" : \"563\", \"Started\" : \"2017-02-02 14:34:05\", \"Finished\" : \"2017-02-02 14:38:14\", \"CurrentQuestionId\" : \"6667\", \"FurthestQuestionId\" : \"6667\", \"fr1\" : \"1\", \"fr2\" : \"2\", \"fr3\" : \"3\", \"fr4\" : \"4\", \"fr5\" : \"1\", \"fr6\" : \"2\", \"fr7\" : \"3\", \"fr8\" : \"4\", \"fr9\" : \"1\", \"fr10\" : \"2\", \"fr11\" : \"3\", \"fr12\" : \"4\", \"fr13\" : \"1\", \"fr14\" : \"2\", \"fr15\" : \"3\", \"fr16\" : \"4\", \"fr17\" : \"1\"} BG5629 6IGKXJ7 73  2017-02-02 14:38:14 5292422 2017-02-02 14:34:05",
                            "id": 74,
                            "data": {
                                "R2612T32660_ID": 74,
                                "EnkatdataJSON": "{\"ParticipantCode\" : \"BG5629\", \"EventCode\" : \"6IGKXJ7\", \"FolderId\" : \"563\", \"Started\" : \"2017-02-02 14:34:05\", \"Finished\" : \"2017-02-02 14:38:14\", \"CurrentQuestionId\" : \"6667\", \"FurthestQuestionId\" : \"6667\", \"fr1\" : \"1\", \"fr2\" : \"2\", \"fr3\" : \"3\", \"fr4\" : \"4\", \"fr5\" : \"1\", \"fr6\" : \"2\", \"fr7\" : \"3\", \"fr8\" : \"4\", \"fr9\" : \"1\", \"fr10\" : \"2\", \"fr11\" : \"3\", \"fr12\" : \"4\", \"fr13\" : \"1\", \"fr14\" : \"2\", \"fr15\" : \"3\", \"fr16\" : \"4\", \"fr17\" : \"1\"}",
                                "Deltagarkod": "BG5629",
                                "DeltagarkodForalder": "6IGKXJ7",
                                "EnkatId": 73,
                                "EnkatNamn": null,
                                "KlarTidpunkt": "2017-02-02 14:38:14",
                                "REGISTERRECORD_ID": 5292422,
                                "StartadTidpunkt": "2017-02-02 14:34:05"
                            }
                        },
                        {
                            "text": "{\"ParticipantCode\" : \"BH56R4\", \"EventCode\" : \"6IGKXJ7\", \"FolderId\" : \"563\", \"Started\" : \"2017-03-28 15:50:48\", \"Finished\" : \"2017-03-28 15:52:22\", \"CurrentQuestionId\" : \"6667\", \"FurthestQuestionId\" : \"6667\", \"fr1\" : \"1\", \"fr2\" : \"1\", \"fr3\" : \"1\", \"fr4\" : \"1\", \"fr5\" : \"1\", \"fr6\" : \"3\", \"fr7\" : \"2\", \"fr8\" : \"2\", \"fr9\" : \"1\", \"fr10\" : \"1\", \"fr11\" : \"4\", \"fr12\" : \"1\", \"fr13\" : \"2\", \"fr14\" : \"2\", \"fr15\" : \"4\", \"fr16\" : \"0\", \"fr17\" : \"1\"} BH56R4 6IGKXJ7 73  2017-03-28 15:52:22 7608634 2017-03-28 15:50:48",
                            "id": 194,
                            "data": {
                                "R2612T32660_ID": 194,
                                "EnkatdataJSON": "{\"ParticipantCode\" : \"BH56R4\", \"EventCode\" : \"6IGKXJ7\", \"FolderId\" : \"563\", \"Started\" : \"2017-03-28 15:50:48\", \"Finished\" : \"2017-03-28 15:52:22\", \"CurrentQuestionId\" : \"6667\", \"FurthestQuestionId\" : \"6667\", \"fr1\" : \"1\", \"fr2\" : \"1\", \"fr3\" : \"1\", \"fr4\" : \"1\", \"fr5\" : \"1\", \"fr6\" : \"3\", \"fr7\" : \"2\", \"fr8\" : \"2\", \"fr9\" : \"1\", \"fr10\" : \"1\", \"fr11\" : \"4\", \"fr12\" : \"1\", \"fr13\" : \"2\", \"fr14\" : \"2\", \"fr15\" : \"4\", \"fr16\" : \"0\", \"fr17\" : \"1\"}",
                                "Deltagarkod": "BH56R4",
                                "DeltagarkodForalder": "6IGKXJ7",
                                "EnkatId": 73,
                                "EnkatNamn": null,
                                "KlarTidpunkt": "2017-03-28 15:52:22",
                                "REGISTERRECORD_ID": 7608634,
                                "StartadTidpunkt": "2017-03-28 15:50:48"
                            }
                        }
                    ]
                ]
            },
            "getRegisterRecordData": {}
        }
    }
});
