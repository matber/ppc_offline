$(function() {

    /* CHART FUNKTIONER */

    initializeOverviewHighChart = function(vm) {
        // create the chart

        vm.overviewHighChartLabels = ['Sammantagen bedömning', 'ECOG-WHO', 'Bilddiagnostik', 'Strålbehandling', 'SSE', 'Brytpunktssamtal', 'CRPC datum', 'PSA', 'ALP', 'Krea', 'Hb', 'Testo', 'PSA dubblering', 'PSA halvering', 'Datum första metastas', 'PROM', 'PROM pie', 'LPK', 'TPK', 'Neutrofila', 'Biobank'];
        vm.overviewWidth = 975; // 850
        vm.overviewLabHeight = 220; // 120
        vm.overviewLabPadding = 50;
        vm.overviewAxisPadding = 25;
        vm.overviewSBHeight = 30;
        vm.overviewSBPadding = 35;
        vm.overviewPlotHeight = 60;
        vm.overviewPlotPadding = 25;
        vm.overviewPROMHeight = 20;
        vm.overviewPROMPadding = 20;
        vm.LMHeight = 20;
        vm.overviewLMHeight = vm.$$.Läkemedel().length * vm.LMHeight;
        vm.overviewLMPadding = 25;

        vm.modifiedPSA = ko.observable(false);
        vm.modifiedALP = ko.observable(false);
        vm.modifiedKrea = ko.observable(false);
        vm.modifiedHb = ko.observable(false);
        vm.modifiedTesto = ko.observable(false);
        vm.modifiedTPK = ko.observable(false);
        vm.modifiedLPK = ko.observable(false);
        vm.modifiedNeutrofila = ko.observable(false);

        vm.redrawLegend = function() {
            vm.overviewHighChart.legend.destroyItem(vm.overviewHighChart.series[2]);
            vm.overviewHighChart.legend.renderItem(vm.overviewHighChart.series[2]);
            vm.overviewHighChart.legend.destroyItem(vm.overviewHighChart.series[3]);
            vm.overviewHighChart.legend.renderItem(vm.overviewHighChart.series[3]);
            vm.overviewHighChart.legend.destroyItem(vm.overviewHighChart.series[4]);
            vm.overviewHighChart.legend.renderItem(vm.overviewHighChart.series[4]);
            vm.overviewHighChart.legend.destroyItem(vm.overviewHighChart.series[5]);
            vm.overviewHighChart.legend.renderItem(vm.overviewHighChart.series[5]);
            vm.overviewHighChart.legend.destroyItem(vm.overviewHighChart.series[6]);
            vm.overviewHighChart.legend.renderItem(vm.overviewHighChart.series[6]);
            vm.overviewHighChart.legend.destroyItem(vm.overviewHighChart.get('LPK'));
            vm.overviewHighChart.legend.renderItem(vm.overviewHighChart.get('LPK'));
            vm.overviewHighChart.legend.destroyItem(vm.overviewHighChart.get('TPK'));
            vm.overviewHighChart.legend.renderItem(vm.overviewHighChart.get('TPK'));
            vm.overviewHighChart.legend.destroyItem(vm.overviewHighChart.get('Neutrofila'));
            vm.overviewHighChart.legend.renderItem(vm.overviewHighChart.get('Neutrofila'));
            vm.overviewHighChart.legend.render();
        };

        vm.senastePSA = ko.computed(function() {
            if (vm.modifiedPSA() && vm.overviewHighChart.series[2].yData.length > 0) {
                var PSAn = vm.overviewHighChart.series[2].yData;
                var datum = vm.overviewHighChart.series[2].xData;

                vm.modifiedPSA(false);

                return PSAn[PSAn.length - 1] + ' µg/L<br>' + new Date(datum[datum.length - 1]).toJSON().slice(0, 10);
            } else {
                return undefined;
            }
        });

        vm.senasteALP = ko.computed(function() {
            if (vm.modifiedALP() && vm.overviewHighChart.series[3].yData.length > 0) {
                var ALPn = vm.overviewHighChart.series[3].yData;
                var datum = vm.overviewHighChart.series[3].xData;

                vm.modifiedALP(false);

                return ALPn[ALPn.length - 1] + ' µkat/L<br>' + new Date(datum[datum.length - 1]).toJSON().slice(0, 10);
            } else {
                return undefined;
            }
        });

        vm.senasteKrea = ko.computed(function() {
            if (vm.modifiedKrea() && vm.overviewHighChart.series[4].yData.length > 0) {
                var Krean = vm.overviewHighChart.series[4].yData;
                var datum = vm.overviewHighChart.series[4].xData;

                vm.modifiedKrea(false);

                return Krean[Krean.length - 1] + ' µmol/L<br>' + new Date(datum[datum.length - 1]).toJSON().slice(0, 10);
            } else {
                return undefined;
            }
        });

        vm.senasteHb = ko.computed(function() {
            if (vm.modifiedHb() && vm.overviewHighChart.series[5].yData.length > 0) {
                var Hbn = vm.overviewHighChart.series[5].yData;
                var datum = vm.overviewHighChart.series[5].xData;

                vm.modifiedHb(false);

                return Hbn[Hbn.length - 1] + ' g/L<br>' + new Date(datum[datum.length - 1]).toJSON().slice(0, 10);
            } else {
                return undefined;
            }
        });

        vm.senasteTesto = ko.computed(function() {
            if (vm.modifiedTesto() && vm.overviewHighChart.series[6].yData.length > 0) {
                var Teston = vm.overviewHighChart.series[6].yData;
                var datum = vm.overviewHighChart.series[6].xData;

                vm.modifiedTesto(false);

                return Teston[Teston.length - 1] + ' nmol/L<br>' + new Date(datum[datum.length - 1]).toJSON().slice(0, 10);
            } else {
                return undefined;
            }
        });

        vm.senasteLPK = ko.computed(function() {
            if(vm.modifiedLPK() && vm.overviewHighChart.get('LPK').yData.length > 0) {
                var LPKValues = vm.overviewHighChart.get('LPK').yData;
                var datum = vm.overviewHighChart.get('LPK').xData;

                vm.modifiedLPK(false);

                return LPKValues[LPKValues.length - 1] + ' 10^9/L<br>' + new Date(datum[datum.length - 1]).toJSON().slice(0, 10);
            } else  {
                return undefined;
            }
        });

        vm.senasteTPK = ko.computed(function() {
            if(vm.modifiedTPK() && vm.overviewHighChart.get('TPK').yData.length > 0) {
                var TPKValues = vm.overviewHighChart.get('TPK').yData;
                var datum = vm.overviewHighChart.get('TPK').xData;

                vm.modifiedTPK(false);

                return TPKValues[TPKValues.length - 1] + ' 10^9/L<br>' + new Date(datum[datum.length - 1]).toJSON().slice(0, 10);
            } else  {
                return undefined;
            }
        });

        vm.senasteNeutrofila = ko.computed(function() {
            if(vm.modifiedNeutrofila() && vm.overviewHighChart.get('Neutrofila').yData.length > 0) {
                var NeutrofilaValues = vm.overviewHighChart.get('Neutrofila').yData;
                var datum = vm.overviewHighChart.get('Neutrofila').xData;

                vm.modifiedNeutrofila(false);

                return NeutrofilaValues[NeutrofilaValues.length - 1] + ' 10^9/L<br>' + new Date(datum[datum.length - 1]).toJSON().slice(0, 10);
            } else  {
                return undefined;
            }
        });

        vm.xOffset = -(vm.overviewPlotHeight + vm.overviewAxisPadding + vm.overviewPlotPadding + vm.overviewLMHeight + vm.overviewLMPadding + vm.overviewSBHeight + vm.overviewSBPadding + vm.overviewPROMHeight + vm.overviewPROMPadding);


        /**
         * Highchartsinställningar
         */

        var highchartOptions = {};

        // chart
        highchartOptions.chart = {
            renderTo: 'overview-highchart',
            zoomType: 'x',
            alignTicks: false,
            marginLeft: 50,
            marginRight: 225,
            marginBottom: 10,
            marginTop: 10,
            padding: 0,
            //type: 'scatter',
            width: vm.overviewWidth,
            height: vm.overviewLabHeight + vm.overviewAxisPadding + vm.overviewLabPadding + vm.overviewSBHeight + vm.overviewSBPadding + vm.overviewPlotHeight + vm.overviewPlotPadding + vm.overviewPROMHeight + vm.overviewPROMPadding + vm.overviewLMHeight + vm.overviewLMPadding,
            resetZoomButton: {
                theme: {
                    display: 'none'
                }
            },
            events: {
                selection: function(event) {
                    var pie = event.target.get('PROM pie');
                    if(pie) {
                        redrawOverviewPROMPie();
                    }
                    $('#zoomOutBtn').removeClass('hiding');
                }
            }
        };

        // xAxis
        highchartOptions.xAxis = {
            type: 'datetime',
            labels: {
                formatter: function() {
                    return Highcharts.dateFormat('%e %b %Y', this.value);
                }
            },
            lineColor: '#000000',
            tickColor: '#101010',
            offset: vm.xOffset,
            startOnTick: false,
            endOnTick: false,
            minPadding: 0.01,
            maxPadding: 0.01,
            showEmpty: false,
            tickPixelInterval: 125
        };

        // yAxis
        // might be nice with a generator, like generateXAxis({ min: 0, max: 2... })
        // use values for present options, fallback to default
        highchartOptions.yAxis = [
            { 
                title: { text: '' },
                labels: {
                    enabled: true,
                    zIndex: -1,
                    style: {
                        color: '#000000',
                        whiteSpace: 'nowrap',
                        textOverflow: 'none'
                    },
                    formatter: function() {
                        return this.value;
                    }
                },
                gridLineColor: '#d3d3d3',
                gridLineWidth: 1,
                offset: 0,
                min: 0,
                opposite: true,
                height: vm.overviewLabHeight
            },
            {
                title: { text: '' },
                labels: {
                    enabled: false,
                    zIndex: -1,
                    style: {
                        color: '#000000',
                        whiteSpace: 'nowrap',
                        textOverflow: 'none'
                    },
                    formatter: function() {
                        return this.value;
                    }
                },
                tickInterval: 1,
                gridLineColor: '#d3d3d3',
                gridLineWidth: 0,
                offset: 0,
                min: 0,
                max: 4,
                opposite: false,
                height: vm.overviewLabHeight
            },
            {
                title: { text: '' },
                tickInterval: 1,
                gridLineColor: '#d3d3d3',
                labels: {
                    enabled: true,
                    x: 6,
                    y: 3,
                    zIndex: -1,
                    formatter: function() {
                       //Innan refaktorisering hårdkodar vi detta block
                        if(this.value==0)
                            return 'Sammantagen bedömning';
                        else if(this.value==1)
                            return 'ECOG-WHO';
                    },
                    style: {
                        whiteSpace: 'nowrap',
                        textOverflow: 'none'
                    }
                },
                offset: 0,
                min: 0,
                max: 1,
                startOnTick: false,
                endOnTick: false,
                opposite: true,
                plotBands: {
                    color: '#eeeeee',
                    zIndex: 0,
                    value: 0.5,
                    width: vm.overviewSBHeight * 1.8
                },
                reversed: true,
                showEmpty: true,
                top: vm.overviewLabHeight + vm.overviewLabPadding + vm.overviewAxisPadding,
                height: vm.overviewSBHeight
            },
            {
                title: { text: '' },
                tickInterval: 1,
                gridLineColor: '#d3d3d3',
                labels: {
                    enabled: true,
                    x: 6,
                    y: 3,
                    zIndex: -1,
                    formatter: function() {
                        //Innan refaktorisering hårdkodar vi detta block
                        if(this.value==2)
                            return 'Bilddiagnostik';
                        else if(this.value==3)
                            return 'Strålbehandling';
                        else if(this.value==4)
                            return 'SSE';
                        else if(this.value==5)
                            return 'Biobank';
                    },
                    style: {
                        whiteSpace: 'nowrap',
                        textOverflow: 'none'
                    }
                },
                offset: 0,
                min: 2,
                max: 5,
                startOnTick: false,
                endOnTick: false,
                opposite: true,
                reversed: true,
                showEmpty: true,
                top: vm.overviewLabHeight + vm.overviewLabPadding + vm.overviewSBHeight + vm.overviewSBPadding + vm.overviewAxisPadding + vm.overviewPROMHeight + vm.overviewPROMPadding,
                height: vm.overviewPlotHeight
            },
            {
                title: { text: '' },
                tickInterval: 1,
                gridLineColor: '#d3d3d3',
                labels: {
                    enabled: true,
                    x: 6,
                    y: 3,
                    zIndex: -1,
                    formatter: function() {
                        if (vm.overviewHighChartLabels[this.value]) {
                            return vm.overviewHighChartLabels[this.value];
                        }
                    },
                    style: {
                        whiteSpace: 'nowrap',
                        textOverflow: 'none'
                    }
                },
                offset: 0,
                maxPadding: 0.1,
                minPadding: 0.1,
                startOnTick: false,
                endOnTick: false,
                opposite: true,
                showEmpty: false,
                reversed: false,
                top: vm.overviewLabHeight + vm.overviewLabPadding + vm.overviewSBHeight + vm.overviewSBPadding + vm.overviewPlotHeight + vm.overviewPlotPadding + vm.overviewAxisPadding + vm.overviewPROMHeight + vm.overviewPROMPadding,
                height: vm.overviewLMHeight
            },
            {
                title: { text: '' },
                labels: {
                    enabled: true,
                    zIndex: -1,
                    style: {
                        color: '#000000',
                        whiteSpace: 'nowrap',
                        textOverflow: 'none'
                    },
                    formatter: function() {
                        return this.value;
                    }
                },
                gridLineColor: '#d3d3d3',
                gridLineWidth: 1,
                offset: 0,
                min: 0,
                opposite: true,
                height: vm.overviewLabHeight
            },
            {
                title: { text: '' },
                tickInterval: 1,
                gridLineColor: '#d3d3d3',
      	        labels: {
          				enabled: true,
          				x: 6,
          				y: 3,
                        zIndex: -1,
          				formatter: function() {
          					return 'PROM';
          				},
          				style: {
          					whiteSpace: 'nowrap',
                            textOverflow: 'none'
          				}
      	        },
                offset: 0,
                opposite: true,
                startOnTick: false,
                endOnTick: false,
                min: 15,
                max: 15,
                showEmpty: true,
                top: vm.overviewLabHeight + vm.overviewLabPadding + vm.overviewSBHeight + vm.overviewSBPadding + vm.overviewAxisPadding,
                height: vm.overviewPROMHeight
        	}
        ];

        /**
         * Extracted formatter for chart legend
         */
        var legendFormatter = function() {
            var legend = '<span>' + this.name + '</span>';

            if (this.name == 'PSA') {
                if (vm.senastePSA() != undefined) {
                    legend += ': ' + vm.senastePSA();
                }
            } else if (this.name == 'ALP') {
                if (vm.senasteALP() != undefined) {
                    legend += ': ' + vm.senasteALP();
                }
            } else if (this.name == 'Krea') {
                if (vm.senasteKrea() != undefined) {
                    legend += ': ' + vm.senasteKrea();
                }
            } else if (this.name == 'Hb') {
                if (vm.senasteHb() != undefined) {
                    legend += ': ' + vm.senasteHb();
                }
            } else if (this.name == 'Testo') {
                if (vm.senasteTesto() != undefined) {
                    legend += ': ' + vm.senasteTesto();
                }
            } else if (this.name == 'LPK') {
                if (vm.senasteLPK() != undefined) {
                    legend += ': ' + vm.senasteLPK();
                }
            } else if (this.name == 'TPK') {
                if (vm.senasteTPK() != undefined) {
                    legend += ': ' + vm.senasteTPK();
                }
            } else if (this.name == 'Neutrofila') {
                if (vm.senasteNeutrofila() != undefined) {
                    legend += ': ' + vm.senasteNeutrofila();
                }
            }
            
            return legend;
        }

        vm.overviewHighChart = new Highcharts.Chart({

            chart: highchartOptions.chart,
            credits: { enabled: false },
            title: { text: '' },
            xAxis: highchartOptions.xAxis,
            yAxis: highchartOptions.yAxis,
            legend: {
                borderWidth: 2,
                borderColor: '#ccc',
                borderRadius: 6,
                margin: 2,
                padding: 8,
                enabled: true,
                layout: 'vertical',
                align: 'right',
                width: 180,
                verticalAlign: 'top',
                labelFormatter: legendFormatter
            },
            tooltip: {
                hideDelay: 0,
                borderColor: null,
                backgroundColor: '#ffffff',
                headerFormat: '<strong>{series.name}</strong>',
                formatter: function() {
                    // check for läkemedel
                    if(this.point.tab === '7' && this.point.tooltip) {
                        var text = '<strong>' + this.series.name + '</strong>';

                        if (this.point.datum)
                            text = text + '<br />' + 'Datum: ' + this.point.datum;
                        if (this.point.handelse)
                            text = text + '<br />' + 'Händelse: ' + this.point.handelse;
                        if (this.point.orsak)
                            text = text + '<br />' + 'Orsak: ' + this.point.orsak;
                        if (this.point.antalbeh)
                            text = text + '<br />' + 'Antal givna behandlingar: ' + this.point.antalbeh;

                        return text;
                    } else if (!this.point.tooltip && this.series.name !== 'PROM pie') {
                        return false;
                    } else {
                        // bind to pointFormatter in series, means we can keep the formatting in the series
                        var pointFormatter = this.series.options.tooltip.pointFormatter.bind(this.point);
                        
                        // if PROM pie, use point.key as header
                        if(this.series.name == 'PROM pie') {
                            var text = '<strong>' + this.key + '</strong>';
                        } else {
                            var text = '<strong>' + this.series.name + '</strong>';
                        }
                        
                        return text += pointFormatter();
                    }
                },
                crosshairs: true,
                shared: false
            },
            // plotoptions can be moved to series
            plotOptions: {
                pie: {
                    dataLabels: { enabled: false },
                    slicedOffset: 0,
                    startAngle: 0,
                    endAngle: 360,
                    size: "11%"
          		},
                series: {
                    stickyTracking: false,
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: PointClick
                        }
                    }
                },
                line: {
                    lineWidth: 12,
                    cursor: 'pointer',
                    shadow: false,
                    marker: {
                        symbol: 'circle',
                        lineWidth: 2,
                        lineColor: '#FFFFFF',
                        radius: 6
                    },
                    events: {
                        hide: function(event) {
                        },
                        show: function(event) {
                            var legendItems = ['PSA', 'ALP', 'Krea', 'Hb', 'Testo', 'LPK', 'TPK', 'Neutrofila'];
                            var hideThese = [];

                            legendItems.forEach(function(item) {
                                if(event.target.name != item)
                                    hideThese.push(item);
                            });

                            hideThese.forEach(function(item) {
                                if(vm.overviewHighChart.get(item).visible) {
                                    vm.overviewHighChart.get(item).hide();
                                }
                            });

                            if(event.target.name === 'PSA') {
                                $('#PSA-log-lin').css("visibility", "visible");
                                $('#PSA-help').css("visibility", "visible");  
                                if(!vm.overviewHighChart.get('PSA').visible){
                                    vm.overviewHighChart.get('PSA dubblering').show();
                                    vm.overviewHighChart.get('PSA halvering').show();
                                }
                            } else {         
                                $('#PSA-log-lin').css("visibility", "hidden");
                                $('#PSA-help').css("visibility", "hidden");
                                vm.overviewHighChart.get('PSA dubblering').hide();
                                vm.overviewHighChart.get('PSA halvering').hide();
                            }

                            // vm.overviewHighChart.get(event.target.name).show();

                            // if (event.target.index == 2) {
                            //     if (!vm.overviewHighChart.series[12].visible || !vm.overviewHighChart.series[13].visible) {
                            //         vm.overviewHighChart.series[12].show();
                            //         vm.overviewHighChart.series[13].show();
                            //     }

                            //     $('#PSA-log-lin').css("visibility", "visible");
                            //     $('#PSA-help').css("visibility", "visible");

                            //     if (vm.overviewHighChart.series[3].visible)
                            //         vm.overviewHighChart.series[3].hide();
                            //     else if (vm.overviewHighChart.series[4].visible)
                            //         vm.overviewHighChart.series[4].hide();
                            //     else if (vm.overviewHighChart.series[5].visible)
                            //         vm.overviewHighChart.series[5].hide();
                            //     else if (vm.overviewHighChart.series[6].visible)
                            //         vm.overviewHighChart.series[6].hide();
                            // } else if (event.target.index == 3) {
                            //     $('#PSA-log-lin').css("visibility", "hidden");
                            //     $('#PSA-help').css("visibility", "hidden");
                                
                            //     if (vm.overviewHighChart.series[2].visible)
                            //         vm.overviewHighChart.series[2].hide();
                            //     if (vm.overviewHighChart.series[12].visible || vm.overviewHighChart.series[13].visible) {
                            //         vm.overviewHighChart.series[12].hide();
                            //         vm.overviewHighChart.series[13].hide();
                            //     } else if (vm.overviewHighChart.series[4].visible)
                            //         vm.overviewHighChart.series[4].hide();
                            //     else if (vm.overviewHighChart.series[5].visible)
                            //         vm.overviewHighChart.series[5].hide();
                            //     else if (vm.overviewHighChart.series[6].visible)
                            //         vm.overviewHighChart.series[6].hide();
                            // } else if (event.target.index == 4) {
                            //     $('#PSA-log-lin').css("visibility", "hidden");
                            //     $('#PSA-help').css("visibility", "hidden");
                                
                            //     if (vm.overviewHighChart.series[2].visible)
                            //         vm.overviewHighChart.series[2].hide();
                            //     if (vm.overviewHighChart.series[12].visible || vm.overviewHighChart.series[13].visible) {
                            //         vm.overviewHighChart.series[12].hide();
                            //         vm.overviewHighChart.series[13].hide();
                            //     } else if (vm.overviewHighChart.series[3].visible)
                            //         vm.overviewHighChart.series[3].hide();
                            //     else if (vm.overviewHighChart.series[5].visible)
                            //         vm.overviewHighChart.series[5].hide();
                            //     else if (vm.overviewHighChart.series[6].visible)
                            //         vm.overviewHighChart.series[6].hide();
                            // } else if (event.target.index == 5) {
                            //     $('#PSA-log-lin').css("visibility", "hidden");
                            //     $('#PSA-help').css("visibility", "hidden");
                                
                            //     if (vm.overviewHighChart.series[2].visible)
                            //         vm.overviewHighChart.series[2].hide();
                            //     if (vm.overviewHighChart.series[12].visible || vm.overviewHighChart.series[13].visible) {
                            //         vm.overviewHighChart.series[12].hide();
                            //         vm.overviewHighChart.series[13].hide();
                            //     } else if (vm.overviewHighChart.series[3].visible)
                            //         vm.overviewHighChart.series[3].hide();
                            //     else if (vm.overviewHighChart.series[4].visible)
                            //         vm.overviewHighChart.series[4].hide();
                            //     else if (vm.overviewHighChart.series[6].visible)
                            //         vm.overviewHighChart.series[6].hide();
                            // } else if (event.target.index == 6) {
                            //     $('#PSA-log-lin').css("visibility", "hidden");
                            //     $('#PSA-help').css("visibility", "hidden");

                            //     if (vm.overviewHighChart.series[2].visible)
                            //         vm.overviewHighChart.series[2].hide();
                            //     if (vm.overviewHighChart.series[12].visible || vm.overviewHighChart.series[13].visible) {
                            //         vm.overviewHighChart.series[12].hide();
                            //         vm.overviewHighChart.series[13].hide();
                            //     } else if (vm.overviewHighChart.series[3].visible)
                            //         vm.overviewHighChart.series[3].hide();
                            //     else if (vm.overviewHighChart.series[4].visible)
                            //         vm.overviewHighChart.series[4].hide();
                            //     else if (vm.overviewHighChart.series[5].visible)
                            //         vm.overviewHighChart.series[5].hide();
                            // }
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        align: 'center',
                        style: {
                            textOutline: false
                        },
                        formatter: function() {
                            return this.point.options && this.point.options.label;
                        }
                    }
                },
                scatter: {
                    marker: {
                        symbol: 'circle',
                        lineWidth: 2,
                        lineColor: '#FFFFFF',
                        radius: 6
                    },
                    stickyTracking: false
                }
            },
            series: [
                {
                    name: 'Brytpunktssamtal',
                    id: 'Brytpunktssamtal',
                    type: 'scatter',
                    yAxis: 2,
                    marker: {
                        enabled: false
                    },
                    showInLegend: false,
                    data: []
                },
                {
                    name: 'CRPC datum',
                    id: 'CRPC datum',
                    type: 'scatter',
                    yAxis: 2,
                    marker: {
                        enabled: false
                    },
                    showInLegend: false,
                    data: []
                },
                {
                    name: 'PSA',
                    id: 'PSA',
                    type: 'line',
                    color: '#000080',
                    visible: true,
                    marker: {
                        enabled: true,
                        radius: 5
                    },
                    tooltip: {
                        pointFormatter: function() {
                            var text = '';

                            if(this.datum)
                                text += '<br>Datum: ' + this.datum;
                            
                            if(this.y)
                                text += '<br>' + 'Värde: ' + this.y + ' [µg/L]';

                            return text;
                        }
                    },
                    legendIndex: 0,
                    shadow: false,
                    yAxis: 5,
                    lineWidth: 0.75,
                    point: {
                        events: {
                            click: PSAClick
                        }
                    }
                },
                {
                    name: 'ALP',
                    id: 'ALP',
                    type: 'line',
                    color: '#008080',
                    visible: false,
                    marker: {
                        enabled: true,
                        radius: 5
                    },
                    tooltip: {
                        pointFormatter: function() {
                            var text = '';

                            if (this.datum)
                                text = text + '<br />' + 'Datum: ' + this.datum;
                            if (this.y)
                                text = text + '<br />' + 'Värde: ' + this.y + ' [µkat/L]';

                            return text;
                        }
                    },
                    legendIndex: 2,
                    shadow: false,
                    yAxis: 0,
                    lineWidth: 0.75
                },
                {
                    name: 'Krea',
                    id: 'Krea',
                    type: 'line',
                    color: '#006400',
                    visible: false,
                    marker: {
                        enabled: true,
                        radius: 5
                    },
                    tooltip: {
                        pointFormatter: function() {
                            var text = '';

                            if (this.datum)
                                text = text + '<br />' + 'Datum: ' + this.datum;
                            if (this.y)
                                text = text + '<br />' + 'Värde: ' + this.y + ' [µmol/L]';

                            return text;
                        }
                    },
                    legendIndex: 1,
                    shadow: false,
                    yAxis: 0,
                    lineWidth: 0.75
                },
                {
                    name: 'Hb',
                    id: 'Hb',
                    type: 'line',
                    color: '#800000',
                    visible: false,
                    marker: {
                        enabled: true,
                        radius: 5
                    },
                    tooltip: {
                        pointFormatter: function() {
                            var text = '';

                            if (this.datum)
                                text = text + '<br />' + 'Datum: ' + this.datum;
                            if (this.y)
                                text = text + '<br />' + 'Värde: ' + this.y + ' [g/L]';

                            return text;
                        }
                    },
                    legendIndex: 3,
                    shadow: false,
                    yAxis: 0,
                    lineWidth: 0.75
                },
                {
                    name: 'Testo',
                    id: 'Testo',
                    type: 'line',
                    color: '#696969',
                    visible: false,
                    marker: {
                        enabled: true,
                        radius: 5
                    },
                    tooltip: {
                        pointFormatter: function() {
                            var text = '';

                            if (this.datum)
                                text = text + '<br />' + 'Datum: ' + this.datum;
                            if (this.y)
                                text = text + '<br />' + 'Värde: ' + this.y + ' [nmol/L]';

                            return text;
                        }
                    },
                    legendIndex: 7,
                    shadow: false,
                    yAxis: 0,
                    lineWidth: 0.75
                },
                {
                    name: 'ECOG-WHO',
                    id: 'ECOG-WHO',
                    color: '#d3d3d3',
                    type: 'scatter',
                    yAxis: 2,
                    marker: {
                        symbol: 'square',
                        radius: 8,
                        lineWidth: 0
                    },
                    dataLabels: {
                        enabled: true,
                        allowOverlap: true,
                        align: 'center',
                        verticalAlign: 'top',
                        padding: 0,
                        margin: 0,
                        x: -1,
                        y: -7,
                        style: {
                            textOutline: false
                        },
                        formatter: function() {
                            var text = '';
                            
                            if (this.point.ecogwholabel)
                                text = text + this.point.ecogwholabel;

                            return text;
                        }
                    },
                    tooltip: {
                        pointFormatter: function() {
                            var text = '';

                            if (this.datum)
                                text = text + '<br />' + 'Datum: ' + this.datum;
                            if (this.ecogwholabel)
                                text = text + '<br />' + this.ecogwholabel;

                            return text;
                        }
                    },
                    showInLegend: false,
                    lineWidth: 0
                },
                {
                    name: 'Sammantagen bedömning',
                    id: 'Sammantagen bedömning',
                    type: 'scatter',
                    yAxis: 2,
                    marker: {
                        symbol: 'triangle',
                        radius: 6
                    },
                    tooltip: {
                        pointFormatter: function() {
                            var text = '';

                            if (this.datum)
                                text = text + '<br />' + 'Datum: ' + this.datum;
                            if (this.klinbed)
                                text = text + '<br />' + this.klinbed;

                            return text;
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        allowOverlap: true,
                        align: 'center',
                        verticalAlign: 'top',
                        padding: 0,
                        margin: 0,
                        x: -1,
                        y: 5,
                        style: {
                            textOutline: false
                        },
                        formatter: function() {
                            var text = '';
                            if (this.point.klinbedlabel) {
                                text = this.klinbedlabel;
                            }
                            return text;
                        }
                    },
                    showInLegend: false,
                    data: []
                },
                {
                    name: 'Bilddiagnostik',
                    id: 'Bilddiagnostik',
                    color: '#484848',
                    type: 'scatter',
                    yAxis: 3,
                    marker: {
                        symbol: 'triangle-down',
                        radius: 6
                    },
                    tooltip: {
                        pointFormatter: function() {
                            var text = '';

                            if (this.datum)
                                text = text + '<br />' + 'Datum: ' + this.datum;

                            if (this.resultat.length > 0) {
                                $.each(this.resultat, function(i, resultat) {
                                    if (resultat.typ)
                                        text = text + '<br />' + resultat.typ;
                                    if (resultat.radbed)
                                        text = text + '<br />' + resultat.radbed;
                                    if (resultat.metastaser.length > 0) {
                                        text = text + '<br />' + 'Nytillkomna metastaser:' + '<br />';

                                        $.each(resultat.metastaser, function(i, metastas) {
                                            text = text + metastas + '<br />'
                                        });
                                    } else {
                                        text = text + '<br />' + '<i>Inga nytillkomna metastaser registrerade.</i>';
                                    }
                                });
                            }

                            return text;
                        }
                    },
                    showInLegend: false,
                    data: []
                },
                {
                    name: 'Strålbehandling',
                    id: 'Strålbehandling',
                    color: '#484848',
                    type: 'scatter',
                    yAxis: 3,
                    marker: {
                        symbol: 'square',
                        radius: 5
                    },
                    tooltip: {
                        pointFormatter: function() {
                            var text = '';

                            if (this.slutdatum)
                                text = text + '<br />' + 'Slutdatum: ' + this.slutdatum;

                            if (this.grupper.length > 0) {
                                $.each(this.grupper, function(i, grupp) {
                                    if (grupp.frakdos)
                                        text = text + '<br />' + 'Dos per fraktion (Gy): ' + grupp.frakdos;
                                    if (grupp.slutdos)
                                        text = text + '<br />' + 'Slutdos (Gy): ' + grupp.slutdos;
                                    if (grupp.lokaler.length > 0) {
                                        text = text + '<br />' + 'Lokaler:';

                                        $.each(grupp.lokaler, function(i, lokal) {
                                            text = text + '<br />' + lokal;
                                        });
                                    } else
                                        text = text + '<br />' + '<i>Inga lokaler registrerade.</i>';
                                });
                            }

                            return text;
                        }
                    },
                    showInLegend: false,
                    data: []
                },
                {
                    name: 'SSE',
                    id: 'SSE',
                    color: '#484848',
                    type: 'scatter',
                    yAxis: 3,
                    marker: {
                        symbol: 'diamond',
                        radius: 6
                    },
                    tooltip: {
                        pointFormatter: function() {
                            var text = '';

                            if (this.datum)
                                text = text + '<br />' + 'Datum: ' + this.datum;

                            if (this.handelser.length > 0) {
                                text = text + '<br />' + 'Händelser:';

                                $.each(this.handelser, function(i, handelse) {
                                    text = text + '<br />' + handelse;
                                });
                            } else {
                                text = text + '<br />' + '<i>Inga händelser registrerade.</i>';
                            }

                            return text;
                        }
                    },
                    showInLegend: false,
                    data: []
                },
                {
                    name: 'PSA dubblering',
                    id: 'PSA dubblering',
                    type: 'line',
                    color: '#ff0000',
                    visible: true,
                    marker: {
                        symbol: 'diamond',
                        radius: 8
                    },
                    tooltip: {
                        pointFormatter: function() {
                            var text = '';

                            if (this.dubbleringstid) {
                                text = text + '<br /><i>' + 'PSA dubblering resultat:' + '</i>';
                                text = text + '<br />' + 'Dubbleringstid: ' + (this.dubbleringstid / 365.24).toFixed(2) + ' år, eller ' + Math.round(this.dubbleringstid / 30.44).toFixed(2) + ' månader, eller ' + Math.round(this.dubbleringstid) + ' dagar';
                            }

                            if (this.datum)
                                text = text + '<br />' + 'Dubbleringsdatum: ' + this.datum;
                            
                            if (this.y)
                                text = text + '<br />' + 'Dubbleringsvärde: ' + this.y + ' [µg/L]';

                            return text;
                        }
                    },
                    showInLegend: false,
                    shadow: false,
                    yAxis: 5,
                    lineWidth: 0.75,
                    zIndex: 15
                },
                {
                    name: 'PSA halvering',
                    id: 'PSA halvering',
                    type: 'line',
                    color: '#00ff00',
                    visible: true,
                    marker: {
                        symbol: 'diamond',
                        radius: 8
                    },
                    tooltip: {
                        pointFormatter: function() {
                            var text = '';

                            if (this.halveringstid) {
                                text = text + '<br /><i>' + 'PSA halvering resultat:' + '</i>';
                                text = text + '<br />' + 'Halveringstid: ' + (this.halveringstid / 365.24).toFixed(2) + ' år, eller ' + Math.round(this.halveringstid / 30.44).toFixed(2) + ' månader,  eller ' + Math.round(this.halveringstid) + ' dagar';
                            }
                            if (this.datum)
                                text = text + '<br />' + 'Halveringsdatum: ' + this.datum;
                            if (this.y)
                                text = text + '<br />' + 'Halveringsvärde: ' + this.y + ' [µg/L]';

                            return text;
                        }
                    },
                    showInLegend: false,
                    shadow: false,
                    yAxis: 5,
                    lineWidth: 0.75,
                    zIndex: 20
                },
                {
                    name: 'Datum första metastas',
                    id: 'Datum första metastas',
                    type: 'scatter',
                    yAxis: 2,
                    marker: {
                        enabled: false
                    },
                    showInLegend: false,
                    data: []
                },
                {
                    name: 'PROM enkäter',
                    id: 'PROM enkäter',
                    type: 'scatter',
                    yAxis: 6,
                    marker: {
                        symbol: 'circle',
                        radius: 9
                    },
                    point: {
                        events: {
                            click: PROMClick
                        }
                    },
                    tooltip: {
                        pointFormatter: function() {
                            var text = '';

                            if(this.datum)
                                text = text + '<br />' + 'Datum för inmatning: ' + this.datum;
                            if(this.label !== "")
                                text = text + '<br />' + 'Uppskattad livskvalitet: ' + this.label;

                            return text;
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        allowOverlap: true,
                        align: 'center',
                        verticalAlign: 'top',
                        padding: 0,
                        margin: 0,
                        style: {
                            color: '#222',
                            textOutline: false
                        },
                        x: 0,
                        y: -8,
                        formatter: function() {
                            var text = '';
                            
                            if(this.point.label !==	"") {
                                text = this.point.label;
                            }
                            
                            return text;
                        }
                    },
                    showInLegend: false,
                    data: []
                },
                {
                    name: 'PROM pie',
                    id: 'PROM pie',
                    type: 'pie',
                    innerSize: '55%',
                    yAxis: 6,
                    zIndex: 5,
                    tooltip: {
                        headerFormat: '<strong>{point.key}</strong><br>',
                        pointFormatter: function() {
                            var text = '';

                            if(this.value == 0) {
                                text = text + "<br>" + "Fråga obesvarad";
                            } else {
                                text = text + '<br>' + 'Frågesvar: ';
                                
                                if(this.value == 1) {
                                    text = text + this.value + " - Inte alls";
                                } else if(this.value == 2) {
                                    text = text + this.value + " - Lite";
                                } else if(this.value == 3) {
                                    text = text + this.value + " - En hel del";
                                } else if(this.value == 4) {
                                    text = text + this.value + " - Mycket";
                                }
                                
                                if(this.extra) {
                                    text = text + "<br>" + "VAS smärta: " + this.extra;
                                }
                            }

                            return text;
                        }
                    },
                    showInLegend: false
                },
                {
                    name: 'LPK',
                    id: 'LPK',
                    type: 'line',
                    color: '#aaaaff',
                    visible: false,
                    marker: {
                        enabled: true,
                        radius: 5
                    },
                    tooltip: {
                        pointFormatter: function() {
                            var text = '';

                            if (this.datum)
                                text = text + '<br />' + 'Datum: ' + this.datum;
                            if (this.y)
                                text = text + '<br />' + 'Värde: ' + this.y + ' [10^9/L]';

                            return text;
                        }
                    },
                    legendIndex: 4,
                    shadow: false,
                    yAxis: 0,
                    lineWidth: 0.75
                },
                {
                    name: 'TPK',
                    id: 'TPK',
                    type: 'line',
                    color: '#fa77fa',
                    visible: false,
                    marker: {
                        enabled: true,
                        radius: 5
                    },
                    tooltip: {
                        pointFormatter: function() {
                            var text = '';

                            if (this.datum)
                                text = text + '<br />' + 'Datum: ' + this.datum;
                            if (this.y)
                                text = text + '<br />' + 'Värde: ' + this.y + ' [10^9/L]';

                            return text;
                        }
                    },
                    legendIndex: 6,
                    shadow: false,
                    yAxis: 0,
                    lineWidth: 0.75
                },
                {
                    name: 'Neutrofila',
                    id: 'Neutrofila',
                    type: 'line',
                    color: '#00ff00',
                    visible: false,
                    marker: {
                        enabled: true,
                        radius: 5
                    },
                    tooltip: {
                        pointFormatter: function() {
                            var text = '';

                            if (this.datum)
                                text = text + '<br />' + 'Datum: ' + this.datum;
                            if (this.y)
                                text = text + '<br />' + 'Värde: ' + this.y + ' [10^9/L]';

                            return text;
                        }
                    },
                    legendIndex: 5,
                    shadow: false,
                    yAxis: 0,
                    lineWidth: 0.75
                }, {
                    name: 'Biobank',
                    id: 'Biobank',
                    color: '#484848',
                    type: 'scatter',
                    yAxis: 3,
                    marker: {
                        symbol: 'url(../../Public/Files/uppsala/common-libs/icons/sample.gif)',
                    },
                    tooltip: {
                        pointFormatter: function() {
                            var text = '';

                            if (this.datum)
                                text = text + '<br />' + 'Datum: ' + this.datum;
                            if (this.provtyper.length > 0) {
                                text = text + '<br />' + 'Provtyper:';

                                $.each(this.provtyper, function(i, provtyp) {
                                    text = text + '<br />' + provtyp;
                                });
                            } else {
                                text = text + '<br />' + '<i>Inga provtyper registrerade.</i>';
                            }
                            return text;
                        }
                    },
                    showInLegend: false,
                    data: []
                },
            ]
        });
    };

    /* CLICK-EVENTS */
    PSAClick = function(event) {
        var self = this;

        var noKeys = (!event.ctrlKey || !event.shiftKey || !event.altKey);

        if ((event.ctrlKey || event.shiftKey) && self.options && self.options.psa) {
            var index = $.inArray(self.x, self.series.xData); //self.series.xData.indexOf(self.x);
            
            if (index < self.series.data.length - 2) {
                var psa1 = self.y;
                var psa1datum = self.x;

                var psa2 = "";
                var psa2datum;

                var psa3 = "";
                var psa3datum;

                var oneDay = 24 * 60 * 60 * 1000; // en dagsranson av millisekunder

                try {
                    while (psa2 == "") {
                        index++;
                        if (index == self.series.data.length) {
                            throw "Det finns för få värden efter valt startvärde som uppfyller kriterierna för att räkna ut PSA-dubbleringstid";
                        }
                        if (self.series.yData[index] >= psa1) {
                            psa2 = self.series.yData[index];
                            psa2datum = self.series.xData[index];
                        }
                    }

                    while (psa3 == "") {
                        index++;
                        if (index == self.series.data.length) {
                            throw "Det finns för få värden efter valt startvärde som uppfyller kriterierna för att räkna ut PSA-dubbleringstid";
                        }
                        if (self.series.yData[index] >= psa2) {
                            psa3 = self.series.yData[index];
                            psa3datum = self.series.xData[index];

                            var datediff = Math.round(Math.abs((psa1datum - psa3datum) / (oneDay)));
                            if (datediff <= 90) {
                                psa3 = "";
                            }
                        }
                    }
                } catch (err) {
                    alert(err);
                    return;
                }

                // Ia delen i uträkningen
                var psa1log = Math.log(psa1);
                var psa2log = Math.log(psa2);
                var psa3log = Math.log(psa3);

                var tid1 = 0;
                var tid2 = psa2datum - psa1datum;
                var tid3 = psa3datum - psa1datum;

                var tid1sqr = tid1 * tid1;
                var tid2sqr = tid2 * tid2;
                var tid3sqr = tid3 * tid3;

                var Iasumma = (tid1sqr * psa1) + (tid2sqr * psa2) + (tid3sqr * psa3);

                // IIa delen i uträkningen
                var IIasumma = (psa1 * psa1log) + (psa2 * psa2log) + (psa3 * psa3log);

                // IIIa delen i uträkningen
                var IIIasumma = (tid1 * psa1) + (tid2 * psa2) + (tid3 * psa3);

                // IVa delen i uträkningen
                var IVasumma = (tid1 * psa1 * psa1log) + (tid2 * psa2 * psa2log) + (tid3 * psa3 * psa3log);

                // Va delen i uträkningen
                var Vasumma = psa1 + psa2 + psa3;

                // Uträkningen
                var a = ((Iasumma * IIasumma) - (IIIasumma * IVasumma)) / ((Vasumma * Iasumma) - (IIIasumma * IIIasumma));

                var b = ((Vasumma * IVasumma) - (IIIasumma * IIasumma)) / ((Vasumma * Iasumma) - (IIIasumma * IIIasumma));

                //Formel för uträkning av y : ln(y) = a+(b*x) => x = (ln(y)-a)/b
                var y = psa1 * 2;
                var x = (Math.log(y) - a) / b;

                var psadubbleringsdatum = psa1datum + x;
                //vm.U_psadubblering(psa1datum + x);

                redrawOverviewPSADubblering(psa1, psa2, psa3, psa1datum, psa2datum, psa3datum, psadubbleringsdatum, x);

            } else {
                alert('färre än tre psa tagna från det valda startvärdet');
                // getinfodialog("kan inte räkna ut psa-dubblering då det inte finns tre värden efter valt värde")
            }
        } else if (event.altKey && self.options && self.options.psa) {
            var index = $.inArray(self.x, self.series.xData); //self.series.xData.indexOf(self.x);
            if (index < self.series.data.length - 1) {
                var psa1 = self.y;
                var psa1datum = self.x;

                try {
                    var psa2 = "";
                    var psa2datum;

                    index++;
                    if (self.series.yData[index] < psa1) {
                        psa2 = self.series.yData[index];
                        psa2datum = self.series.xData[index];
                    }

                    index++;
                    if (psa2 == "" && index < self.series.data.length && self.series.yData[index] < psa1) {
                        psa2 = self.series.yData[index];
                        psa2datum = self.series.xData[index];
                    }

                    if (psa2 == "") {
                        throw 'Kan inte räkna ut psa-halvering då det inte finns ett fallande värde direkt efter valt värde';
                    }

                } catch (err) {
                    alert(err);
                    return;
                }

                var slope = (psa1 - psa2) / (psa1datum - psa2datum);

                var x = ((psa1 / 2) - psa1) / slope;

                var halveringsdatum = psa1datum + x;

                redrawOverviewPSAhalvering(psa1, psa2, psa1datum, psa2datum, halveringsdatum, x);

            } else {
                alert('Kan inte räkna ut psa-halvering då det inte finns några värden efter valt värde');
            }
        } else if (noKeys) {
            PointClick(event, self);
        }
    };

    PROMClick = function(event) {
        var self = this;

        if((event.ctrlKey || event.shiftKey) && self.y == 15) {
            redrawOverviewPROMPie(self.values, self.plotX, vm.overviewLabHeight + vm.overviewLabPadding + vm.overviewPlotHeight + vm.overviewPlotPadding + vm.overviewAxisPadding-vm.overviewPROMPadding);
        } else if(event.altKey && self.y == 15) {
            redrawOverviewPROMPie();
        }
    }

    PointClick = function(event, self) {
        if(!self) 
            var self = this;
        
        var noKeys = (!event.ctrlKey || !event.shiftKey || !event.altKey);

        if (noKeys && self.options && (self.options.psadbl || self.options.psahlv)) {
            self.series.setData([]);
        } else if(noKeys) {
            $('#prostataprocess-tabs').tabs('option', 'active', self.options.tab - 1);
            
            var active = $('#prostataprocess-tabs').tabs('option', 'active') + 1;
            var subactive = "";
            var i = "";
            
            switch (active) {
                case 2:

                    $('#B-accordion h3 a').each(function(index, elem) {
                        var datum = $(elem).children('span').first().text();

                        if (datum == self.options.datum) {
                            i = index;
                        }

                    });

                    if (i != "") {
                        $('#B-accordion').accordion("option", "active", i);
                    }
                    break;

                case 3:

                    $('#BD-accordion h3 a').each(function(index, elem) {
                        var datum = $(elem).children('span').first().text();

                        if (datum == self.options.datum) {
                            i = index;
                        }

                    });

                    if (i != "") {
                        $('#BD-accordion').accordion("option", "active", i);
                    }
                    break;

                case 4:

                    $('#SB-accordion h3 a').each(function(index, elem) {
                        var datum = $(elem).children('span').first().text();

                        if (datum == self.options.datum) {
                            i = index;
                        }

                    });

                    if (i != "") {
                        $('#SB-accordion').accordion("option", "active", i);
                    }
                    break;

                case 5:

                    $('#SE-accordion h3 a').each(function(index, elem) {
                        var datum = $(elem).children('span').first().text();

                        if (datum == self.options.datum) {
                            i = index;
                        }

                    });

                    if (i != "") {
                        $('#SE-accordion').accordion("option", "active", i);
                    }
                    break;

                case 7:

                    $('#prostataprocess-lmtabs ul li a span.rcc-small span').each(function(index, elem) {
                        var datum = $(elem).text();

                        if (datum == self.options.startdatum) {
                            i = index;
                        }

                    });

                    if (i != "") {
                        $('#prostataprocess-lmtabs').tabs("option", "active", i);
                    }
                    break;

                default:
                    break;
            }
        }
    }

    /* REDRAW FUNKTIONER */

    /* OVERVIEW */


    /**
     * Funktion för uppdatering av Root data för Highcharts
     *
     * @method redrawOverviewRootChart
     */
    redrawOverviewRootChart = function(vm) {
        if (vm.U_crpcdatum()) {
            var crpcDatumsplit = vm.U_crpcdatum().split("-");
            // remove old crpc plotline
            vm.overviewHighChart.xAxis[0].removePlotLine('crpc-plotline');
            // add new crpc plotline
            vm.overviewHighChart.xAxis[0].addPlotLine({
                id: 'crpc-plotline',
                value: Date.UTC(crpcDatumsplit[0], (crpcDatumsplit[1] - 1), (crpcDatumsplit[2])),
                width: 1,
                color: '#92a2bb',
                dashStyle: 'shortdash',
                zIndex: 2,
                label: {
                    align: 'left',
                    textAlign: 'left',
                    x: 5,
                    rotation: 0,
                    style: {
                        fontSize: '10px',
                        color: '#92a2bb'
                    },
                    text: 'CRPC Resistent<br>' + vm.U_crpcdatum()
                }
            });
            // add invisible point to crpc series
            vm.overviewHighChart.series[1].setData([{
                x: Date.UTC(crpcDatumsplit[0], (crpcDatumsplit[1] - 1), crpcDatumsplit[2]),
                y: 0
            }], false);
        } else {
            vm.overviewHighChart.xAxis[0].removePlotLine('crpc-plotline');
        }
        if (vm.U_metastasdatum()) {
            var metastasDatumsplit = vm.U_metastasdatum().split("-");
            // remove old metastas plotline
            vm.overviewHighChart.xAxis[0].removePlotLine('metastas-plotline');
            // add new mestastas plotline
            vm.overviewHighChart.xAxis[0].addPlotLine({
                id: 'metastas-plotline',
                value: Date.UTC(metastasDatumsplit[0], (metastasDatumsplit[1] - 1), (metastasDatumsplit[2])),
                width: 1,
                color: '#cd8448',
                dashStyle: 'shortdash',
                zIndex: 2,
                label: {
                    align: 'right',
                    textAlign: 'right',
                    x: -5,
                    rotation: 0,
                    style: {
                        fontSize: '10px',
                        color: '#db9356'
                    },
                    text: 'Första metastas<br>' + vm.U_metastasdatum()
                }
            });
            // add invisible point to metastas series
            vm.overviewHighChart.series[14].setData([{
                x: Date.UTC(metastasDatumsplit[0], (metastasDatumsplit[1] - 1), metastasDatumsplit[2]),
                y: 0
            }], false);
        } else {
            vm.overviewHighChart.xAxis[0].removePlotLine('metastas-plotline');
        }
    };

    /* LAB */

    /**
     * Funktion för uppdatering av  Labbprov, PSA data för Highcharts
     *
     * @method redrawLabPSAChart
     */
    redrawOverviewPSAChart = function(vm) {
        var data = [];

        ko.utils.arrayForEach(vm.sortTable(vm.$$.Labbprov, 'LP_datum', 'ascending'), function(item) {
            var datum = item.LP_datum();
            var value = item.LP_psa();

            if (value) {
                if (typeof value == 'string')
                    value = parseFloat(value.replace(',', '.'));

                if (datum) {
                    var datumsplit = datum.split("-");
                    if (!isNaN(value))
                        data.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: value,
                            tab: '6',
                            psa: true,
                            datum: datum,
                            tooltip: true
                        });
                    else
                        data.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: 0,
                            tab: '6',
                            psa: true,
                            datum: datum,
                            tooltip: true
                        });
                }
            }


        });

        if (vm.overviewHighChart) {
            vm.overviewHighChart.series[2].setData(data, false);

            vm.modifiedPSA(true);

        }
    };

    /**
     * Funktion för uppdatering av PSA-dubbleringstid
     *
     * @method redrawOverviewPSADubblering
     */
    redrawOverviewPSADubblering = function(psa1, psa2, psa3, psa1datum, psa2datum, psa3datum, psadubbleringsdatum, psadubbleringstid) {
        var data = [];

        if (psadubbleringstid > 0) {

            var psaData = [{
                datum: psa1datum,
                psa: psa1
            }, {
                datum: psa2datum,
                psa: psa2
            }, {
                datum: psa3datum,
                psa: psa3
            }, {
                datum: psadubbleringsdatum,
                psa: psa1 * 2
            }];

            var sortedPsaData = psaData.slice(0).sort(function(a, b) {
                var va = (a.datum == null) ? "9999-99-99" : a.datum;
                var vb = (b.datum == null) ? "9999-99-99" : b.datum;
                return va == vb ? 0 : (va < vb ? -1 : 1);
            });

            ko.utils.arrayForEach(sortedPsaData, function(item) {
                if (item.datum != psadubbleringsdatum) {
                    data.push({
                        x: item.datum,
                        y: item.psa,
                        datum: new Date(item.datum).getFullYear() + '-' + (('' + (new Date(item.datum).getMonth() + 1)).length < 2 ? '0' : '') + (new Date(item.datum).getMonth() + 1) + '-' + (('' + new Date(item.datum).getDate()).length < 2 ? '0' : '') + new Date(item.datum).getDate(),
                        startpsa: psa1,
                        datum1: new Date(psa1datum).toJSON().slice(0, 10),
                        datum2: new Date(psa2datum).toJSON().slice(0, 10),
                        datum3: new Date(psa3datum).toJSON().slice(0, 10),
                        dubbleringstid: psadubbleringstid / (24 * 60 * 60 * 1000),
                        psadbl: true,
                        color: '#FF0000',
                        marker: {
                            radius: 4,
                            fillColor: '#FF0000',
                            lineColor: '#FF2222',
                            lineWidth: 0,
                            states: {
                                hover: {
                                    radius: 4,
                                    fillColor: '#FF0000',
                                    lineColor: '#FF2222',
                                    lineWidth: 2
                                }
                            }
                        },
                        tooltip: true
                    });
                    //data.push({x: psa2datum, y: psa2, datum: new Date(psa2datum).getFullYear() + '-' + ((''+(new Date(psa2datum).getMonth()+1)).length < 2 ? '0' : '') + (new Date(psa2datum).getMonth() + 1) + '-' + ((''+new Date(psa2datum).getDate()).length < 2 ? '0' : '') + new Date(psa2datum).getDate(), startpsa: psa1, datum1: new Date(psa1datum).toJSON().slice(0,10), datum2: new Date(psa2datum).toJSON().slice(0,10), datum3: new Date(psa3datum).toJSON().slice(0,10), dubbleringstid: psadubbleringstid/(24*60*60*1000), psadbl: true, color: '#FF0000', marker: { radius: 4, fillColor: '#FF0000', lineColor: '#FF2222', lineWidth: 0, states: { hover: { radius: 4, fillColor: '#FF0000', lineColor: '#FF2222', lineWidth: 2}}}, tooltip: true});
                    //data.push({x: psa3datum, y: psa3, datum: new Date(psa3datum).getFullYear() + '-' + ((''+(new Date(psa3datum).getMonth()+1)).length < 2 ? '0' : '') + (new Date(psa3datum).getMonth() + 1) + '-' + ((''+new Date(psa3datum).getDate()).length < 2 ? '0' : '') + new Date(psa3datum).getDate(), startpsa: psa1, datum1: new Date(psa1datum).toJSON().slice(0,10), datum2: new Date(psa2datum).toJSON().slice(0,10), datum3: new Date(psa3datum).toJSON().slice(0,10), dubbleringstid: psadubbleringstid/(24*60*60*1000), psadbl: true, color: '#FF0000', marker: { radius: 4, fillColor: '#FF0000', lineColor: '#FF2222', lineWidth: 0, states: { hover: { radius: 4, fillColor: '#FF0000', lineColor: '#FF2222', lineWidth: 2}}}, tooltip: true});
                } else {
                    data.push({
                        x: item.datum,
                        y: item.psa,
                        datum: new Date(item.datum).getFullYear() + '-' + (('' + (new Date(item.datum).getMonth() + 1)).length < 2 ? '0' : '') + (new Date(item.datum).getMonth() + 1) + '-' + (('' + new Date(item.datum).getDate()).length < 2 ? '0' : '') + new Date(item.datum).getDate(),
                        startpsa: psa1,
                        datum1: new Date(psa1datum).toJSON().slice(0, 10),
                        datum2: new Date(psa2datum).toJSON().slice(0, 10),
                        datum3: new Date(psa3datum).toJSON().slice(0, 10),
                        dubbleringstid: psadubbleringstid / (24 * 60 * 60 * 1000),
                        psadbl: true,
                        tooltip: true
                    });
                }
            });
        }

        if (vm.overviewHighChart) {
            vm.overviewHighChart.series[12].setData(data, true);
        }
    };

    /**
     * Funktion för uppdatering av PSA-halveringstid
     *
     * @method redrawOverviewPSAhalvering
     */
    redrawOverviewPSAhalvering = function(psa1, psa2, psa1datum, psa2datum, psahalveringsdatum, psahalveringstid) {
        var data = [];

        if (psahalveringstid > 0) {
            var psaData = [{
                datum: psa1datum,
                psa: psa1
            }, {
                datum: psa2datum,
                psa: psa2
            }, {
                datum: psahalveringsdatum,
                psa: psa1 / 2
            }];

            var sortedPsaData = psaData.slice(0).sort(function(a, b) {
                var va = (a.datum == null) ? "9999-99-99" : a.datum;
                var vb = (b.datum == null) ? "9999-99-99" : b.datum;
                return va == vb ? 0 : (va < vb ? -1 : 1);
            });

            ko.utils.arrayForEach(sortedPsaData, function(item) {
                if (item.datum != psahalveringsdatum) {
                    data.push({
                        x: item.datum,
                        y: item.psa,
                        datum: new Date(item.datum).getFullYear() + '-' + (('' + (new Date(item.datum).getMonth() + 1)).length < 2 ? '0' : '') + (new Date(item.datum).getMonth() + 1) + '-' + (('' + new Date(item.datum).getDate()).length < 2 ? '0' : '') + new Date(item.datum).getDate(),
                        startpsa: psa1,
                        datum1: new Date(psa1datum).toJSON().slice(0, 10),
                        datum2: new Date(psa2datum).toJSON().slice(0, 10),
                        halveringstid: psahalveringstid / (24 * 60 * 60 * 1000),
                        psahlv: true,
                        color: '#00FF00',
                        marker: {
                            radius: 4,
                            fillColor: '#00FF00',
                            lineColor: '#22FF22',
                            lineWidth: 0,
                            states: {
                                hover: {
                                    radius: 4,
                                    fillColor: '#00FF00',
                                    lineColor: '#22FF22',
                                    lineWidth: 2
                                }
                            }
                        },
                        tooltip: true
                    });
                    //data.push({x: psa2datum, y: psa2, datum: new Date(psa2datum).getFullYear() + '-' + ((''+(new Date(psa2datum).getMonth()+1)).length < 2 ? '0' : '') + (new Date(psa2datum).getMonth() + 1) + '-' + ((''+new Date(psa2datum).getDate()).length < 2 ? '0' : '') + new Date(psa2datum).getDate(), startpsa: psa1, datum1: new Date(psa1datum).toJSON().slice(0,10), datum2: new Date(psa2datum).toJSON().slice(0,10), halveringstid: psahalveringstid/(24*60*60*1000), psahlv: true, color: '#00FF00', marker: { radius: 4, fillColor: '#00FF00', lineColor: '#22FF22', lineWidth: 0, states: { hover: { radius: 4, fillColor: '#00FF00', lineColor: '#22FF22', lineWidth: 2}}}, tooltip: true});
                } else {
                    data.push({
                        x: item.datum,
                        y: item.psa,
                        datum: new Date(item.datum).getFullYear() + '-' + (('' + (new Date(item.datum).getMonth() + 1)).length < 2 ? '0' : '') + (new Date(item.datum).getMonth() + 1) + '-' + (('' + new Date(item.datum).getDate()).length < 2 ? '0' : '') + new Date(item.datum).getDate(),
                        startpsa: psa1,
                        datum1: new Date(psa1datum).toJSON().slice(0, 10),
                        datum2: new Date(psa2datum).toJSON().slice(0, 10),
                        halveringstid: psahalveringstid / (24 * 60 * 60 * 1000),
                        psahlv: true,
                        tooltip: true
                    });
                }
            });
        }

        if (vm.overviewHighChart) {
            vm.overviewHighChart.series[13].setData(data, true);
        }
    };

    /**
     * Funktion för uppdatering av  Labbprov, PSA data för Highcharts
     *
     * @method redrawLabALPChart
     */
    redrawOverviewALPChart = function(vm) {
        var data = [];

        ko.utils.arrayForEach(vm.sortTable(vm.$$.Labbprov, 'LP_datum', 'ascending'), function(item) {
            var datum = item.LP_datum();
            var value = item.LP_alp();

            if (value) {
                if (typeof value == 'string')
                    value = parseFloat(value.replace(',', '.'));

                if (datum) {
                    var datumsplit = datum.split("-");
                    if (!isNaN(value))
                        data.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: value,
                            tab: '6',
                            datum: datum,
                            tooltip: true
                        });
                    else
                        data.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: 0,
                            tab: '6',
                            datum: datum,
                            tooltip: true
                        });
                }
            }
        });

        if (vm.overviewHighChart) {
            vm.overviewHighChart.series[3].setData(data, false);

            vm.modifiedALP(true);

        }
    };

    /**
     * Funktion för uppdatering av  Labbprov, Krea data för Highcharts
     *
     * @method redrawLabKreaChart
     */
    redrawOverviewKreaChart = function(vm) {
        var data = [];

        ko.utils.arrayForEach(vm.sortTable(vm.$$.Labbprov, 'LP_datum', 'ascending'), function(item) {
            var datum = item.LP_datum();
            var value = item.LP_krea();

            if (value) {
                if (typeof value == 'string')
                    value = parseFloat(value.replace(',', '.'));

                if (datum) {
                    var datumsplit = datum.split("-");
                    if (!isNaN(value))
                        data.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: value,
                            tab: '6',
                            datum: datum,
                            tooltip: true
                        });
                    else
                        data.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: 0,
                            tab: '6',
                            datum: datum,
                            tooltip: true
                        });
                }
            }


        });

        if (vm.overviewHighChart) {
            vm.overviewHighChart.series[4].setData(data, false);

            vm.modifiedKrea(true);

        }
    };

    /**
     * Funktion för uppdatering av  Labbprov, Hb data för Highcharts
     *
     * @method redrawLabHbChart
     */
    redrawOverviewHbChart = function(vm) {
        var data = [];

        ko.utils.arrayForEach(vm.sortTable(vm.$$.Labbprov, 'LP_datum', 'ascending'), function(item) {
            var datum = item.LP_datum();
            var value = item.LP_hb();

            if (value) {
                if (typeof value == 'string')
                    value = parseFloat(value.replace(',', '.'));

                if (datum) {
                    var datumsplit = datum.split("-");
                    if (!isNaN(value))
                        data.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: value,
                            tab: '6',
                            datum: datum,
                            tooltip: true
                        });
                    else
                        data.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: 0,
                            tab: '6',
                            datum: datum,
                            tooltip: true
                        });
                }
            }

        });

        if (vm.overviewHighChart) {
            vm.overviewHighChart.series[5].setData(data, false);

            vm.modifiedHb(true);

        }
    };

    /**
     * Funktion för uppdatering av  Labbprov, Hb data för Highcharts
     *
     * @method redrawLabHbChart
     */
    redrawOverviewTestosteronChart = function(vm) {
        var data = [];

        ko.utils.arrayForEach(vm.sortTable(vm.$$.Labbprov, 'LP_datum', 'ascending'), function(item) {
            var datum = item.LP_datum();
            var value = item.LP_testosteron();

            if (value) {
                if (typeof value == 'string')
                    value = parseFloat(value.replace(',', '.'));

                if (datum) {
                    var datumsplit = datum.split("-");
                    if (!isNaN(value))
                        data.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: value,
                            tab: '6',
                            datum: datum,
                            tooltip: true
                        });
                    else
                        data.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: 0,
                            tab: '6',
                            datum: datum,
                            tooltip: true
                        });
                }
            }

        });

        if (vm.overviewHighChart) {
            vm.overviewHighChart.series[6].setData(data, false);

            vm.modifiedTesto(true);

        }
    };

    /**
     * Funktion för uppdatering av  Labbprov, LPK data för Highcharts
     *
     * @method redrawOverviewLPKChart
     */
    redrawOverviewLPKChart = function(vm) {
        var data = [];

        ko.utils.arrayForEach(vm.sortTable(vm.$$.Labbprov, 'LP_datum', 'ascending'), function(item) {
            var datum = item.LP_datum();
            var value = item.LP_lpk();

            if (value) {
                if (typeof value == 'string')
                    value = parseFloat(value.replace(',', '.'));

                if (datum) {
                    var datumsplit = datum.split("-");
                    if (!isNaN(value))
                        data.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: value,
                            tab: '6',
                            datum: datum,
                            tooltip: true
                        });
                    else
                        data.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: 0,
                            tab: '6',
                            datum: datum,
                            tooltip: true
                        });
                }
            }

        });

        if (vm.overviewHighChart) {
            vm.overviewHighChart.get('LPK').setData(data, false);

            vm.modifiedLPK(true);

        }
    };

    /**
     * Funktion för uppdatering av  Labbprov, LPK data för Highcharts
     *
     * @method redrawOverviewTPKChart
     */
    redrawOverviewTPKChart = function(vm) {
        var data = [];

        ko.utils.arrayForEach(vm.sortTable(vm.$$.Labbprov, 'LP_datum', 'ascending'), function(item) {
            var datum = item.LP_datum();
            var value = item.LP_tpk();

            if (value) {
                if (typeof value == 'string')
                    value = parseFloat(value.replace(',', '.'));

                if (datum) {
                    var datumsplit = datum.split("-");
                    if (!isNaN(value))
                        data.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: value,
                            tab: '6',
                            datum: datum,
                            tooltip: true
                        });
                    else
                        data.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: 0,
                            tab: '6',
                            datum: datum,
                            tooltip: true
                        });
                }
            }

        });

        if (vm.overviewHighChart) {
            vm.overviewHighChart.get('TPK').setData(data, false);

            vm.modifiedTPK(true);

        }
    };

    /**
     * Funktion för uppdatering av  Labbprov, LPK data för Highcharts
     *
     * @method redrawOverviewNeutrofilaChart
     */
    redrawOverviewNeutrofilaChart = function(vm) {
        var data = [];

        ko.utils.arrayForEach(vm.sortTable(vm.$$.Labbprov, 'LP_datum', 'ascending'), function(item) {
            var datum = item.LP_datum();
            var value = item.LP_neutrofila();

            if (value) {
                if (typeof value == 'string')
                    value = parseFloat(value.replace(',', '.'));

                if (datum) {
                    var datumsplit = datum.split("-");
                    if (!isNaN(value))
                        data.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: value,
                            tab: '6',
                            datum: datum,
                            tooltip: true
                        });
                    else
                        data.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: 0,
                            tab: '6',
                            datum: datum,
                            tooltip: true
                        });
                }
            }

        });

        if (vm.overviewHighChart) {
            vm.overviewHighChart.get('Neutrofila').setData(data, false);

            vm.modifiedNeutrofila(true);

        }
    };

    /**
     * Funktion för uppdatering av Besök  data för Highcharts
     *
     * @method redrawOverviewBChart
     */
    redrawOverviewBChart = function(vm) {
        var crpc = false;
        var dataEW = [];
        var dataSB = [];

        vm.overviewHighChart.xAxis[0].removePlotLine('brpb-plotline');

        ko.utils.arrayForEach(vm.sortTable(vm.$$.Besök, 'B_besokdatum', 'ascending'), function(b) {
            var datum = b.B_besokdatum();
            if (datum) {
                var datumsplit = datum.split("-");
                if (b.B_ecogwho()) {
                    var color;
                    var value = b.B_ecogwho().value;
                    if (typeof value == 'string')
                        value = parseFloat(value.replace(',', '.'));
                    if (value == 2) {
                        color = '#ffe500';
                    } else if (value == 3) {
                        color = '#ffb500';
                    } else if (value == 4) {
                        color = '#ff7500';
                    } else {
                        color = '#d3d3d3';
                    }
                    dataEW.push({
                        x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                        y: 1,
                        tab: '2',
                        datum: datum,
                        ecogwholabel: b.B_ecogwho().value,
                        tooltip: true,
                        marker: {
                            fillColor: color,
                            states: {
                                hover: {
                                    fillColor: color
                                }
                            }
                        }
                    });
                }
                if (b.B_klinbed()) {
                    if (b.B_klinbed().value == 0)
                        dataSB.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: 0,
                            tab: '2',
                            datum: datum,
                            klinbed: b.B_klinbed().text,
                            klinbedlabel: 'R',
                            color: '#86cd86',
                            marker: {
                                fillColor: '#86cd86',
                                lineColor: '#64ab64',
                                lineWidth: 2,
                                states: {
                                    hover: {
                                        fillColor: '#86cd86',
                                        lineColor: '#64ab64',
                                        lineWidth: 2
                                    }
                                }
                            },
                            tooltip: true
                        });
                    else if (b.B_klinbed().value == 1)
                        dataSB.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: 0,
                            tab: '2',
                            datum: datum,
                            klinbed: b.B_klinbed().text,
                            klinbedlabel: 'S',
                            color: '#aaaaaa',
                            marker: {
                                fillColor: '#aaaaaa',
                                lineColor: '#777777',
                                lineWidth: 2,
                                states: {
                                    hover: {
                                        fillColor: '#aaaaaa',
                                        lineColor: '#777777',
                                        lineWidth: 2
                                    }
                                }
                            },
                            tooltip: true
                        });
                    else if (b.B_klinbed().value == 2)
                        dataSB.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: 0,
                            tab: '2',
                            datum: datum,
                            klinbed: b.B_klinbed().text,
                            klinbedlabel: 'P',
                            color: '#ff0000',
                            marker: {
                                fillColor: '#ff0000',
                                lineColor: '#cc0000',
                                lineWidth: 2,
                                states: {
                                    hover: {
                                        fillColor: '#ff0000',
                                        lineColor: '#cc0000',
                                        lineWidth: 2
                                    }
                                }
                            },
                            tooltip: true
                        });
                    else if (b.B_klinbed().value == 3)
                        dataSB.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: 0,
                            tab: '2',
                            datum: datum,
                            klinbed: b.B_klinbed().text,
                            klinbedlabel: 'AB',
                            color: '#dddddd',
                            marker: {
                                fillColor: '#dddddd',
                                lineColor: '#aaaaaa',
                                lineWidth: 2,
                                states: {
                                    hover: {
                                        fillColor: '#dddddd',
                                        lineColor: '#aaaaaa',
                                        lineWidth: 2
                                    }
                                }
                            },
                            tooltip: true
                        });
                    else if (b.B_klinbed().value == 4)
                        dataSB.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: 0,
                            tab: '2',
                            datum: datum,
                            klinbed: b.B_klinbed().text,
                            klinbedlabel: 'KR',
                            color: '#008600',
                            marker: {
                                fillColor: '#008600',
                                lineColor: '#006400',
                                lineWidth: 2,
                                states: {
                                    hover: {
                                        fillColor: '#008600',
                                        lineColor: '#006400',
                                        lineWidth: 2
                                    }
                                }
                            },
                            tooltip: true
                        });
                         else if (b.B_klinbed().value == 6)
                        dataSB.push({
                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                            y: 0,
                            tab: '2',
                            datum: datum,
                            klinbed: b.B_klinbed().text,
                            klinbedlabel: 'KR',
                            color: '#008600',
                            marker: {
                                fillColor: '#008600',
                                lineColor: '#006400',
                                lineWidth: 2,
                                states: {
                                    hover: {
                                        fillColor: '#008600',
                                        lineColor: '#006400',
                                        lineWidth: 2
                                    }
                                }
                            },
                            tooltip: true
                        });
                }
                if(b.B_brytpunktsbesok()) {
                    // remove old crpc plotline
                    vm.overviewHighChart.xAxis[0].removePlotLine('brpb-plotline');
                    // add new crpc plotline
                    vm.overviewHighChart.xAxis[0].addPlotLine({
                        id: 'brpb-plotline',
                        value: Date.UTC(datumsplit[0], (datumsplit[1] - 1), (datumsplit[2])),
                        width: 1,
                        color: '#bdbdbd',
                        dashStyle: 'shortdash',
                        zIndex: 2,
                        label: {
                            align: 'left',
                            textAlign: 'left',
                            x: 5,
                            rotation: 0,
                            style: {
                                fontSize: '10px',
                                color: '#aaa'
                            },
                            text: 'Brytpunktssamtal <br>' + datum
                        }
                    });
                    // add invisible point to crpc series
                    vm.overviewHighChart.series[0].setData([{
                        x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                        y: 0
                    }]);
                }
            }

        });

        if (vm.overviewHighChart) {
            vm.overviewHighChart.series[7].setData(dataEW, false);
            vm.overviewHighChart.series[8].setData(dataSB, false);
        }
    };


    /**
     * Funktion för uppdatering av  Bilddiagnostik  data för Highcharts
     *
     * @method redrawOverviewBDChart
     */
    redrawOverviewBDChart = function(vm) {
        var data = [];

        ko.utils.arrayForEach(vm.sortTable(vm.$$.Bilddiagnostik, 'BD_datum', 'ascending'), function(bd) {
            var datum = bd.BD_datum();
            var radbed;
            var typ;
            var metastaser = [];

            if (bd.BD_radbed())
                radbed = bd.BD_radbed().text;

            if (bd.BD_radundertyp())
                typ = bd.BD_radundtypannan() ? bd.BD_radundtypannan() : bd.BD_radundertyp().text;

            if (bd.$$.BD_Metastas) {
                ko.utils.arrayForEach(bd.$$.BD_Metastas(), function(metastas) {
                    if (metastas.BDM_metastaslokal())
                        if (metastas.BDM_metastaslokal().value == '5')
                            metastaser.push(metastas.BDM_metastaslokalann());
                        else
                            metastaser.push(metastas.BDM_metastaslokal().text);
                });
            }

            if (datum) {

                var resultat = [];

                resultat.push({
                    radbed: radbed,
                    typ: typ,
                    metastaser: metastaser
                });

                if (data.length > 0 && data[data.length - 1].datum == datum) {
                    data[data.length - 1].resultat = data[data.length - 1].resultat.concat(resultat);
                } else {
                    var datumsplit = datum.split("-");
                    data.push({
                        x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                        y: 2,
                        tab: '3',
                        datum: datum,
                        resultat: resultat,
                        marker: {
                            fillColor: '#606060',
                            lineColor: '#303030',
                            lineWidth: 2,
                            states: {
                                hover: {
                                    fillColor: '#606060',
                                    lineColor: '#303030',
                                    lineWidth: 2
                                }
                            }
                        },
                        tooltip: true
                    });
                }
            }
        });

        if (vm.overviewHighChart)
            vm.overviewHighChart.series[9].setData(data, false);
    };


    /**
     * Funktion för uppdatering av Strålbehandling  data för Highcharts
     *
     * @method redrawOverviewSBChart
     */
    redrawOverviewSBChart = function(vm) {
        var data = [];

        ko.utils.arrayForEach(vm.sortTable(vm.$$.Strålbehandling, 'SB_slutdatum', 'ascending'), function(sb) {
            var slutdatum = sb.SB_slutdatum();
            var grupper = [];

            if (sb.$$.SB_Grupp) {
                ko.utils.arrayForEach(sb.$$.SB_Grupp(), function(grupp) {
                    var grupp;
                    grupp.frakdos = grupp.SBG_frakdos();
                    grupp.slutdos = grupp.SBG_slutdos();
                    grupp.lokaler = [];

                    if (grupp.$$.SBG_Lokal) {
                        ko.utils.arrayForEach(grupp.$$.SBG_Lokal(), function(lokal) {
                            if (lokal.SBGL_lokal())
                                if (lokal.SBGL_lokal().value == '2' && lokal.SBGL_lokalskelett())
                                    grupp.lokaler.push(lokal.SBGL_lokal().text + " (" + lokal.SBGL_lokalskelett() + ")");
                                else if (lokal.SBGL_lokal().value == '5')
                                grupp.lokaler.push(lokal.SBGL_lokalann());
                            else
                                grupp.lokaler.push(lokal.SBGL_lokal().text);
                        });
                    }
                    grupper.push(grupp);
                });
            }

            if (slutdatum) {
                var slutdatumsplit = slutdatum.split("-");
                data.push({
                    x: Date.UTC(slutdatumsplit[0], (slutdatumsplit[1] - 1), slutdatumsplit[2]),
                    y: 3,
                    tab: '4',
                    slutdatum: slutdatum,
                    grupper: grupper,
                    marker: {
                        fillColor: '#606060',
                        lineColor: '#303030',
                        lineWidth: 2,
                        states: {
                            hover: {
                                fillColor: '#606060',
                                lineColor: '#303030',
                                lineWidth: 2
                            }
                        }
                    },
                    tooltip: true
                });
            }
        });

        if (vm.overviewHighChart)
            vm.overviewHighChart.series[10].setData(data, false);
    };


    /**
     * Funktion för uppdatering av SSE  data för Highcharts
     *
     * @method redrawOverviewSEChart
     */
    redrawOverviewSEChart = function(vm) {
        var data = [];

        ko.utils.arrayForEach(vm.sortTable(vm.$$.SRE, 'SRE_datum', 'ascending'), function(sre) {
            var datum = sre.SRE_datum();
            var handelser = [];

            if (sre.$$.SRE_Händelse) {
                ko.utils.arrayForEach(sre.$$.SRE_Händelse(), function(handelse) {
                    if (handelse.SREH_handelse())
                        handelser.push(handelse.SREH_handelse().text);
                });
            }

            if (datum) {
                var datumsplit = datum.split("-");
                data.push({
                    x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                    y: 4,
                    tab: '5',
                    datum: datum,
                    handelser: handelser,
                    marker: {
                        fillColor: '#606060',
                        lineColor: '#303030',
                        lineWidth: 2,
                        states: {
                            hover: {
                                fillColor: '#606060',
                                lineColor: '#303030',
                                lineWidth: 2
                            }
                        }
                    },
                    tooltip: true
                });
            }
        });

        if (vm.overviewHighChart)
            vm.overviewHighChart.series[11].setData(data, false);
    };

        /**
     * Funktion för uppdatering av Biobank data för Highcharts
     *
     * @method redrawOverviewBBChart
     */
    redrawOverviewBBChart = function(vm) {
        var data = [];



        ko.utils.arrayForEach(vm.sortTable(vm.$$.Biobank, 'BB_biobanksdatum', 'ascending'), function(bb) {
            var datum = bb.BB_biobanksdatum();
            var provtyper = [];
            if (bb.$$.BB_Biobanksprov) {
                ko.utils.arrayForEach(bb.$$.BB_Biobanksprov(), function(provtyp) {
                   
                    if (provtyp.BBP_biobankprovtyp()){
                       var text=provtyp.BBP_biobankprovtyp().text;
                       if(provtyp.BBP_typavvavnad())
                            text += ' (' + provtyp.BBP_typavvavnad().text + ')';
                        provtyper.push(text);
                    }
                   
                });
            }

            if (datum) {
                var datumsplit = datum.split("-");
                data.push({
                    x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                    y: 5,
                    tab: '10',
                    datum: datum,
                    provtyper: provtyper,
                    marker: {
                        fillColor: '#606060',
                        lineColor: '#303030',
                        lineWidth: 2,
                        states: {
                            hover: {
                                fillColor: '#606060',
                                lineColor: '#303030',
                                lineWidth: 2
                            }
                        }
                    },
                    tooltip: true
                });
            }
        });

        if (vm.overviewHighChart)
            vm.overviewHighChart.series[20].setData(data, false);
    };


    /**
    * Funktion för uppdatering av PROM data för Highcharts
    *
    * @method redrawOverviewPROMChart
    */
    redrawOverviewPROMChart = function(vm) {
    	var median = 0;
    	var data = [];
      if(vm.U_EnkatVD.listValues() && vm.U_EnkatVD.listValues().length > 0) {
      	ko.utils.arrayForEach(vm.sortPROM(vm.U_EnkatVD.listValues(), 'data', 'KlarTidpunkt', 'ascending'), function (p) {
          var datum = p.data.KlarTidpunkt.slice(0, 10);

      		if(datum) {
      			var datumsplit = datum.split("-");

            var enkatdata = JSON.parse(p.data.EnkatdataJSON);

            var count = 0;
      			var avg = 0.0;
            for(param in enkatdata) {
              if(param.indexOf("fr") === 0) {
                if(isNaN(parseInt(enkatdata[param]))) {
                  enkatdata[param] = 0;
                } else {
                  enkatdata[param] = parseInt(enkatdata[param]);
                  if(param != "fr15" && param != "fr16") {
                    avg += enkatdata[param] != "" ? enkatdata[param] : 0;
                    count++;
                  }
                }
              }
            }

            if(count > 0) {
              avg = (avg/count).toFixed(1);
            } else {
              avg = 0.0;
            }

      			var values = [{y: 1, value: enkatdata.fr1, name: "Kort promenad"}, {y: 1, value: enkatdata.fr2, name: "Sitta eller ligga"}, {y: 1, value: enkatdata.fr3, name: "Vardagshjälp"}, {y: 1, value: enkatdata.fr10, name: "Förstoppad"},
              {y: 1, value: enkatdata.fr8, name: "Aptit"}, {y: 1, value: enkatdata.fr9, name: "Illamående"}, {y: 1, value: enkatdata.fr17, name: "Miktion"}, {y: 1, value: enkatdata.fr4, name: "Andfådd"},
              {y: 1, value: enkatdata.fr7, name: "Svag"}, {y: 1, value: enkatdata.fr11, name: "Trött"}, {y: 1, value: enkatdata.fr6, name: "Sover"}, {y: 1, value: enkatdata.fr13, name: "Spänd"},
              {y: 1, value: enkatdata.fr14, name: "Nedstämd"}, {y: 1, value: enkatdata.fr5, name: "Smärta", extra: enkatdata.fr16}, {y: 1, value: enkatdata.fr12, name: "Aktivitet påverkad av smärta" }];

      			//var h = Math.floor((5 - (avg+1)) * 120 / 5);
                var h = Math.floor(parseInt(enkatdata.fr15) * 120 / 7);
                  
      			var color = hsv2rgb(h, 1, 0.9);
      			var linecolor = hsv2rgb(h, 1, 0.67);

      			data.push({x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]), y: 15, datum: datum, label: enkatdata.fr15, values: values, marker: { fillColor: color, lineColor: linecolor, lineWidth: 2, states: { hover: { fillColor: color, lineColor: linecolor, lineWidth: 2}}}, tooltip: true});

      		}

      	});
      }
    	if(vm.overviewHighChart) {
    		vm.overviewHighChart.series[15].setData(data, false);
    	}
    };


    /**
    * Funktion för uppdatering av PSA-dubbleringstid
    *
    * @method redrawOverviewPSAhalvering
    */
    redrawOverviewPROMPie = function(values, x, y) {
    	var data = [];
        var xCoord = parseInt(x);
        var yCoord = parseInt(y);

    	if(values && x && y) {
    		for(var i = 0; i < values.length; i++) {
                var color;
                
                if(values[i].value == 0) {
                    color = '#bbb'
                } else {
      			    var h = Math.floor((5 - ((values[i].value)+1)) * 120 / 5);
      			    color = hsv2rgb(h, 1, 0.9);
                }

    			values[i].color = color;
    		}

    		data = values;
    	}

        if(vm.overviewHighChart) {
            vm.overviewHighChart.series[16].setData(data, true, false, false);
            
            if(data != []) {
                vm.overviewHighChart.series[16].update({center: [xCoord, yCoord]}, true);
            }
        }
    };

    /**
     * Funktion för uppdatering av Läkemedel  data för Highcharts
     *
     * @method redrawOverviewLMCharts
     */
    redrawOverviewLMCharts = function(vm) {
        var index = 21;
        var length = vm.overviewHighChart.series.length;

        // ta bort lm serier
        for (var i = length - 1; i > (index - 1); i--) {
            vm.overviewHighChart.series[i].remove(false);
        }

        // ta bort lmlm labels
        if (vm.overviewHighChartLabels)
            vm.overviewHighChartLabels = vm.overviewHighChartLabels.slice(0, index);


        ko.utils.arrayForEach(vm.sortLMTable(vm.$$.Läkemedel()), function(lm) {
            if (lm.LM_typ() && (vm.firstLMEventDate(lm) && ((lm.LM_studielkm() && lm.LM_studielkmtxt()) || (lm.LM_typ() && vm.getSelectedSubstance(lm.LM_substans))) || (lm.LM_ablatiotestis() && lm.LM_ablatiotestisdatum()))) {
                var lmName = null;
                var lmColor = '#000000';

                if (lm.LM_studielkm() && lm.LM_studielkmtxt()) {
                    lmName = lm.LM_studielkmtxt() + " (studie)";
                    lmColor = "#000000";
                } else if (lm.LM_ablatiotestis() && lm.LM_ablatiotestisdatum()) {
                    lmName = "ablatio testis (behandling)";
                    lmColor = "#000000";
                } else if (lm.LM_typ() && lm.LM_substans()) {
                    lmName = vm.getSelectedSubstance(lm.LM_substans).sub_subnamnrek;
                    //lmColor = vm.getSelectedSubstance(lm.LM_substans).kat_substansfarg;
                }

                var lmNumber = index;

                // lägg till lm label
                vm.overviewHighChartLabels.push(lmName);

                //  ablatio testis
                if (lm.LM_ablatiotestis()) {
                    var behandlingsDatum = lm.LM_ablatiotestisdatum();

                    if (behandlingsDatum) {
                        var behandlingsDatumSplit = behandlingsDatum.split("-");
                        var handelse = 'Behandling';

                        // skapa punkt av behandlingen
                        var lmBehandlingPoint = {
                            name: lmName,
                            yAxis: 4,
                            showInLegend: false,
                            zIndex: 2,
                            color: lmColor,
                            type: 'scatter',
                            marker: {
                                symbol: 'diamond',
                                radius: 6
                            },
                        }

                        // skapa dataobjekt och lägg till behandlingen till punkten
                        lmBehandlingPoint.data = [];
                        lmBehandlingPoint.data.push({
                            x: Date.UTC(behandlingsDatumSplit[0], (behandlingsDatumSplit[1] - 1), behandlingsDatumSplit[2]),
                            y: lmNumber,
                            tab: '7',
                            datum: behandlingsDatum,
                            startdatum: behandlingsDatum,
                            handelse: handelse,
                            tooltip: true
                        });


                        // om overviewchart finns, lägg till behandlingspunkten
                        if (vm.overviewHighChart) {
                            vm.overviewHighChart.addSeries(lmBehandlingPoint, false);
                        }

                        // hämta dagens datum
                        var d = new Date();
                        var dagensDatum = d.getFullYear() + "-" + (('' + (d.getMonth() + 1)).length < 2 ? '0' : '') + (d.getMonth() + 1) + "-" + (('' + (d.getDate())).length < 2 ? '0' : '') + d.getDate();

                        if (dagensDatum) {
                            var dagensDatumSplit = dagensDatum.split("-");

                            // skapa behandlings serie
                            var lmBehandlingSerie = {
                                name: lmName,
                                color: lmColor,
                                type: 'line',
                                zIndex: 1,
                                lineWidth: 7,
                                yAxis: 4,
                                showInLegend: false,
                                marker: {
                                    enabled: false
                                },
                                states: {
                                    hover: {
                                        enabled: false
                                    }
                                },
                                enableMouseTracking: true
                            }

                            // skapa dataobjekt och lägg till behandlingsserien.
                            lmBehandlingSerie.data = [];
                            lmBehandlingSerie.data.push({
                                x: Date.UTC(behandlingsDatumSplit[0], (behandlingsDatumSplit[1] - 1), (behandlingsDatumSplit[2])),
                                y: lmNumber,
                                tab: '7',
                                startdatum: behandlingsDatum,
                                tooltip: false
                            }, {
                                x: Date.UTC(dagensDatumSplit[0], (dagensDatumSplit[1] - 1), (dagensDatumSplit[2])),
                                y: lmNumber,
                                tab: '7',
                                startdatum: behandlingsDatum,
                                tooltip: false
                            });

                            // om overviewchart finns, lägg till behandlingsserie objektet.
                            if (vm.overviewHighChart) {
                                vm.overviewHighChart.addSeries(lmBehandlingSerie, false);
                            }

                            // skapa slut punkt
                            var lmPoint = {
                                name: lmName,
                                yAxis: 4,
                                showInLegend: false,
                                zIndex: 0,
                                color: lmColor,
                                type: 'scatter',
                                marker: {
                                    symbol: 'diamond',
                                    radius: 6
                                }
                            }

                            // skapa dataobjekt och lägg till slutpunkten
                            lmPoint.data = [];
                            lmPoint.data.push({
                                x: Date.UTC(dagensDatumSplit[0], (dagensDatumSplit[1] - 1), dagensDatumSplit[2]),
                                y: lmNumber,
                                tab: '7',
                                datum: behandlingsDatum,
                                startdatum: behandlingsDatum,
                                tooltip: false
                            });

                            // om overviewchart finns, lägg till slutpunkten
                            if (vm.overviewHighChart) {
                                vm.overviewHighChart.addSeries(lmPoint, false);
                            }
                        }
                    }
                }
                // om insättningar finns
                else if (lm.$$.LM_Insättning()) {
                    // loopa igenom insättningar
                    ko.utils.arrayForEach(lm.$$.LM_Insättning(), function(lmIns) {
                        var pDatum;
                        var pHandelse;
                        var startdatum;

                        // finns händelser för insättningen
                        if (lmIns.$$.LMI_Händelse()) {
                            // loopa igenom händelser
                            ko.utils.arrayForEach(lmIns.$$.LMI_Händelse(), function(lmHandelse) {
                                var datum = lmHandelse.LMIH_handelsedatum();
                                // har händelsen ett datum
                                if (datum) {
                                    lmColor = '#000000';
                                    startdatum = datum;
                                    var datumsplit = datum.split("-");
                                    var handelse;
                                    var orsak;

                                    // har händelseobjektet en händelseaternativ angivet
                                    if (lmHandelse.LMIH_handelse())
                                        handelse = lmHandelse.LMIH_handelse().text;
                                    // har händelseobjektet en händelseorsak angiven
                                    if (lmHandelse.LMIH_handelseorsak())
                                        if (lmHandelse.LMIH_handelseorsak().value == '4')
                                            orsak = lmHandelse.LMIH_handelseorsakann();
                                        else
                                            orsak = lmHandelse.LMIH_handelseorsak().text;

                                        // markerar händelsen ett slut, skapa och lägg till en händelseserie av händelsen utifrån den tidiagre händelsen
                                    if (pDatum) {
                                        var pDatumsplit = pDatum.split("-");

                                        // skapa serie av händelsen
                                        var lmHandelseSerie = {
                                            name: lmName,
                                            color: lmColor,
                                            type: 'line',
                                            zIndex: 1,
                                            yAxis: 4,
                                            lineWidth: 11, // nytt
                                            showInLegend: false,
                                            marker: {
                                                enabled: false
                                            },
                                            states: {
                                                hover: {
                                                    enabled: false
                                                }
                                            },
                                            enableMouseTracking: true
                                        }

                                        // markerar händelsen slutet av en paus, byt färg på händelsen.
                                        if (pHandelse == 'Paus')
                                            lmHandelseSerie.color = increaseLMBrightness(lmColor, 75);

                                        // skapa dataobjekt och lägg till händelsen till serien.
                                        lmHandelseSerie.data = [];
                                        lmHandelseSerie.data.push({
                                            x: Date.UTC(pDatumsplit[0], (pDatumsplit[1] - 1), pDatumsplit[2]),
                                            y: lmNumber,
                                            tab: '7',
                                            startdatum: startdatum,
                                            tooltip: false
                                        }, {
                                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                                            y: lmNumber,
                                            tab: '7',
                                            startdatum: startdatum,
                                            tooltip: false
                                        });


                                        // om overviewchart finns, lägg till händelseserien.
                                        if (vm.overviewHighChart) {
                                            vm.overviewHighChart.addSeries(lmHandelseSerie, false);
                                        }

                                    }

                                    if (lmIns.$$.LMI_Utsättning() && lmIns.$$.LMI_Utsättning().length > 0) {
                                        lmColor = '#666666';
                                    }

                                    // skapa punkt av händelsen
                                    var lmInsattningPoint = {
                                        name: lmName,
                                        yAxis: 4,
                                        showInLegend: false,
                                        zIndex: 2,
                                        color: lmColor,
                                        type: 'scatter',
                                        marker: {
                                            radius: 6
                                        }
                                    }

                                    // markerar händelsen början av en paus, byt färg och justera storleken på punkten.
                                    if (handelse == 'Paus') {
                                        lmInsattningPoint.color = increaseLMBrightness(lmColor, 75);
                                        lmColor = increaseLMBrightness(lmColor, 75);
                                        lmInsattningPoint.marker.radius = 4;
                                    } else {
                                        lmInsattningPoint.color = lmColor
                                        lmInsattningPoint.marker.radius = 6;
                                    }

                                    // skapa dataobjekt och lägg till händelsen till punkten
                                    lmInsattningPoint.data = [];
                                    lmInsattningPoint.data.push({
                                        x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                                        y: lmNumber,
                                        tab: '7',
                                        datum: datum,
                                        startdatum: startdatum,
                                        handelse: handelse,
                                        orsak: orsak,
                                        tooltip: true
                                    });


                                    // om overviewchart finns, lägg till händelsepunkten
                                    if (vm.overviewHighChart) {
                                        vm.overviewHighChart.addSeries(lmInsattningPoint, false);
                                    }

                                    // spara händelseuppgifter till nästkommande händelse
                                    pHandelse = handelse;
                                    pDatum = datum;
                                }
                            });
                        }

                        // finns utsättning för insättningen
                        if (lmIns.$$.LMI_Utsättning() && lmIns.$$.LMI_Utsättning().length > 0) {
                            // loopa igenom utsättningar
                            ko.utils.arrayForEach(lmIns.$$.LMI_Utsättning(), function(lmUtsattning) {
                                var datum = lmUtsattning.LMIU_utsattningsdatum();
                                // har utsättningen ett datum
                                if (datum) {
                                    var datumsplit = datum.split("-");
                                    var handelse = 'Utsättning';
                                    var orsak;
                                    var antalbeh = lmUtsattning.LMIU_antalbeh();

                                    if (lmUtsattning.LMIU_utsattningsorsak())
                                        if (lmUtsattning.LMIU_utsattningsorsak().value == '4')
                                            orsak = lmUtsattning.LMIU_utsattningsorsakann();
                                        else
                                            orsak = lmUtsattning.LMIU_utsattningsorsak().text;


                                        // markerar utsättningen ett slut, skapa och lägg till en utsättningsserie av utsättningen utifrån den tidiagre händelsen
                                    if (pDatum) {
                                        lmColor = '#666666';
                                        var pDatumsplit = pDatum.split("-");

                                        var lmUtsattningSerie = {
                                            name: lmName,
                                            color: lmColor,
                                            type: 'line',
                                            zIndex: 1,
                                            yAxis: 4,
                                            lineWidth: 11, // nytt
                                            showInLegend: false,
                                            marker: {
                                                enabled: false
                                            },
                                            states: {
                                                hover: {
                                                    enabled: false
                                                }
                                            },
                                            enableMouseTracking: true
                                        }

                                        // markerar händelsen slutet av en paus, byt färg på händelsen.
                                        if (pHandelse == 'Paus') {
                                            lmUtsattningSerie.color = increaseLMBrightness(lmColor, 75);
                                            lmColor = increaseLMBrightness(lmColor, 75);
                                        }

                                        // skapa dataobjekt och lägg till utsättningen till serien.
                                        lmUtsattningSerie.data = [];
                                        lmUtsattningSerie.data.push({
                                            x: Date.UTC(pDatumsplit[0], (pDatumsplit[1] - 1), (pDatumsplit[2])),
                                            y: lmNumber,
                                            tab: '7',
                                            startdatum: startdatum,
                                            tooltip: false
                                        }, {
                                            x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), (datumsplit[2])),
                                            y: lmNumber,
                                            tab: '7',
                                            startdatum: startdatum,
                                            tooltip: false
                                        });

                                        // om overviewchart finns, lägg till utsättningsserien.
                                        if (vm.overviewHighChart) {
                                            vm.overviewHighChart.addSeries(lmUtsattningSerie, false);
                                        }
                                    }

                                    // skapa punkt av utsättningen
                                    var lmUtsattningPoint = {
                                        name: lmName,
                                        yAxis: 4,
                                        showInLegend: false,
                                        zIndex: 2,
                                        color: lmColor,
                                        type: 'scatter'
                                    }

                                    // skapa dataobjekt och lägg till utsättningen till punkten
                                    lmUtsattningPoint.data = [];
                                    lmUtsattningPoint.data.push({
                                        x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                                        y: lmNumber,
                                        tab: '7',
                                        datum: datum,
                                        startdatum: startdatum,
                                        handelse: handelse,
                                        orsak: orsak,
                                        antalbeh: antalbeh,
                                        tooltip: true
                                    });

                                    // om overviewchart finns, lägg till utsättningspunkten
                                    if (vm.overviewHighChart) {
                                        vm.overviewHighChart.addSeries(lmUtsattningPoint, false);
                                    }

                                    // spara utsättningsuppgifter till nästkommande händelse
                                    pDatum = datum;
                                }
                            });
                        }
                        // finns ingen utsättning för insättningen, låt den löpa till dagens datum
                        else {
                            // hämta dagens datum
                            var d = new Date();
                            var datum = d.getFullYear() + "-" + (('' + (d.getMonth() + 1)).length < 2 ? '0' : '') + (d.getMonth() + 1) + "-" + (('' + (d.getDate())).length < 2 ? '0' : '') + d.getDate();
                            var datumsplit = datum.split("-");

                            // markerar utsättningen ett slut, skapa och lägg till en utsättningsserie av utsättningen utifrån den tidiagre händelsen
                            if (pDatum) {
                                lmColor = '#000000';
                                var pDatumsplit = pDatum.split("-");
                                var handelse = 'Ej utsatt';

                                var lmUtsattningSerie = {
                                    name: lmName,
                                    color: lmColor,
                                    type: 'line',
                                    zIndex: 1,
                                    yAxis: 4,
                                    lineWidth: 11, // nytt
                                    showInLegend: false,
                                    marker: {
                                        enabled: false
                                    },
                                    states: {
                                        hover: {
                                            enabled: false
                                        }
                                    },
                                    enableMouseTracking: true
                                }

                                // markerar händelsen slutet av en paus, byt färg på händelsen.
                                if (pHandelse == 'Paus')
                                    lmUtsattningSerie.color = increaseLMBrightness(lmColor, 50);

                                // skapa dataobjekt och lägg till utsättningen till serien.
                                lmUtsattningSerie.data = [];
                                lmUtsattningSerie.data.push({
                                    x: Date.UTC(pDatumsplit[0], (pDatumsplit[1] - 1), (pDatumsplit[2])),
                                    y: lmNumber,
                                    tab: '7',
                                    startdatum: startdatum,
                                    tooltip: false
                                }, {
                                    x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), (datumsplit[2])),
                                    y: lmNumber,
                                    tab: '7',
                                    startdatum: startdatum,
                                    tooltip: false
                                });

                                // om overviewchart finns, lägg till utsättningsserien.
                                if (vm.overviewHighChart) {
                                    vm.overviewHighChart.addSeries(lmUtsattningSerie, false);
                                }
                            }

                            // skapa punkt för pågående
                            var lmUtsattningPoint = {
                                name: lmName,
                                yAxis: 4,
                                showInLegend: false,
                                zIndex: 2,
                                color: lmColor,
                                type: 'scatter',
                                marker: {
                                    symbol: 'diamond',
                                    radius: 6,
                                    lineWidth: 0
                                }
                            }

                            // skapa dataobjekt och lägg till utsättningen till punkten
                            lmUtsattningPoint.data = [];
                            lmUtsattningPoint.data.push({
                                x: Date.UTC(datumsplit[0], (datumsplit[1] - 1), datumsplit[2]),
                                y: lmNumber,
                                tab: '7',
                                datum: datum,
                                startdatum: startdatum,
                                handelse: handelse,
                                tooltip: true
                            });

                            // om overviewchart finns, lägg till utsättningspunkten
                            if (vm.overviewHighChart) {
                                vm.overviewHighChart.addSeries(lmUtsattningPoint, false);
                            }

                        }
                    });

                }
                index++;
            }
        });


        // temp
        if (vm.overviewLMHeight != (vm.$$.Läkemedel().length * vm.LMHeight)) {
            vm.overviewLMHeight = vm.$$.Läkemedel().length * vm.LMHeight;
            vm.overviewHighChart.yAxis[4].update({
                height: vm.overviewLMHeight
            }, false);
            vm.overviewHighChart.xAxis[0].update({
                offset: -(vm.overviewPlotHeight + vm.overviewPlotPadding + vm.overviewAxisPadding + vm.overviewLMHeight + vm.overviewLMPadding + vm.overviewSBHeight + vm.overviewSBPadding + vm.overviewPROMHeight + vm.overviewPROMPadding)
            }, false);
            vm.overviewHighChart.setSize(vm.overviewWidth, vm.overviewLabHeight + vm.overviewAxisPadding + vm.overviewLabPadding + vm.overviewSBHeight + vm.overviewSBPadding + vm.overviewPROMHeight + vm.overviewPROMPadding + vm.overviewPlotHeight + vm.overviewPlotPadding + vm.overviewLMHeight + vm.overviewLMPadding, false);
        }
    };




    /* ÖVRIGA FUNKTIONER */

    /**
     * Funktion för justera färgsättning på en läkemedelslinje
     *
     * @method increaseLMBrightness
     */
    increaseLMBrightness = function(hex, percent) {
        // strip the leading # if it's there
        hex = hex.replace(/^\s*#|\s*$/g, '');

        // convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
        if (hex.length == 3) {
            hex = hex.replace(/(.)/g, '$1$1');
        }

        var r = parseInt(hex.substr(0, 2), 16),
            g = parseInt(hex.substr(2, 2), 16),
            b = parseInt(hex.substr(4, 2), 16);

        return '#' +
            ((0 | (1 << 8) + r + (256 - r) * percent / 100).toString(16)).substr(1) +
            ((0 | (1 << 8) + g + (256 - g) * percent / 100).toString(16)).substr(1) +
            ((0 | (1 << 8) + b + (256 - b) * percent / 100).toString(16)).substr(1);
    };

    var hsv2rgb = function(h, s, v) {
      // adapted from http://schinckel.net/2012/01/10/hsv-to-rgb-in-javascript/
      var rgb, i, data = [];
      if (s === 0) {
        rgb = [v,v,v];
      } else {
        h = h / 60;
        i = Math.floor(h);
        data = [v*(1-s), v*(1-s*(h-i)), v*(1-s*(1-(h-i)))];
        switch(i) {
          case 0:
            rgb = [v, data[2], data[0]];
            break;
          case 1:
            rgb = [data[1], v, data[0]];
            break;
          case 2:
            rgb = [data[0], v, data[2]];
            break;
          case 3:
            rgb = [data[0], data[1], v];
            break;
          case 4:
            rgb = [data[2], data[0], v];
            break;
          default:
            rgb = [v, data[0], data[1]];
            break;
        }
      }
      return '#' + rgb.map(function(x){
        return ("0" + Math.round(x*239).toString(16)).slice(-2);
      }).join('');
    };


});
