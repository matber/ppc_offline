(function ($) {
    /**
     * Extender för touch
     *
     * @extender touch
     */
    ko.extenders.touch = function (target, table) {
        target.subscribe(function (newValue) {
            vm.touchForm();
            vm.touchTable(table);
        });
        return target;
    };

    ko.extenders.sre = function (target, parent) {
        if (target.rcc.regvarName == 'SBGL_lokal') {
            target.subscribe(function (newValue) {
                if (newValue && newValue.value == '2') {
                    vm.getDialog('#rcc-add-SRE').done(function () {
                        if (parent.SB_slutdatum() != null) {
                            vm.addSREStral(parent.SB_slutdatum());
                        } else {
                            vm.getInfoDialogJS('#rcc-sre-info');
                        }
                    }).fail(function () {

                    });
                }
            });
        }
        return target;
    };

})(jQuery);


$(document).ready(function () {
    /**
     *  Initialize viewmodel
     */
    var validering = new RCC.Validation();
    var vm = new RCC.ViewModel({
        validation: validering
    });
    window.vm = vm;

    // deaktivera formulärvalidering
    vm.$validation.enabled(false);

    //setTimeout(function () { // Varför är det en timeout på denna?
        $("#rcc-progressbar").dialog({
            height: 80,
            width: 245,
            modal: true
        });
    //}, 1);

    vm.normalErrand = ko.observable(false);
    vm.registerPost = ko.observable(false);
    vm.originalHandling = ko.observable(false);
    vm.externalInca = ko.observable(false);

    if (!inca.errand)
        vm.registerPost(true);
    else if (inca.form.isReadOnly)
        vm.originalHandling(true);
    else
        vm.normalErrand(true);

    if (typeof (inca.getBaseUrl) == "undefined")
        vm.externalInca(true);


    var formWidth = $('#rcc-form').width();

    $('button').button();

    /**
     *  LOKALA FORMULÄR VARIABLER
     */
    vm.U_EnkatVD.listValues = ko.observableArray(undefined);

    vm.PSA_funktioner_help = "För att beräkna PSA-dubbleringstid, håll in Ctrl knappen på tangentbordet och klicka på den punkt som du vill ha som startpunkt.<br>För att beräkna PSA-halveringstiden, håll in Alt knappen på tangentbordet och klicka på den punkt som du vill ha som startpunkt.<br><br>För att ta bort den uträknade punkten för dubblering eller halveringstid, håll in Ctrl och klicka på punkten.";
    vm.zoom_help = "Klicka på och dra längs med tidsaxeln för att zooma i grafen.<br><br>När du zoomat in kan du återgå till ursprungligt läge med \"Återställ\" knappen.";
    vm.linjar_skala_help = "Den linjära skalan är bra att använda när man har en liten variation bland PSA-mätvärdena.<br><br>Den är som namnet säger linjär och är således lättare att tolka för ögat än den logaritmiska skalan, men kan vara svår att läsa om det är både väldigt höga och väldigt låga mätvärden.";
    vm.log_skala_help = "Den logaritmiska skalan är bra att använda när man har stor variation bland PSA-mätvärdena.<br><br>Den får kurvan att se jämnare ut, vilket gör den mer svårtolkad för ögat, men visar på skillnader både i liten och stor skala för små och stora mätvärden.";

    vm.psa_help = "Efter radikal prostatektomi:  Två konsekutivt ökande PSA – värden över > 0.2 ug/l.<br><br>Efter kurativt syftande radioterapi: PSA-värdet > 2.0 ug/l från nadir efter strålbehandling";
    vm.crpc_help = "Datum för kastrationsresistent sjukdom enligt Nationella vårdprogrammet<br><br>Plasmatestosteronvärde &lt; 1.7 nmol/l (50 ng/dl) under pågående behandling med GnRH analog eller orkidektomi samt att det finns ett eller flera av nedanstående tecken på progress minst 6 veckor efter att en eventuell bikalutamidbehandling har avslutats.<br><br><ol><li>Lokal progress värderad genom palpation eller bilddiagnostik</li><li>Nytillkomna eller tillväxande metastaser</li><li>2 på varandra stigande PSA värden över 2 ug/l.</li></ol>";
    vm.klinbed_help = "Respons – Patient som ej har stabil sjukdom eller progress<br /><br />Stabil sjukdom – stabilt PSA (+/- 25% förändring av PSA). Inga nytillkomna metastaser eller nytillkomna cancerrelaterade symtom<br /><br />Progress – stigande PSA, nytillkomna metastaser, ökade cancerrelaterade symtom. Progress bedöms från bästa respons<br /><br />Avvakta bedömning - när det inte finns underlag att fatta ett beslut<br /><br />Ej bedömt - när det inte finns en tydlig journalanteckning";
    vm.multidisc_help = "Multidisciplinär konferens där urolog, onkolog och kontaktsjuksköterska deltar";
    vm.promfragor_help = "PROM; Patient Reported Outcome Measures<br><br>Patientrapporterade utfallsmått dvs enkät med frågor om sjukdomssymtom, funktionsförmåga och livskvalitet.";
    vm.brytpunktBesok_help = "Kryssa för när all systemisk onkologisk behandling är avslutad";
    vm.tstadium_help = "T - lokalt tumörstadium";
    vm.nstadium_help = "N - regionala lymfkörtlar";
    vm.mstadium_help = "M - fjärrmetastaser";

    vm.overviewHighChartLabels = null;
    vm.overviewHighChart = null;

    vm.isCRPCSet = ko.observable(undefined);
    vm.isPallHemSet = ko.observable(undefined);

    vm.formHasChanged = ko.observable(false);
    vm.overviewHighChartHasChanged = ko.observable(false);
    vm.rootHasChanged = ko.observable(true);
    vm.tableBHasChanged = ko.observable(true);
    vm.tableBDHasChanged = ko.observable(true);
    vm.tableSBHasChanged = ko.observable(true);
    vm.tableSEHasChanged = ko.observable(true);
    vm.tableLPHasChanged = ko.observable(true);
    vm.tableBBHasChanged = ko.observable(true);
    vm.tableLMHasChanged = ko.observable(true);
    vm.PROMHasChanged = ko.observable(true);

    var requestCount = ko.observable(0);

    /**
     *  LOKALA FUNKTIONER
     */


    /**
     * Funktion för förberedande åtgärder innan formulärets initializeras
     *
     * @method runFunctionsOnLoad
     */
    vm.runFunctionsOnLoad = function () {

        var helpImgSrc = '../../Public/Files/uppsala/common-libs/icons/info.png';
        var zoomImgSrc = '../../Public/Files/uppsala/common-libs/icons/mag.jpg';

        $('img.rcc-help-icon').each(function () {
            $(this).attr('src', helpImgSrc);
        });
        $('img.rcc-zoom-icon').each(function () {
            $(this).attr('src', zoomImgSrc);
        });

        // initializera substanslista för samtliga läkemedel
        ko.utils.arrayForEach(vm.$$.Läkemedel(), function (lm) {
            lm.LM_substans.listValues = ko.observableArray(undefined);

            if (lm.LM_typ()) {
                if (lm.LM_typ().value == '0')
                    vm.loadSubstances(lm, 'ProstataProcess_CY');
                else if (lm.LM_typ().value == '1')
                    vm.loadSubstances(lm, 'ProstataProcess_ET');
                else if (lm.LM_typ().value == '2')
                    vm.loadSubstances(lm, 'ProstataProcess_BB');
                else if (lm.LM_typ().value == '3')
                    vm.loadSubstances(lm, 'ProstataProcess_IS');
                else if (lm.LM_typ().value == '4')
                    vm.loadSubstances(lm, 'ProstataProcess_OC');
            }

        });

        /**
         *  Hämta eventuell enkätdata på patienten
         */
        if (vm.U_PromEnkatkod()) {
            requestCount(requestCount() + 1);
            inca.form.getValueDomainValues({
                vdlist: "U_EnkatVD",
                parameters: {
                    deltagarkod: vm.U_PromEnkatkod()
                },
                success: function (data) {
                    vm.U_EnkatVD.listValues(data);
                    requestCount(requestCount() - 1);
                    vm.PROMHasChanged(true);
                },
                error: function (err) {
                    requestCount(requestCount() - 1 );
                    $('<div title="Ett fel har uppstått"></div>')
                        .text( 'Lyckades inte ladda enkät: ' + err + ', ' + JSON.stringify(err) + '. Försök att ladda om formuläret.')
                        .dialog({
                            buttons: {
                                OK: function () {
                                    $(this).close();
                                }
                            }
                        });
                    $(inca.form.getContainer()).hide();
                }
            });
        }

        // initializera formulärtabbar
        $("#prostataprocess-tabs").tabs({
            select: function (event, ui) {
                if (ui.index == 0) {
                    //var start = new Date().getTime();  // log start timestamp

                    vm.overviewHighChartHasChanged(false);
                    //vm.chartInitialized(false);

                    if (vm.rootHasChanged()) {
                        redrawOverviewRootChart(vm);
                        vm.rootHasChanged(false);
                        vm.overviewHighChartHasChanged(true);
                    }
                    if (vm.tableBHasChanged()) {
                        redrawOverviewBChart(vm);
                        vm.tableBHasChanged(false);
                        vm.overviewHighChartHasChanged(true);
                    }
                    if (vm.tableBDHasChanged()) {
                        redrawOverviewBDChart(vm);
                        vm.tableBDHasChanged(false);
                        vm.overviewHighChartHasChanged(true);
                    }
                    if (vm.tableSBHasChanged()) {
                        redrawOverviewSBChart(vm);
                        vm.tableSBHasChanged(false);
                        vm.overviewHighChartHasChanged(true);
                    }
                    if (vm.tableSEHasChanged()) {
                        redrawOverviewSEChart(vm);
                        vm.tableSEHasChanged(false);
                        vm.overviewHighChartHasChanged(true);
                    }
                    if (vm.tableLPHasChanged()) {
                        redrawOverviewPSAChart(vm);
                        redrawOverviewALPChart(vm);
                        redrawOverviewKreaChart(vm);
                        redrawOverviewHbChart(vm);
                        redrawOverviewTestosteronChart(vm);
                        redrawOverviewLPKChart(vm);
                        redrawOverviewTPKChart(vm);
                        redrawOverviewNeutrofilaChart(vm);

                        vm.tableLPHasChanged(false);
                        vm.overviewHighChartHasChanged(true);
                    }
                    if (vm.tableBBHasChanged()) {
                        redrawOverviewBBChart(vm);
                        vm.tableBBHasChanged(false);
                        vm.overviewHighChartHasChanged(true);
                      }
                    if (vm.PROMHasChanged()) {
                        redrawOverviewPROMChart(vm);
                        vm.PROMHasChanged(false);
                        vm.overviewHighChartHasChanged(true);
                    }
                    if (vm.tableLMHasChanged()) {
                        redrawOverviewLMCharts(vm);
                        vm.tableLMHasChanged(false);
                        vm.overviewHighChartHasChanged(true);
                    }
                    if (vm.overviewHighChartHasChanged()) {

                        vm.overviewHighChart.redraw();
                        setTimeout(function () {
                            vm.redrawLegend();
                        }, 1);

                    }

                }
            }

        });

    };


    /**
     * Funktion för åtgärder efter att formuläret har initializeras
     *
     * @method runFunctionsAfterLoad
     */
    vm.runFunctionsAfterLoad = function () {

        // initiera charts
        initializeOverviewHighChart(vm); 
       
        ko.utils.arrayForEach(vm.$$.Läkemedel(), function (lm) {
            vm.setLakemedelSubscribers(lm);
        });

        $('#prostataprocess-tabs').tabs('select', 1);
        $('#prostataprocess-tabs').tabs('select', 0);

        // initializera läkemedelstabbar
        $('.accordion').accordion({
            collapsible: true,
            active: false,
            autoHeight: false
        });

        $('.LM-accordion').accordion({
            collapsible: true,
            active: 0,
            autoHeight: false
        });

        $('#prostataprocess-lmtabs').tabs({ collapsible: true, active: false });

		setTimeout(function() {
			$("#rcc-progressbar").dialog("destroy");
		}, 10);
			
        $('#rcc-form').removeClass('hidden');
    };


    /**
     * Funktion för att ladda substanslista från Substans/Kategori värdedomän.
     *
     * @method loadSubstances
     * @param {String} lm
     * @param {String} category
     * @return {Object} Dataobject
     */
    vm.loadSubstances = function (lm, category) {
        requestCount(requestCount() + 1);
        inca.form.getValueDomainValues({
            vdlist: "LM_substans",
            parameters: {
                kategori: category
            },
            success: function (data) {
                lm.LM_substans.listValues(data);
                requestCount(requestCount() - 1);
            },
            error: function (err) {
                requestCount(requestCount() - 1);
                $('<div title="Ett fel har uppstått"></div>')
                    .text('Lyckades inte ladda rader från värdedomänen ”Substans/Kategori”: ' + err + ', ' + JSON.stringify(err) + '. Försök att ladda om formuläret.')
                    .dialog({
                        buttons: {
                            OK: function () {
                                $(this).dialog('close');
                            }
                        }
                    }).position({
                        my: 'left',
                        at: 'right',
                        of: target
                    });
                $(inca.form.getContainer()).hide();
            }
        });
    };

    /**
     * Funktion för att returnera en enkät via värdedomän som ett dataobjekt.
     *
     * @method getSelectedEnkat
     * @param {String} pk Primärnyckelvärde för dataobjekt
     * @return {Object} Dataobject
     */
    vm.getSelectedEnkat = function (pk) {
        if (vm.U_EnkatVD() != null) {
            var item = ko.utils.arrayFirst(vm.U_EnkatVD.listValues() || [], function (item) {
                return item.id == pk;
            });

            if (!item)
                return undefined;

            return item.data;
        } else {
            return undefined;
        }
    };


    /**
     * Funktion för att returnera en registerpost från substans/kategori värdedomän som ett dataobjekt.
     *
     * @method getSelectedSubstance
     * @param {String} substance Substans variabel
     * @return {Object} Dataobject
     */
    vm.getSelectedSubstance = function (substance) {
        if (substance != null) {
            var item = ko.utils.arrayFirst(substance.listValues() || [], function (item) {
                return item.id == substance();
            });

            if (!item)
                return undefined;

            return item.data;
        } else {
            return undefined;
        }
    };


    function addNotAfterDeathValidation(variable) {
        variable.rcc.validation.add(
            new RCC.Validation.Tests.NotAfterDeathDate(
                variable,
                { deathDate: vm.$env._DATEOFDEATH, ignoreMissingValues: true }
            )
        );
    }

    /* ÅTERBESÖK */
    /**
     * Funktion för att lägga till ett Besök
     * subscribers sätts sedan på berörda variabler
     *
     * @method addAterbesok
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.addAterbesok = function (data, event) {
        // lägg till en ny rad till Besökstabellen
        var item = vm.$$.Besök.rcc.add();

        // uppdatera accordion för tabellen
        vm.accordTable('#B-accordion', true);

    };


    /**
     * Funktion för att sätta 'Vårdgivare' till inloggad användare
     *
     * @method setName
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.setName = function (data, event) {
        data.B_lakare(inca.user.firstName + " " + inca.user.lastName);
    };


    /**
     * Funktion för att sätta 'dagens datum' till besöksdatum
     *
     * @method setDate
     * @param {Object} data Registervariabel
     * @param {Object} event Eventobjekt
     */
    vm.setDate = function (data, event) {
        data(getDagensDatum());
    };


    /**
     * Funktion för att sätta 'Nej till alla' frågor under har patienten sedan föregående besök ...
     *
     * @method setNoToAll
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.setNoToAll = function (data, event) {
        //data.B_biobank(data.B_biobank.rcc.term.listValues[0]);
        data.B_multikonf(data.B_multikonf.rcc.term.listValues[0]);
        data.B_studie(data.B_studie.rcc.term.listValues[0]);
        data.B_palliativvard(data.B_palliativvard.rcc.term.listValues[0]);
    };


    /**
     * Funktion för att ta bort ett  Besök.
     *
     * @method removeAterbesok
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.removeAterbesok = function (data, event) {
        
         //Vi ser till att ta bort värde för crpc-datum som beräknas utifrån Besöksfliken
        if(data.B_besokdatum() == vm.U_crpcdatum() && (data.B_crpc() && data.B_crpc().value == '1')){
            vm.U_crpcdatum(undefined);
        }

          //Vi ser till att ta bort värde för palliativ hemsjukvård-datum som beräknas utifrån Besöksfliken
        if(data.B_besokdatum() == vm.U_pallHemSjkvrd() && (data.B_palliativvard() && data.B_palliativvard().value == '1')){
            vm.U_pallHemSjkvrd(undefined);
        }

        // ta bort en rad till besökstabellen
        vm.$$.Besök.rcc.remove(data);

        // uppdatera accordion för tabellen
        vm.accordTable('#B-accordion', true);
    };



    /* BILDDIAGNOSTIK */


    /**
     * Funktion för att lägga till en bilddiagnostik
     * subscribers sätts sedan på berörda variabler
     *
     * @method addBilddiagnostik
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.addBilddiagnostik = function (data, event) {
        // lägg till en ny rad till Besökstabellen
        vm.$$.Bilddiagnostik.rcc.add();

        // uppdatera accordion för tabellen
        vm.accordTable('#BD-accordion', true);
    };


    /**
     * Funktion för att ta bort en Bilddiagnostik.
     *
     * @method removeBilddiagnostik
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.removeBilddiagnostik = function (data, event) {
        // ta bort en rad till läkemedelstabellen
        vm.$$.Bilddiagnostik.rcc.remove(data);

        // uppdatera accordion för tabellen
        vm.accordTable('#BD-accordion', true);
    };


    /**
     * Funktion för att lägga till en bilddiagnostik metastas
     * subscribers sätts sedan på berörda variabler
     *
     * @method addBDMetastas
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.addBDMetastas = function (data, event) {
        // lägg till en ny rad till Lokaltabellen
        var item = data.$$.BD_Metastas.rcc.add();
    };

    /**
     * Funktion för att ta bort en bilddiagnostik metastas
     *
     * @method removeBDMetastas
     * @param {Object} parent parent to Dataobject
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.removeBDMetastas = function (parent, data, event) {
        // ta bort en grupp-rad
        parent.$$.BD_Metastas.rcc.remove(data);
    };


  
    /* STRÅLBEHANDLING */


    /**
     * Funktion för att lägga till en strålbehandling
     * subscribers sätts sedan på berörda variabler
     *
     * @method addStralbehandling
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.addStralbehandling = function (data, event) {
        // lägg till en ny rad till Strålbehandlingstabellen
        vm.$$.Strålbehandling.rcc.add();

        vm.$$.Strålbehandling()[vm.$$.Strålbehandling().length - 1].$$.SB_Grupp.rcc.add();

        // uppdatera accordion för tabellen
        vm.accordTable('#SB-accordion', true);
    };


    /**
     * Funktion för att ta bort en strålbehandling.
     *
     * @method removeStralbehandling
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.removeStralbehandling = function (data, event) {

        if(data.SB_slutdatum()){
            //Här ska vi ev ta bort SSE, men först fråga
            var isSkelett = false;

            ko.utils.arrayForEach(data.$$.SB_Grupp(), function (grp) {
                // Vi loopar på alla grupper som ligger i strålbehandlingen
                if(!isSkelett){
                    ko.utils.arrayForEach(grp.$$.SBG_Lokal(), function (lokal) {
                        // Vi loopar på alla lokalen som ligger i gruppen
                        if(lokal.SBGL_lokal() && lokal.SBGL_lokal().value == '2'){
                            // Vi har en lokal som är satt till Skelett
                            isSkelett = true;
                        }
                    
                    });
                }
            });

            if(isSkelett){
                vm.removeSkelettSRE(data.SB_slutdatum());
            }
        }

        // ta bort en rad till läkemedelstabellen
        vm.$$.Strålbehandling.rcc.remove(data);

        // uppdatera accordion för tabellen
        vm.accordTable('#SB-accordion', true);
    };

 /**
     * Funktion för att ta bort en grupp ur strålbehandling.
     *
     * @method removeGrupp
     * @param {Object} parent parent to Dataobject
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.removeGrupp = function (parent, data, event) {
    
        if(parent.SB_slutdatum()){
            //Här ska vi ev ta bort SSE, men först fråga
            var isSkelett = false;
            ko.utils.arrayForEach(data.$$.SBG_Lokal(), function (lokal) {
                // Vi loopar på alla lokalen som ligger i gruppen
                if(lokal.SBGL_lokal() && lokal.SBGL_lokal().value == '2'){
                    // Vi har en lokal som är satt till Skelett
                    isSkelett = true;
                }
            });
        
            if(isSkelett){
                vm.removeSkelettSRE(parent.SB_slutdatum());
            }
        }

        // ta bort en grupp-rad
        parent.$$.SB_Grupp.rcc.remove(data);

    };

     /**
     * Funktion för att ta bort en lokal ur strålbehandling.
     *
     * @method removeLokal
     * @param {Object} grandparent grandparent to Dataobject
     * @param {Object} parent parent to Dataobject
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.removeLokal = function (grandparent, parent, data, event) {
        
        if(grandparent.SB_slutdatum()){
            if(data.SBGL_lokal() && data.SBGL_lokal().value == '2'){
                // Vi har en lokal som är satt till Skelett
                vm.removeSkelettSRE(grandparent.SB_slutdatum());
            }
        }

        // ta bort en lokal-rad
        parent.$$.SBG_Lokal.rcc.remove(data);

    };


    /**
     * Funktion för att lägga till en strålbehandling grupp
     *
     * @method addSBGrupp
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.addSBGrupp = function (data, event) {
        // lägg till en ny rad till Lokaltabellen
        data.$$.SB_Grupp.rcc.add();

    };


    /**
     * Funktion för att lägga till en strålbehandling lokal
     * subscribers sätts sedan på berörda variabler
     *
     * @method addSBLokal
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.addSBLokal = function (data, event) {
        // lägg till en ny rad till Lokaltabellen
        var item = data.$$.SBG_Lokal.rcc.add();

    };

    vm.removeSkelettSRE = function(slutDatum){

        var sreObject = undefined;
        // Om strålbehandlingen vi tar bort har lokal Skelett
        ko.utils.arrayForEach(vm.$$.SRE(), function (sre) {
            // Vi kollar först att vi inte läst in objektet än, onödigt annars
            if(!sreObject && sre.SRE_datum() == slutDatum){
                ko.utils.arrayForEach(sre.$$.SRE_Händelse(), function (handelse) {
                    if(handelse.SREH_handelse() && handelse.SREH_handelse().value=='2'){
                        // Vi har hittat ett objekt som ev ska tas bort
                        sreObject = sre;
                    }
                });
            }
        });
        if(sreObject){
            // Vi har ett objekt som ev ska tas bort
            vm.getDialog('#rcc-remove-SRE').done(function () {
            // Om vi får ok av användaren att ta bort SSE-objektet tar vi bort det
            vm.$$.SRE.rcc.remove(sreObject);
        }).fail(function () {

        });
        }
    };

    /* SRE */


    /**
     * Funktion för att lägga till en tom SRE
     *
     * @method addSRE
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.addSRE = function (data, event) {
        // lägg till en ny rad till SRE tabellen
        vm.$$.SRE.rcc.add();

        // uppdatera accordion för tabellen
        vm.accordTable('#SE-accordion', true);
    };


    /**
     * Funktion för att lägga till en SRE med förutbestämda värden
     *
     * @method addSREStral
     * @param {Object} object Dataobject med förutbestämda värden
     */
    vm.addSREStral = function (object) {
        // lägg till en ny rad till SRE tabellen
        var item = vm.$$.SRE.rcc.add();

        // uppdatera accordion för tabellen
        vm.accordTable('#SE-accordion', true);

        // sätt SRE datum
        item.SRE_datum(object);

        vm.addSEHandelseStral(item);

    };


    /**
     * Funktion för att ta bort ett SRE.
     *
     * @method removeSRE
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.removeSRE = function (data, event) {
        // ta bort en rad till läkemedelstabellen
        vm.$$.SRE.rcc.remove(data);

        // uppdatera accordion för tabellen
        vm.accordTable('#SE-accordion', true);

    };


    /**
     * Funktion för att lägga till en SRE händelse
     * focus sätts därefter på den första läkemedelslinjens flik(om det finns kvarvarande linjer)
     *
     * @method addSEHandelse
     * @param {Object} data SRE
     * @param {Object} event Eventobject
     */
    vm.addSEHandelse = function (data, event) {
        // lägg till en ny rad till Händelsetabellen
        data.$$.SRE_Händelse.rcc.add();
    };


    /**
     * Funktion för att lägga till en SRE händelse
     * focus sätts därefter på den första läkemedelslinjens flik(om det finns kvarvarande linjer)
     *
     * @method addSEHandelse
     * @param {Object} data SRE
     * @param {Object} event Eventobject
     */
    vm.addSEHandelseStral = function (data) {
        // lägg till en ny rad till Händelsetabellen
        var item = data.$$.SRE_Händelse.rcc.add();
        item.SREH_handelse(item.SREH_handelse.rcc.term.listValues[2]);

    };


    /* Biobank */

    /**
     * Funktion för att lägga till en tom Biobanksprov
     *
     * @method addBiobanksprov
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.addBiobanksprov = function (data, event) {
        // lägg till en ny rad till Biobank tabellen
        var item = vm.$$.Biobank.rcc.add();

        // uppdatera accordion för tabellen
        vm.accordTable('#BB-accordion', true);
    };

    /**
     * Funktion för att lägga till en tom SRE
     *
     * @method addBiobanksprov
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.addBBProvtyp = function (data, event) {
        // lägg till en ny rad till Biobankprovtyp tabellen
        data.$$.BB_Biobanksprov.rcc.add();
    };


    /**
     * Funktion för att ta bort ett biobanksprov.
     *
     * @method removeBiobanksprov
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.removeBiobanksprov = function (data, event) {
        // ta bort en rad till läkemedelstabellen
        vm.$$.Biobank.rcc.remove(data);

        // uppdatera accordion för tabellen
        vm.accordTable('#BB-accordion', true);

    };


    /* LABBPROV */


    /**
     * Funktion för att lägga till ett labbprov
     * subscribers sätts sedan på berörda variabler
     *
     * @method addLabbprov
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.addLabbprov = function (data, event) {
        // lägg till en ny rad till Besökstabellen
        vm.$$.Labbprov.rcc.add();
    };


    /* LÄKEMEDEL */


    /**
     * Funktion för att lägga till en läkemedelslinje
     * subscribers sätts sedan på berörda variabler
     * focus sätts därefter på den tillagda läkemedelslinjens flik
     *
     * @method addLakemedel
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.addLakemedel = function (data, event) {
        // lägg till en ny rad till läkemedelstabellen
        var item = vm.$$.Läkemedel.rcc.add();

        // initializera substanslista
        item.LM_substans.listValues = ko.observableArray(undefined);

        vm.setLakemedelSubscribers(item);

        // uppdatera läkemedelstabbar
        // HACK: tabs('refresh') stöds ej?
        $('#prostataprocess-lmtabs').tabs();
        $('#prostataprocess-lmtabs').tabs('destroy');
        $('#prostataprocess-lmtabs').tabs();

        // fokusera på den senast tillagda fliken
        $('#prostataprocess-lmtabs').tabs('select', $('#prostataprocess-lmtabs >ul >li').size() - 1);
    };


    /**
     * Funktion för att ta bort en läkemedelslinje.
     * focus sätts därefter på den första läkemedelslinjens flik(om det finns kvarvarande linjer)
     *
     * @method removeLakemedel
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.removeLakemedel = function (data, event) {
       // ta bort en rad till läkemedelstabellen
        vm.$$.Läkemedel.rcc.remove(data);

        // uppdatera läkemedelstabbar
        // HACK: tabs('refresh') stöds ej?
        $('#prostataprocess-lmtabs').tabs('destroy');
        $('#prostataprocess-lmtabs').tabs();

        // om det fortfarande finns flikar, fokusera på den första
        if ($('#prostataprocess-lmtabs >ul >li').size() > 0)
            $('#prostataprocess-lmtabs').tabs('select', 0);
    };


    /**
     * funktion för att lägga till en Läkemedelsinsättning
     * subscribers sätts sedan på berörda variabler
     *
     * @method addLmInsattning
     * @param {Object} index Läkemedelsindex
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.addLmInsattning = function (index, data, event) {
        var id = '#LM-' + index + '-accordion';

        // lägg till en ny rad till Insättningstabellen
        var insattning = data.$$.LM_Insättning.rcc.add();

        // sätt värde till på Indikationslistan till  "Palliativ"
        insattning.LMI_indikation(insattning.LMI_indikation.rcc.term.listValues[0]);

        var handelse = insattning.$$.LMI_Händelse.rcc.add();

        // uppdatera accordion för tabellen
        if ($(id).hasClass('ui-accordion'))
            vm.accordTable(id, true);
        else
            vm.accordTable(id, false);
    };


    /**
     * Funktion för att ta bort en läkemedelsinsättning.
     *
     * @method removeLMInsattning
     * @param {Object} parent Läkemedel
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.removeLMInsattning = function (parent, data, event) {
        var id = '.LM-accordion';

        // ta bort en rad till läkemedelstabellen
        parent.$$.LM_Insättning.rcc.remove(data);

        // uppdatera accordion för tabellen
        vm.accordTable(id, true);
    };


    /**
     * funktion för att lägga till en Läkemedelshändelse
     * subscribers sätts sedan på berörda variabler
     *
     * @method addLmHandelse
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.addLmHandelse = function (data, event) {
        // lägg till en ny rad till Händelsetabellen
        data.$$.LMI_Händelse.rcc.add();
    };

    /**
     * Funktion för att ta bort en Läkemedelshändelse
     *
     * @method removeLmHandelse
     * @param {Object} grandparent grandparent to Dataobject
     * @param {Object} parent parent to Dataobject
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.removeLmHandelse = function (grandparent, parent, data, event) {
        // ta bort en lokal-rad
        parent.$$.LMI_Händelse.rcc.remove(data);
    };


    /**
     * funktion för att lägga till en läkemedelsutsättning
     * subscribers sätts sedan på berörda variabler
     *
     * @method addLmUtsattning
     * @param {Object} data Dataobject
     * @param {Object} event Eventobjekt
     */
    vm.addLmUtsattning = function (data, event) {
        // lägg till en ny rad till Utsättningstabellen
        data.$$.LMI_Utsättning.rcc.add();
    };


    /**
     * Funktion för att lägga till subscribers
     *
     * @method setLakemedelSubscribers
     * @param {Object} data Dataobject
     */
    vm.setLakemedelSubscribers = function (data) {
        /*data.LM_studielkm.subscribe(function (newValue) {
            if(newValue){
                notRequiredAndClear(data.LM_substans);
                required(data.LM_studielkmtxt);
                notRequiredAndClear(data.LM_ablatiotestisdatum);
            }
            else if(!newValue){
                notRequiredAndClear(data.LM_studielkmtxt);
                required(data.LM_substans);
                notRequiredAndClear(data.LM_ablatiotestisdatum);
            }
        }.bind(data));*/


        data.LM_ablatiotestis.subscribe(function (newValue) {
            if (newValue) {
                if (data.$$.LM_Insättning().length > 0)
                    data.$$.LM_Insättning.removeAll();
            }
            /*else{
                required(data.LM_substans);
                notRequiredAndClear(data.LM_studielkmtxt);
                notRequiredAndClear(data.LM_ablatiotestisdatum);
            }*/
        }.bind(data));

        data.LM_typ.subscribe(function (newValue) {
            data.LM_substans(undefined);
            data.LM_studielkm(undefined);
            data.LM_ablatiotestis(undefined);
            if (newValue) {
                if (newValue.value == '0')
                    vm.loadSubstances(data, 'ProstataProcess_CY');
                else if (newValue.value == '1')
                    vm.loadSubstances(data, 'ProstataProcess_ET');
                else if (newValue.value == '2')
                    vm.loadSubstances(data, 'ProstataProcess_BB');
                else if (newValue.value == '3')
                    vm.loadSubstances(data, 'ProstataProcess_IS');
                else if (newValue.value == '4')
                    vm.loadSubstances(data, 'ProstataProcess_OC');
            }
        }.bind(data));
    };


    /**
     * Funktion för att flagga formuländringar
     *
     * @method touchForm
     */
    vm.touchForm = function () {
        if (!vm.formHasChanged()) {
            vm.formHasChanged(true);

            // hämta dagens datum
            //var d = new Date();
            //var datum = d.getFullYear() + "-" + ((''+d.getMonth() + 1).length < 2 ? '0' : '') + (d.getMonth() + 1) + "-" + ((''+d.getDate()).length < 2 ? '0' : '') + d.getDate();
            //vm.U_datum(datum);
        }
    };


    /**
     * Funktion för att flagga tabelländringar
     *
     * @method touchTable
     * @param {String} table Tabell för touch
     */
    vm.touchTable = function (table) {
        if (table == 'root') {
            vm.rootHasChanged(true);
        } else if (table == 'B') {
            vm.tableBHasChanged(true);
        } else if (table == 'BD') {
            vm.tableBDHasChanged(true);
        } else if (table == 'SB') {
            vm.tableSBHasChanged(true);
        } else if (table == 'SE') {
            vm.tableSEHasChanged(true);
        } else if (table == 'LP') {
            vm.tableLPHasChanged(true);
        } else if (table == 'BB') {
            vm.tableBBHasChanged(true);
        } else if (table == 'LM') {
            vm.tableLMHasChanged(true);
            if ($("#prostataprocess-lmtabs").data("tabs")) {
                $("#prostataprocess-lmtabs").tabs("refresh");
            }
        }
    };


    /**
     * Funktion för att uppdatera en accordion
     *
     * @method accordTable
     * @param {String} table Tabell att uppdatera accordion för
     * @param {Boolean} destroy Skall accordion förstöras innan uppdatering
     */
    vm.accordTable = function (table, destroy) {
        if (destroy)
            $(table).accordion('destroy');
        $(table).accordion({
            collapsible: true,
            active: 0,
            autoHeight: false
        });
    };


    /**
     * Funktion för att returnera sorterade tabellrader
     *
     * @method sortTable
     * @param {Array} table Tabell att sortera
     * @param {String} key Variabelnamn att sortera på
     * @return {Array} Sorterad tabell
     */
    vm.sortTable = function (table, key, order) {
        return table.slice(0).sort(function (a, b) {
            var va = (a[key]() == null) ? "9999-99-99" : a[key]();
            var vb = (b[key]() == null) ? "9999-99-99" : b[key]();
            if (order == 'ascending')
                return va == vb ? 0 : (va < vb ? -1 : 1);
            else if (order == 'descending')
                return va == vb ? 0 : (va > vb ? -1 : 1);
        });
    };


    /**
     * Funktion för att returnera sorterade tabellrader
     *
     * @method sortTable
     * @param {Array} table Tabell att sortera
     * @param {String} key Variabelnamn att sortera på
     * @return {Array} Sorterad tabell
     */
    vm.sortPROM = function (table, prop, key, order) {
        return table.slice(0).sort(function (a, b) {
            var va = (prop == null) ? ((a[key] == null) ? "9999-99-99" : a[key]) : ((a[prop][key] == null) ? "9999-99-99" : a[prop][key]);
            var vb = (prop == null) ? ((b[key] == null) ? "9999-99-99" : b[key]) : ((b[prop][key] == null) ? "9999-99-99" : b[prop][key]);
            if (order == 'ascending')
                return va == vb ? 0 : (va < vb ? -1 : 1);
            else if (order == 'descending')
                return va == vb ? 0 : (va > vb ? -1 : 1);
        });
    };


    /**
     * Funktion för att returnera sorterade tabellrader i läkemedelstabell
     *
     * @method sortLMTable
     * @param {Array} table Tabell att sortera
     * @return {Array} Sorterad tabell
     */
    vm.sortLMTable = function (table) {

        return table.slice(0).sort(function (a, b) {
            var va = (vm.firstLMEventDate(a) == null) ? "9999-99-99" : vm.firstLMEventDate(a);
            var vb = (vm.firstLMEventDate(b) == null) ? "9999-99-99" : vm.firstLMEventDate(b);

            return va == vb ? 0 : (va < vb ? -1 : 1);
        });
    };


    /**
     * Funktion för att returnera sorterade tabellrader i insättningstabell
     *
     * @method sortInsTable
     * @param {Array} table Tabell att sortera
     * @return {Array} Sorterad tabell
     */
    vm.sortInsTable = function (table) {
        return table.slice(0).sort(function (a, b) {
            var va = (vm.firstInsEventDate(a) == null) ? "9999-99-99" : vm.firstInsEventDate(a);
            var vb = (vm.firstInsEventDate(b) == null) ? "9999-99-99" : vm.firstInsEventDate(b);
            return va == vb ? 0 : (va > vb ? -1 : 1);
        });
    };


    /**
     * Funktion för att returnera datum för första läkemedelshändelse
     *
     * @method firstLMEventDate
     * @param {Object} obj Läkemdel
     * @return {String} Datum för första läkemedelshändelse
     */
    vm.firstLMEventDate = function (obj) {
        var first = null;

        if (obj.LM_ablatiotestisdatum())
            first = obj.LM_ablatiotestisdatum();
        else
            ko.utils.arrayForEach(obj.$$.LM_Insättning(), function (insattning) {
                if (!first || vm.firstInsEventDate(insattning) < first)
                    first = vm.firstInsEventDate(insattning);
            });

        return first;
    };


    /**
     * Funktion för att returnera datum för första insättningshändelse
     *
     * @method firstInsEventDate
     * @param {Object} obj Insättning
     * @return {String} Datum för första insättningshändelse
     */
    vm.firstInsEventDate = function (obj) {
        var first = null;

        ko.utils.arrayForEach(obj.$$.LMI_Händelse(), function (handelse) {
            if (!first || handelse.LMIH_handelsedatum() < first)
                first = handelse.LMIH_handelsedatum();
        });

        return first;
    };


    vm.firstUtsEventDate = function (obj) {
        var first = null;

        ko.utils.arrayForEach(obj.$$.LMI_Utsättning(), function (utsattning) {
            if (!first || utsattning.LMIU_utsattningsdatum() < first)
                first = utsattning.LMIU_utsattningsdatum();
        });

        return first;
    };


    /**
     * Beräknande funktion för beräkna och returnera samtliga ordinationsdoser i regimen
     *
     * @method ordinationDoser
     * @return {Array} Array med samtliga ordinationsdoser i regimen
     */
    vm.studyList = ko.computed(function () {
        var studys = [];

        // loopa igenom alla Besök
        ko.utils.arrayForEach(vm.$$.Besök(), function (item) {
            if (item.B_studietxt())
                studys.push({
                    date: item.B_besokdatum(),
                    study: item.B_studietxt()
                });
        });

        // returnera studier
        return studys.reverse();
    });


    /**
     *	Specifika funktioner för översiktsregistret, som ännu ej är i generella hjälpfunktioner.
        */

    /**
     * Funktion för att visa "Info"-dialog.
     *
     * @method getInfoDialog
     * @param {*} [dialog] Identifierare för dialogen.
     */
    vm.getInfoDialogHTML = function (dialog) {
        return function (data, event) {
            $(dialog).dialog({
                dialogClass: 'rcc-dialog',
                position: [event.clientX + 15, event.clientY - 30],
                width: 500,
                modal: true,
                buttons: [{
                    text: 'OK',
                    priority: 'primary',
                    click: function () {
                        $(this).dialog('close');
                    }
                }]
            });
        };
    };


    /**
     * Funktion för att visa "Info"-dialog från javascript.
     *
     * @method getInfoDialog
     * @param {*} [dialog] Identifierare för dialogen.
     */
    vm.getInfoDialogJS = function (dialog) {
        $(dialog).dialog({
            dialogClass: 'rcc-dialog',
            position: [(formWidth / 2) - 150, 300],
            width: 500,
            modal: true,
            buttons: [{
                text: 'OK',
                priority: 'primary',
                click: function () {
                    $(this).dialog('close');
                }
            }]
        });
    };


    /**
     * Funktion för att visa en dialog.
     *
     * @method getDialog
     * @param {*} [dialog] Identifierare för dialogen.
     */
    vm.getDialog = function (dialog) {
        var def = $.Deferred();
        $(dialog).dialog({
            dialogClass: 'rcc-dialog',
            position: [(formWidth / 2) - 150, 300],
            width: 500,
            modal: true,
            buttons: [{
                text: 'Avbryt',
                click: function () {
                    $(this).dialog('close');
                    def.reject();
                },
                priority: 'secondary'
            }, {
                text: 'OK',
                click: function () {
                    $(this).dialog('close');
                    def.resolve();
                },
                priority: 'primary'
            }]
        });
        $(dialog).parent().find('[priority="primary"]').focus();

        return def.promise();
    };


    vm.splitTNM = function (object) {
        var result;
        var split;

        if (object.indexOf(" - ") != -1)
            split = object.split(" - ");
        else
            split = object.split(" ");

        if (split[0])
            result = split[0];
        else
            result = object;

        return result;
    };

    /**
         *
         *	Generera enkätkoder enligt Chrilles script
            *
            */
    vm.generateCode = function (besok, event) {
        var sequence = "PromPremDeltagarKod";
        var enkatKod = "8721";

        if (vm.U_PromEnkatkod())
            return;

        requestCount(requestCount() + 1); // Behövs denna??
        $.ajax({
            type: 'PUT',
            url: inca.getBaseUrl() + "api/Sequences/" + sequence + "/GetNextValue"
        }).done(function (data) {
            requestCount(requestCount() - 1);
            var code = "00000" + data;
            var encodedCode = vm.encodeParticipantCode(enkatKod + code.substr(-5), 7);
            vm.U_PromEnkatkod(encodedCode);
            vm.touchForm();
            alert("Vänligen spara patientöversikten snarast för att undvika att förlora koden.");
        }).fail(function (err) {
            alert("Det går av någon anledning inte att hämta sekvenser från INCA. Felmeddelande som fås är: " + err.toString());
        });

    };

    vm.encodeParticipantCode = function (codeBase, codeLength) {
        kontroll = vm.checksumLuhn(codeBase);
        codeBase = codeBase + "" + kontroll;
        var number = parseInt(codeBase);

        var characters = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "q", "w", "e", "r", "t", "y", "u", "i", "p", "a", "s", "d", "f", "g", "h", "j", "k", "z", "x", "c", "v", "b", "n", "m"];
        var res = "";

        var l = characters.length;

        while (number > 0) {
            res += characters[number % l];
            number = Math.floor(number / l);
        }
        //motsvaradne nollutfyllnad
        res = res + Array(codeLength + 1).join(characters[0]);

        return res.substr(0, codeLength);
    };

    vm.checksumLuhn = function (cc) {
        // digits 0-9 doubled with nines cast out
        var doubled = [0, 2, 4, 6, 8, 1, 3, 5, 7, 9];

        // convert to string
        var digits = cc + '';
        digits = digits.split('');

        // alternate between summing the digits
        // or the result of doubling the digits and
        // casting out nines (see Luhn description)
        var alt = false;
        var total = 0;
        while (digits.length) {
            var d = Number(digits.pop());
            total += (alt ? doubled[d] : d);
            alt = !alt;
        }
        return total % 10;
    };

    /**
     *  Datumberäkningar för basdata
     */


    /**
     *  Beräkna datum för brytpunktssamtal från besöksrader
     *  VAD GÖRA???
     */
    ko.computed(function () {
        var brytpunktBesok = ko.utils.arrayFirst(vm.$$.Besök(), function (besok) {
            if (besok.B_brytpunktsbesok()) {
                return besok;
            }
        });

        if (!brytpunktBesok) {
            if (vm.U_brytpunktssamtalsdatum()) {
                vm.U_brytpunktssamtalsdatum(undefined);
            }
        } else {
            if (vm.U_brytpunktssamtalsdatum() != brytpunktBesok.B_besokdatum()) {
                vm.U_brytpunktssamtalsdatum(brytpunktBesok.B_besokdatum());
            }
        }
    });

    /**
     *  Beräkna CRPC datum från besöksrader
     */
    ko.computed(function () {
        var crpcBesok = ko.utils.arrayFirst(vm.$$.Besök(), function (besok) {
            if (besok.B_crpc() && besok.B_crpc().value == '1') {
                return besok;
            }
        });

        if (!crpcBesok) {
            vm.isCRPCSet(false);
        } else {
            if (vm.U_crpcdatum.peek() != crpcBesok.B_besokdatum()) {
                vm.U_crpcdatum(crpcBesok.B_besokdatum());
            }
            vm.isCRPCSet(true);
        }
    });

    /**
     *  Beräkna datum för start av palliativ hemsjukvård från besöksrader
     */
    ko.computed(function () {
        var pallHemSjk = ko.utils.arrayFirst(vm.$$.Besök(), function (besok) {
            if (besok.B_palliativvard() && besok.B_palliativvard().value == '1') {
                return besok;
            }
        });

        if (!pallHemSjk) {
            vm.isPallHemSet(false);
        } else {
            if (vm.U_pallHemSjkvrd.peek() != pallHemSjk.B_besokdatum()) {
                vm.U_pallHemSjkvrd(pallHemSjk.B_besokdatum());
            }
            vm.isPallHemSet(true);
        }
    });

    /**
     *  Beräkna datum för första metastas från bilddiagnostik
     */
    vm.forstaMetastasIsCalculated = ko.observable(false);
    vm.calculatingForstaMetastas = ko.observable(false);

    vm.U_metastasdatum.subscribe(function (newValue) {
        if(newValue){
            if(!vm.calculatingForstaMetastas())
                vm.forstaMetastasIsCalculated(false);
        }
    });

    ko.computed(function () {
        vm.calculatingForstaMetastas(true);
        var metastasisDtm = [];

        if(vm.forstaMetastasIsCalculated())
            vm.U_metastasdatum(undefined);

        ko.utils.arrayForEach(vm.$$.Bilddiagnostik(), function (bd) {
            if (bd.$$.BD_Metastas().length >= 1) {
                metastasisDtm.push(bd.BD_datum());
            }
        });

        if (metastasisDtm.length > 0) {
            metastasisDtm.sort(function (a, b) {
                var keyA = new Date(a),
                    keyB = new Date(b);
                // Compare the 2 dates
                if (keyA < keyB) return -1;
                if (keyA > keyB) return 1;
                return 0;
            });

            firstDtm = metastasisDtm[0];
            baseDtm = vm.U_metastasdatum.peek();

            if (baseDtm != firstDtm) {
                var parsedFirstDtm = RCC.Utils.parseYMD(firstDtm);
                var parsedBaseDtm = RCC.Utils.parseYMD(baseDtm);
                if (parsedBaseDtm) {
                    if (parsedFirstDtm.getTime() < parsedBaseDtm.getTime()) {
                        vm.U_metastasdatum(firstDtm);
                        vm.forstaMetastasIsCalculated(true);
                    }
                } else {
                    vm.U_metastasdatum(firstDtm);
                    vm.forstaMetastasIsCalculated(true);
                }
            }
            else{
                vm.forstaMetastasIsCalculated(true);
            }
        }
        vm.calculatingForstaMetastas(false);
    });

    /**
     *  Beräkna datum för start av hormonbehandling från läkemedelsrader
     */
    vm.hormonStartIsCalculated = ko.observable(false);
    vm.calculatingHormonStart = ko.observable(false);

    vm.U_hormonstartdatum.subscribe(function (newValue) {
        if(newValue){
            if(!vm.calculatingHormonStart())
                vm.hormonStartIsCalculated(false);
        }
    });

    ko.computed(function () {
        vm.calculatingHormonStart(true);
        var lakemedelDtm = [];

        if(vm.hormonStartIsCalculated())
            vm.U_hormonstartdatum(undefined);

        ko.utils.arrayForEach(vm.$$.Läkemedel(), function (lkm) {
            if (lkm.LM_ablatiotestis() && lkm.LM_ablatiotestisdatum()) {
                lakemedelDtm.push(lkm.LM_ablatiotestisdatum());
            }
            else if(lkm.LM_typ() && lkm.LM_typ().value == '1'){
                ko.utils.arrayForEach(lkm.$$.LM_Insättning(), function (ins) {
                    ko.utils.arrayForEach(ins.$$.LMI_Händelse(), function (handelse) {
                        if (handelse.LMIH_handelsedatum())
                            lakemedelDtm.push(handelse.LMIH_handelsedatum());
                    });
                });
            }
        });

        if (lakemedelDtm.length > 0) {
            lakemedelDtm.sort(function (a, b) {
                var keyA = new Date(a),
                    keyB = new Date(b);
                // Compare the 2 dates
                if (keyA < keyB) return -1;
                if (keyA > keyB) return 1;
                return 0;
            });

            firstDtm = lakemedelDtm[0];
            baseDtm = vm.U_hormonstartdatum.peek();

            if (baseDtm != firstDtm) {
                var parsedFirstDtm = RCC.Utils.parseYMD(firstDtm);
                var parsedBaseDtm = RCC.Utils.parseYMD(baseDtm);
                if (parsedBaseDtm) {
                    if (parsedFirstDtm.getTime() < parsedBaseDtm.getTime()) {
                        vm.U_hormonstartdatum(firstDtm);
                        vm.hormonStartIsCalculated(true);
                    }
                } else {
                    vm.U_hormonstartdatum(firstDtm);
                    vm.hormonStartIsCalculated(true);
                }
            }
            else{
                vm.hormonStartIsCalculated(true);
            }
        }

        vm.calculatingHormonStart(false);
    });



    /****************************************
     *										*
        *				Validering				*
        *										*
        ****************************************/

    // Variabler i rot-tabellen
    notRequired(vm.U_AMstartdatum);
    notRequired(vm.U_AMslutdatum);
    notRequired(vm.U_psadatum);
    notRequired(vm.U_hormonstartdatum);
    notRequired(vm.U_crpcdatum);
    notRequired(vm.U_metastasdatum);
    notRequired(vm.U_pallHemSjkvrd);

    notRequired(vm.U_diadat);
    notRequired(vm.U_tstadDia);
    notRequired(vm.U_nstadDia);
    notRequired(vm.U_mstadDia);
    notRequired(vm.U_gleassaGrad1);
    notRequired(vm.U_gleassaGrad2);
    notRequired(vm.U_gleassa);
    notRequired(vm.U_rtTyp);
    notRequired(vm.U_rpOpdat);
    notRequired(vm.U_rtKurSlutdat);
    notRequired(vm.U_rtPostopSlutdat);
    notRequired(vm.U_datagranskat);

    // Datumvariabler - ej framtida datum
    vm.U_AMstartdatum.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.U_AMstartdatum, {
        ignoreMissingValues: true
    }));
    addNotAfterDeathValidation(vm.U_AMstartdatum);

    vm.U_AMslutdatum.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.U_AMslutdatum, {
        ignoreMissingValues: true
    }));
    addNotAfterDeathValidation(vm.U_AMslutdatum);

    vm.U_psadatum.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.U_psadatum, {
        ignoreMissingValues: true
    }));
    addNotAfterDeathValidation(vm.U_psadatum);

    vm.U_hormonstartdatum.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.U_hormonstartdatum, {
        ignoreMissingValues: true
    }));
    addNotAfterDeathValidation(vm.U_hormonstartdatum);

    vm.U_crpcdatum.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.U_crpcdatum, {
        ignoreMissingValues: true
    }));
    addNotAfterDeathValidation(vm.U_crpcdatum);

    vm.U_metastasdatum.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.U_metastasdatum, {
        ignoreMissingValues: true
    }));
    addNotAfterDeathValidation(vm.U_metastasdatum);

    vm.U_pallHemSjkvrd.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.U_pallHemSjkvrd, {
        ignoreMissingValues: true
    }));
    addNotAfterDeathValidation(vm.U_pallHemSjkvrd);
    
    vm.U_diadat.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.U_diadat, {
        ignoreMissingValues: true
    }));
    addNotAfterDeathValidation(vm.U_diadat);

    vm.U_rpOpdat.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.U_rpOpdat, {
        ignoreMissingValues: true
    }));
    addNotAfterDeathValidation(vm.U_rpOpdat);

    vm.U_rtKurSlutdat.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.U_rtKurSlutdat, {
        ignoreMissingValues: true
    }));
    addNotAfterDeathValidation(vm.U_rtKurSlutdat);

    vm.U_rtPostopSlutdat.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.U_rtPostopSlutdat, {
        ignoreMissingValues: true
    }));
    addNotAfterDeathValidation(vm.U_rtPostopSlutdat);

    // Ej slut före start
    vm.U_AMslutdatum.rcc.validation.add(new RCC.Validation.Tests.NotBeforeDate(vm.U_AMslutdatum, {
        dependables: [vm.U_AMstartdatum], ignoreMissingValues: true
    }));

    ko.computed(function () {
        if (!vm.U_AMstartdatum()) {
            clear(vm.U_AMslutdatum);
        }
    });


    vm.U_gleassaGrad1.rcc.validation.add(new RCC.Validation.Tests.Integer(vm.U_gleassaGrad1, {min: 1, max: 5,
        ignoreMissingValues: true
    }));
    vm.U_gleassaGrad2.rcc.validation.add(new RCC.Validation.Tests.Integer(vm.U_gleassaGrad2, {min: 1, max: 5,
        ignoreMissingValues: true
    }));


    resetValidation(vm.U_spsa);
    vm.U_spsa.rcc.validation.add(new RCC.Validation.Tests.Decimal(vm.U_spsa, {
        decimalPlaces: 3,
        min: 0.001,
        ignoreMissingValues: true
    }));

    vm.U_datagranskat.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.U_datagranskat, {
        ignoreMissingValues: true
    }));

    // Besöksvalidering, tända släcka funktion anpassning
    ko.computed(function () {
        if (vm.$$.Besök().length > 0) {
            for (var i = 0; i < vm.$$.Besök().length; i++) {

                resetValidation(vm.$$.Besök()[i].B_besokdatum);

                addNotAfterDeathValidation(vm.$$.Besök()[i].B_besokdatum);
                vm.$$.Besök()[i].B_besokdatum.rcc.validation.add(new RCC.Validation.Tests.NotMissing(vm.$$.Besök()[i].B_besokdatum, {
                    ignoreMissingValues: true
                }));
                vm.$$.Besök()[i].B_besokdatum.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.$$.Besök()[i].B_besokdatum, {
                    ignoreMissingValues: true
                }));

                /*resetValidation(vm.$$.Besök()[i].B_biobankDatum);
                vm.$$.Besök()[i].B_biobankDatum.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.$$.Besök()[i].B_biobankDatum, {
                    ignoreMissingValues: true
                }));*/

                // Ta bort Komplett- och Partiell respons i listan för Sammantagen klinisk bedömning (erätts av Respons)
                removeValueInList(vm.$$.Besök()[i].B_klinbed, '0');
                removeValueInList(vm.$$.Besök()[i].B_klinbed, '4');

                if (vm.U_crpcdatum() && (!vm.$$.Besök()[i].B_crpc() || vm.$$.Besök()[i].B_crpc().value != 1)) {
                    notRequired(vm.$$.Besök()[i].B_crpc);
                } else if (vm.$$.Besök()[i].B_telefonbesok()) {
                    notRequired(vm.$$.Besök()[i].B_crpc);
                } else if (vm.$$.Besök()[i].B_sskbesok()) {
                    notRequiredAndClear(vm.$$.Besök()[i].B_crpc);
                } else {
                    required(vm.$$.Besök()[i].B_crpc);
                }

                if ((vm.U_pallHemSjkvrd() && (!vm.$$.Besök()[i].B_palliativvard() || vm.$$.Besök()[i].B_palliativvard().value == 1)) || vm.$$.Besök()[i].B_telefonbesok()) {
                    notRequired(vm.$$.Besök()[i].B_palliativvard);
                } else {
                    required(vm.$$.Besök()[i].B_palliativvard);
                }

                if (!vm.$$.Besök()[i].B_studie() || vm.$$.Besök()[i].B_studie().value != 1) {
                    notRequiredAndClear(vm.$$.Besök()[i].B_studietxt);
                } else {
                    required(vm.$$.Besök()[i].B_studietxt);
                }


                if (vm.$$.Besök()[i].B_ejecogwho()) {
                    notRequiredAndClear(vm.$$.Besök()[i].B_ecogwho);
                } else if (vm.$$.Besök()[i].B_telefonbesok()) {
                    notRequired(vm.$$.Besök()[i].B_ecogwho);
                } else {
                    required(vm.$$.Besök()[i].B_ecogwho);
                }

                if (vm.$$.Besök()[i].B_sskbesok()) {
                    if (vm.$$.Besök()[i].B_telefonbesok()) {
                        //notRequired(vm.$$.Besök()[i].B_ecogwho);
                        notRequired(vm.$$.Besök()[i].B_multikonf);
                        //notRequired(vm.$$.Besök()[i].B_biobank);
                    } else {
                        //required(vm.$$.Besök()[i].B_ecogwho);
                        required(vm.$$.Besök()[i].B_multikonf);
                        //required(vm.$$.Besök()[i].B_biobank);
                    }
                    notRequiredAndClear(vm.$$.Besök()[i].B_klinbed);
                    notRequired(vm.$$.Besök()[i].B_studie);
                    clear(vm.$$.Besök()[i].B_brytpunktsbesok);
                } else if (vm.$$.Besök()[i].B_telefonbesok()) {
                    //notRequired(vm.$$.Besök()[i].B_ecogwho);
                    notRequired(vm.$$.Besök()[i].B_multikonf);
                    //notRequired(vm.$$.Besök()[i].B_biobank);
                    notRequired(vm.$$.Besök()[i].B_klinbed);
                    notRequired(vm.$$.Besök()[i].B_studie);
                    //clear(vm.$$.Besök()[i].B_brytpunktsbesok);
                } else {
                    //required(vm.$$.Besök()[i].B_ecogwho);
                    required(vm.$$.Besök()[i].B_multikonf);
                    //required(vm.$$.Besök()[i].B_biobank);
                    required(vm.$$.Besök()[i].B_klinbed);
                    required(vm.$$.Besök()[i].B_studie);
                }

                vm.$$.Besök()[i].B_crpc.subscribe(function (newValue) {
                    if (!newValue || newValue.value == 0) {
                        vm.U_crpcdatum(undefined);
                    }
                });

                vm.$$.Besök()[i].B_palliativvard.subscribe(function (newValue) {
                    if (!newValue || newValue.value == 0) {
                        vm.U_pallHemSjkvrd(undefined);
                    }
                });


                /*if (!vm.$$.Besök()[i].B_biobank() || vm.$$.Besök()[i].B_biobank().value != 1) {
                    notRequiredAndClear(vm.$$.Besök()[i].B_biobankprovtyp);
                    notRequiredAndClear(vm.$$.Besök()[i].B_biobankDatum);
                } else {
                    required(vm.$$.Besök()[i].B_biobankprovtyp);
                    required(vm.$$.Besök()[i].B_biobankDatum);
                }*/

            }

        }

    });

    // Bilddiagnostiksvalidering, tända släcka funktion anpassning
    ko.computed(function () {
        if (vm.$$.Bilddiagnostik().length > 0) {
            for (var i = 0; i < vm.$$.Bilddiagnostik().length; i++) {

                resetValidation(vm.$$.Bilddiagnostik()[i].BD_datum);

                addNotAfterDeathValidation(vm.$$.Bilddiagnostik()[i].BD_datum);

                vm.$$.Bilddiagnostik()[i].BD_datum.rcc.validation.add(new RCC.Validation.Tests.NotMissing(vm.$$.Bilddiagnostik()[i].BD_datum, {
                    ignoreMissingValues: true
                }));

                vm.$$.Bilddiagnostik()[i].BD_datum.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.$$.Bilddiagnostik()[i].BD_datum, {
                    ignoreMissingValues: true
                }));

                notRequired(vm.$$.Bilddiagnostik()[i].BD_radbed);

                resetValidation(vm.$$.Bilddiagnostik()[i].BD_bsi);
                vm.$$.Bilddiagnostik()[i].BD_bsi.rcc.validation.add(new RCC.Validation.Tests.Decimal(vm.$$.Bilddiagnostik()[i].BD_bsi, {
                    decimalPlaces: 1,
                    min: 0,
                    max: 100,
                    ignoreMissingValues: true
                }));

                // Ta bort MR och CT i bildundersökning
                removeValueInList(vm.$$.Bilddiagnostik()[i].BD_radundertyp, '2');
                removeValueInList(vm.$$.Bilddiagnostik()[i].BD_radundertyp, '3');

                if (!vm.$$.Bilddiagnostik()[i].BD_radundertyp() || vm.$$.Bilddiagnostik()[i].BD_radundertyp().value != 9) {
                    notRequiredAndClear(vm.$$.Bilddiagnostik()[i].BD_radundtypannan);
                } else {
                    required(vm.$$.Bilddiagnostik()[i].BD_radundtypannan);
                }

                var undertabell = vm.$$.Bilddiagnostik()[i].$$.BD_Metastas();
                for (var n = 0; n < undertabell.length; n++) {
                    if (!undertabell[n].BDM_metastaslokal() || undertabell[n].BDM_metastaslokal().value != 5 || undertabell[n].BDM_metastaslokal().value != 5) {
                        notRequiredAndClear(undertabell[n].BDM_metastaslokalann);
                    } else {
                        required(undertabell[n].BDM_metastaslokalann);
                    }
                }
            }
        }
    });

    // Strålbehandlingsvalidering, tända släcka funktion anpassning
    ko.computed(function () {
        if (vm.$$.Strålbehandling().length > 0) {
            for (var i = 0; i < vm.$$.Strålbehandling().length; i++) {
                // Variabler som inte används?
                notRequired(vm.$$.Strålbehandling()[i].SB_frakdos2);
                notRequired(vm.$$.Strålbehandling()[i].SB_slutdos2);

                resetValidation(vm.$$.Strålbehandling()[i].SB_slutdatum);

                addNotAfterDeathValidation(vm.$$.Strålbehandling()[i].SB_slutdatum);

                vm.$$.Strålbehandling()[i].SB_slutdatum.rcc.validation.add(new RCC.Validation.Tests.NotMissing(vm.$$.Strålbehandling()[i].SB_slutdatum, {
                    ignoreMissingValues: true
                }));

                vm.$$.Strålbehandling()[i].SB_slutdatum.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.$$.Strålbehandling()[i].SB_slutdatum, {
                    ignoreMissingValues: true
                }));


                var tabell = vm.$$.Strålbehandling()[i].$$.SB_Grupp();
                for (var n = 0; n < tabell.length; n++) {
                    // Variabler som inte används?
                    notRequired(tabell[n].SBL_lokal2);
                    notRequired(tabell[n].SBL_lokalann2);
                    notRequired(tabell[n].SBL_lokalskelett2);

                    resetValidation(tabell[n].SBG_slutdos);
                    tabell[n].SBG_slutdos.rcc.validation.add(new RCC.Validation.Tests.DynamicMinMaxInteger(tabell[n].SBG_slutdos, {
                        min: tabell[n].SBG_frakdos, ignoreMissingValues: true, errorMessage: 'Fraktionsdos får inte vara högre än slutdos'
                    }));

                    var undertabell = tabell[n].$$.SBG_Lokal();
                    for (var m = 0; m < undertabell.length; m++) {
                        if (!undertabell[m].SBGL_lokal() || undertabell[m].SBGL_lokal().value != 2) {
                            notRequiredAndClear(undertabell[m].SBGL_lokalskelett);
                        } else {
                            required(undertabell[m].SBGL_lokalskelett);
                        }

                        if (!undertabell[m].SBGL_lokal() || undertabell[m].SBGL_lokal().value != 5) {
                            notRequiredAndClear(undertabell[m].SBGL_lokalann);
                        } else {
                            required(undertabell[m].SBGL_lokalann);
                        }
                        if (!undertabell[m].SBGL_lokal() || undertabell[m].SBGL_lokal().value != 6) {
                            notRequiredAndClear(undertabell[m].SBGL_lokallymf);
                        } else {
                            required(undertabell[m].SBGL_lokallymf);
                        }
                    }




                }
            }
        }
    });


    // Kontrollerar att multipeln mellan fraktionsdos och slutdos är jämn.
    // Visar ett varningsmeddelande om multipeln är ojämn. Visar bara varningsmeddelande i de fall då motsvarande tabb samt accordion index är de som visas just nu.
    ko.computed(function () {
        if (vm.$$.Strålbehandling().length > 0) {
            for (var i = 0; i < vm.$$.Strålbehandling().length; i++) {
                var tabell = vm.$$.Strålbehandling()[i].$$.SB_Grupp();
                for (var n = 0; n < tabell.length; n++) {
                    if ((tabell[n].SBG_slutdos() && tabell[n].SBG_frakdos()) && ((tabell[n].SBG_slutdos() % tabell[n].SBG_frakdos()) !== 0)) {
                        if ($("#prostataprocess-tabs").data().tabs && $("#prostataprocess-tabs").tabs('option', 'active') === 3) { // Visa endast dialogen om vald tab är 3 == strålbehandling.
                            if (vm.$$.Strålbehandling().slice().reverse()[$('#SB-accordion').accordion("option", "active")].$$.SB_Grupp() === tabell) {
                                vm.getInfoDialogJS('#rcc-sre-slutdos-jamn-multipel');
                            }
                        }
                    }
                }
            }
        }
    });

    // SRE validering
    ko.computed(function () {
        if (vm.$$.SRE().length > 0) {
            for (var i = 0; i < vm.$$.SRE().length; i++) {

                resetValidation(vm.$$.SRE()[i].SRE_datum);

                addNotAfterDeathValidation(vm.$$.SRE()[i].SRE_datum)
                vm.$$.SRE()[i].SRE_datum.rcc.validation.add(new RCC.Validation.Tests.NotMissing(vm.$$.SRE()[i].SRE_datum, {
                    ignoreMissingValues: true
                }));
                vm.$$.SRE()[i].SRE_datum.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.$$.SRE()[i].SRE_datum, {
                    ignoreMissingValues: true
                }));
            }
        }
    });

    // Biobank validering
    ko.computed(function () {
        if (vm.$$.Biobank().length > 0) {
            for (var i = 0; i < vm.$$.Biobank().length; i++) {

                resetValidation(vm.$$.Biobank()[i].BB_biobanksdatum);
                vm.$$.Biobank()[i].BB_biobanksdatum.rcc.validation.add(new RCC.Validation.Tests.NotMissing(vm.$$.Biobank()[i].BB_biobanksdatum, {
                    ignoreMissingValues: true
                }));
                vm.$$.Biobank()[i].BB_biobanksdatum.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.$$.Biobank()[i].BB_biobanksdatum, {
                    ignoreMissingValues: true
                }));
            }
        }
    });

    RCC.Validation.Tests.DateNotInList = function (date, labbList, opt) {
        return function () {
            var d = date();
            if (opt.ignoreMissingValues && !RCC.Validation.Tests._hasValue(d, true))
                return { result: 'ok' };

            var parsedDate = RCC.Utils.parseYMD(d);

            for (var i = 0; i < labbList.length; i++) {
                var labbProv = labbList[i];
                var prevDate = RCC.Utils.parseYMD(labbProv.LP_datum());

                if (date === labbProv.LP_datum) //Jämför objekt, inte datum.
                    continue;

                if (parsedDate && prevDate && (parsedDate.getTime() === prevDate.getTime())) {
                    return { result: 'failed', message: 'Datum för labbprov kan inte vara samma som tidigare datum för labbprov.' };
                }
            };
            return { result: 'ok' };
        };
    };

    // Labbprov validering, minst ett av värdena måste fyllas i logik som styr valideringen här
    ko.computed(function () {

        if (vm.$$.Labbprov().length > 0) {
            for (var i = 0; i < vm.$$.Labbprov().length; i++) {

                resetValidation(vm.$$.Labbprov()[i].LP_datum);

                addNotAfterDeathValidation(vm.$$.Labbprov()[i].LP_datum);
                vm.$$.Labbprov()[i].LP_datum.rcc.validation.add(new RCC.Validation.Tests.NotMissing(vm.$$.Labbprov()[i].LP_datum, {
                    ignoreMissingValues: true
                }));
                vm.$$.Labbprov()[i].LP_datum.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.$$.Labbprov()[i].LP_datum, {
                    ignoreMissingValues: true
                }));
                vm.$$.Labbprov()[i].LP_datum.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(vm.$$.Labbprov()[i].LP_datum, {
                    ignoreMissingValues: true
                }));
                vm.$$.Labbprov()[i].LP_datum.rcc.validation.add(new RCC.Validation.Tests.DateNotInList(vm.$$.Labbprov()[i].LP_datum, vm.$$.Labbprov(), {
                    ignoreMissingValues: true
                }));

                // Set them to notRequired so that empty values are OK
                notRequired(vm.$$.Labbprov()[i].LP_psa);
                notRequired(vm.$$.Labbprov()[i].LP_alp);
                notRequired(vm.$$.Labbprov()[i].LP_krea);
                notRequired(vm.$$.Labbprov()[i].LP_hb);
                notRequired(vm.$$.Labbprov()[i].LP_testosteron);
                notRequired(vm.$$.Labbprov()[i].LP_lpk);
                notRequired(vm.$$.Labbprov()[i].LP_tpk);
                notRequired(vm.$$.Labbprov()[i].LP_neutrofila);

                // Add dynamic min-max values for decimal
                vm.$$.Labbprov()[i].LP_psa.rcc.validation.add(new RCC.Validation.Tests.Decimal(vm.$$.Labbprov()[i].LP_psa, {
                    decimalPlaces: 2,
                    min: 0.01,
                    ignoreMissingValues: true
                }));

                vm.$$.Labbprov()[i].LP_alp.rcc.validation.add(new RCC.Validation.Tests.Decimal(vm.$$.Labbprov()[i].LP_alp, {
                    decimalPlaces: 1,
                    min: 0.1,
                    ignoreMissingValues: true
                }));

                vm.$$.Labbprov()[i].LP_krea.rcc.validation.add(new RCC.Validation.Tests.Integer(vm.$$.Labbprov()[i].LP_krea, {
                    min: 1,
                    ignoreMissingValues: true
                }));

                vm.$$.Labbprov()[i].LP_hb.rcc.validation.add(new RCC.Validation.Tests.Integer(vm.$$.Labbprov()[i].LP_hb, {
                    min: 1,
                    ignoreMissingValues: true
                }));

                vm.$$.Labbprov()[i].LP_testosteron.rcc.validation.add(new RCC.Validation.Tests.Decimal(vm.$$.Labbprov()[i].LP_testosteron, {
                    decimalPlaces: 2,
                    min: 0.01,
                    ignoreMissingValues: true
                }));
                
                vm.$$.Labbprov()[i].LP_lpk.rcc.validation.add(new RCC.Validation.Tests.Decimal(vm.$$.Labbprov()[i].LP_lpk, {
                    decimalPlaces: 1,
                    min: 0.1,
                    ignoreMissingValues: true
                }));
                
                vm.$$.Labbprov()[i].LP_tpk.rcc.validation.add(new RCC.Validation.Tests.Integer(vm.$$.Labbprov()[i].LP_tpk, {
                    min: 1,
                    ignoreMissingValues: true
                }));
                
                vm.$$.Labbprov()[i].LP_neutrofila.rcc.validation.add(new RCC.Validation.Tests.Decimal(vm.$$.Labbprov()[i].LP_neutrofila, {
                    decimalPlaces: 1,
                    min: 0.1,
                    ignoreMissingValues: true
                }));

                // Add the new validation so that at least one of the fields has to have a value
                vm.$$.Labbprov()[i].LP_psa.rcc.validation.add(new RCC.Validation.Tests.atLeastOneValue(vm.$$.Labbprov()[i].LP_psa, {
                    dependables: [vm.$$.Labbprov()[i].LP_psa, vm.$$.Labbprov()[i].LP_alp, vm.$$.Labbprov()[i].LP_krea, vm.$$.Labbprov()[i].LP_hb, vm.$$.Labbprov()[i].LP_lpk, vm.$$.Labbprov()[i].LP_neutrofila, vm.$$.Labbprov()[i].LP_tpk, vm.$$.Labbprov()[i].LP_testosteron],
                    ignoreMissingValues: true
                }));
                vm.$$.Labbprov()[i].LP_alp.rcc.validation.add(new RCC.Validation.Tests.atLeastOneValue(vm.$$.Labbprov()[i].LP_alp, {
                    dependables: [vm.$$.Labbprov()[i].LP_psa, vm.$$.Labbprov()[i].LP_alp, vm.$$.Labbprov()[i].LP_krea, vm.$$.Labbprov()[i].LP_hb, vm.$$.Labbprov()[i].LP_lpk, vm.$$.Labbprov()[i].LP_neutrofila, vm.$$.Labbprov()[i].LP_tpk, vm.$$.Labbprov()[i].LP_testosteron],
                    ignoreMissingValues: true
                }));
                vm.$$.Labbprov()[i].LP_krea.rcc.validation.add(new RCC.Validation.Tests.atLeastOneValue(vm.$$.Labbprov()[i].LP_krea, {
                    dependables: [vm.$$.Labbprov()[i].LP_psa, vm.$$.Labbprov()[i].LP_alp, vm.$$.Labbprov()[i].LP_krea, vm.$$.Labbprov()[i].LP_hb, vm.$$.Labbprov()[i].LP_lpk, vm.$$.Labbprov()[i].LP_neutrofila, vm.$$.Labbprov()[i].LP_tpk, vm.$$.Labbprov()[i].LP_testosteron],
                    ignoreMissingValues: true
                }));
                vm.$$.Labbprov()[i].LP_hb.rcc.validation.add(new RCC.Validation.Tests.atLeastOneValue(vm.$$.Labbprov()[i].LP_hb, {
                    dependables: [vm.$$.Labbprov()[i].LP_psa, vm.$$.Labbprov()[i].LP_alp, vm.$$.Labbprov()[i].LP_krea, vm.$$.Labbprov()[i].LP_hb, vm.$$.Labbprov()[i].LP_lpk, vm.$$.Labbprov()[i].LP_neutrofila, vm.$$.Labbprov()[i].LP_tpk, vm.$$.Labbprov()[i].LP_testosteron],
                    ignoreMissingValues: true
                }));
                vm.$$.Labbprov()[i].LP_lpk.rcc.validation.add(new RCC.Validation.Tests.atLeastOneValue(vm.$$.Labbprov()[i].LP_lpk, {
                    dependables: [vm.$$.Labbprov()[i].LP_psa, vm.$$.Labbprov()[i].LP_alp, vm.$$.Labbprov()[i].LP_krea, vm.$$.Labbprov()[i].LP_hb, vm.$$.Labbprov()[i].LP_lpk, vm.$$.Labbprov()[i].LP_neutrofila, vm.$$.Labbprov()[i].LP_tpk, vm.$$.Labbprov()[i].LP_testosteron],
                    ignoreMissingValues: true
                }));
                vm.$$.Labbprov()[i].LP_neutrofila.rcc.validation.add(new RCC.Validation.Tests.atLeastOneValue(vm.$$.Labbprov()[i].LP_neutrofila, {
                    dependables: [vm.$$.Labbprov()[i].LP_psa, vm.$$.Labbprov()[i].LP_alp, vm.$$.Labbprov()[i].LP_krea, vm.$$.Labbprov()[i].LP_hb, vm.$$.Labbprov()[i].LP_lpk, vm.$$.Labbprov()[i].LP_neutrofila, vm.$$.Labbprov()[i].LP_tpk, vm.$$.Labbprov()[i].LP_testosteron],
                    ignoreMissingValues: true
                }));
                vm.$$.Labbprov()[i].LP_tpk.rcc.validation.add(new RCC.Validation.Tests.atLeastOneValue(vm.$$.Labbprov()[i].LP_tpk, {
                    dependables: [vm.$$.Labbprov()[i].LP_psa, vm.$$.Labbprov()[i].LP_alp, vm.$$.Labbprov()[i].LP_krea, vm.$$.Labbprov()[i].LP_hb, vm.$$.Labbprov()[i].LP_lpk, vm.$$.Labbprov()[i].LP_neutrofila, vm.$$.Labbprov()[i].LP_tpk, vm.$$.Labbprov()[i].LP_testosteron],
                    ignoreMissingValues: true
                }));
                vm.$$.Labbprov()[i].LP_testosteron.rcc.validation.add(new RCC.Validation.Tests.atLeastOneValue(vm.$$.Labbprov()[i].LP_testosteron, {
                    dependables: [vm.$$.Labbprov()[i].LP_psa, vm.$$.Labbprov()[i].LP_alp, vm.$$.Labbprov()[i].LP_krea, vm.$$.Labbprov()[i].LP_hb, vm.$$.Labbprov()[i].LP_lpk, vm.$$.Labbprov()[i].LP_neutrofila, vm.$$.Labbprov()[i].LP_tpk, vm.$$.Labbprov()[i].LP_testosteron],
                    ignoreMissingValues: true
                }));
            }
        }
    });

    // Läkemedel validering, tända släcka funktion anpassning
    ko.computed(function () {
        if (vm.$$.Läkemedel().length > 0) {
            for (var i = 0; i < vm.$$.Läkemedel().length; i++) {
                var lkm = vm.$$.Läkemedel()[i]
                resetValidation(lkm.LM_ablatiotestisdatum);
                lkm.LM_ablatiotestisdatum.rcc.validation.add(new RCC.Validation.Tests.NotMissing(lkm.LM_ablatiotestisdatum, {
                    ignoreMissingValues: true
                }));
                lkm.LM_ablatiotestisdatum.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(lkm.LM_ablatiotestisdatum, {
                    ignoreMissingValues: true
                }));

                if (!lkm.LM_ablatiotestis() && !lkm.LM_studielkm()) {
                    required(lkm.LM_substans);
                } else {
                    notRequiredAndClear(lkm.LM_substans);
                }

                if (!lkm.LM_ablatiotestis()) {
                    notRequiredAndClear(lkm.LM_ablatiotestisdatum);
                } else {
                    required(lkm.LM_ablatiotestisdatum);
                }

                if (!lkm.LM_studielkm()) {
                    notRequiredAndClear(lkm.LM_studielkmtxt);
                } else {
                    required(lkm.LM_studielkmtxt);
                }

                var tabell = lkm.$$.LM_Insättning();
                for (var j = 0; j < tabell.length; j++) {

                    var undertabellHand = tabell[j].$$.LMI_Händelse();
                    for (var n = 0; n < undertabellHand.length; n++) {

                        resetValidation(undertabellHand[n].LMIH_handelsedatum);

                        addNotAfterDeathValidation(undertabellHand[n].LMIH_handelsedatum);
                        undertabellHand[n].LMIH_handelsedatum.rcc.validation.add(new RCC.Validation.Tests.NotMissing(undertabellHand[n].LMIH_handelsedatum, {
                            ignoreMissingValues: true
                        }));
                        undertabellHand[n].LMIH_handelsedatum.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(undertabellHand[n].LMIH_handelsedatum, {
                            ignoreMissingValues: true
                        }));

                        // Ta bort "Planerat stopp" i orsak till utsättning
                        removeValueInList(undertabellHand[n].LMIH_handelseorsak, '1');

                        if (!undertabellHand[n].LMIH_handelseorsak() || undertabellHand[n].LMIH_handelseorsak().value != 4) {
                            notRequiredAndClear(undertabellHand[n].LMIH_handelseorsakann);
                        } else {
                            required(undertabellHand[n].LMIH_handelseorsakann);
                        }

                    }

                    var undertabellUt = tabell[j].$$.LMI_Utsättning();
                    for (var n = 0; n < undertabellUt.length; n++) {

                        resetValidation(undertabellUt[n].LMIU_utsattningsdatum);

                        addNotAfterDeathValidation(undertabellUt[n].LMIU_utsattningsdatum);

                        notRequired(undertabellUt[n].LMIU_antalbeh);

                        undertabellUt[n].LMIU_utsattningsdatum.rcc.validation.add(new RCC.Validation.Tests.NotFutureDate(undertabellUt[n].LMIU_utsattningsdatum, {
                            ignoreMissingValues: true
                        }));

                        if (!undertabellUt[n].LMIU_utsattningsorsak() || undertabellUt[n].LMIU_utsattningsorsak().value != 4) {
                            notRequiredAndClear(undertabellUt[n].LMIU_utsattningsorsakann);
                        } else {
                            required(undertabellUt[n].LMIU_utsattningsorsakann);
                        }

                    }

                }

            }
        }
    });

    // Biobanksprov validering
    ko.computed(function () {
        if (vm.$$.Biobank().length > 0) {
            for (var i = 0; i < vm.$$.Biobank().length; i++) {

                resetValidation(vm.$$.Biobank()[i].BB_biobanksdatum);
                vm.$$.Biobank()[i].BB_biobanksdatum.rcc.validation.add(new RCC.Validation.Tests.NotMissing(vm.$$.Biobank()[i].BB_biobanksdatum, {
                    ignoreMissingValues: true
                }));
                addNotAfterDeathValidation(vm.$$.Biobank()[i].BB_biobanksdatum);
            }
        }
    });

    /*

        Nuvarande koncept för validering:

        Testa används för att kolla varje "rad" och returnerar ett värde, sant eller falskt, om
        det finns fel eller inte på den rad som undersöks.
        Om raden har en undertabell måste även denna testas, varje underrad till varje rad testas
        rekursivt innuti funktionen.
        Har ett fel upptäckts visas detta med röd text i HTML koden för "raden" i fråga.

        För att hålla reda på om det finns något fel i någon av alla rader i en tab, och i så fall
        göra texten	röd på tabben, finns en beräknad variabel för varje tabell tillhörande en tab
        och ännu en variabel som kollar alla beräknade variabler som hör till en tab.
        Om fel hittas sätts variabeln (tillhörande tabellen med raden med fel i) till true, annars
        är den false. Om det finns fel visas detta med röd färg på texten för tabben i fråga.

    */

    vm.validate = ko.observable(false);

    vm.validateData = function () {
        vm.validate(true);

        if (vm.besokFelFinns() || vm.tab5FelFinns() || vm.tab6FelFinns() || vm.tab7FelFinns() || vm.labbprovFelFinns() || vm.tab9FelFinns() || vm.tab10FelFinns() || vm.tab1FelFinns()) {
            getInfoDialog("Fel funna i formuläret", "Det saknas information eller finns fel i de inmatade uppgifterna i formuläret. De flikar och rader som innehåller fel har rödmarkerats.");
            return false;
        } else {
            if (vm.ejGjordBedömning()) {
                if (!warningDialog("Ett eller flera besök väntar på att få en sammantagen klinisk bedömning. Är du säker på att du vill spara utan att fylla i dessa?")) {
                    return false;
                }
            }
            if (vm.ejIfylldRadBed()) {
                if (!warningDialog("Det saknas radiologisk bedömning på en eller flera bilddiagnostiker. Är du säker på att du vill spara utan att fylla i dessa?")) {
                    return false;
                }
            }
        }

        vm.actionsToDoBeforePost();

    };

    if (!vm.externalInca())
        inca.on('validation', vm.validateData);

    vm.actionsToDoBeforePost = function () {
        if (vm.formHasChanged()) {
            var d = new Date();
            var datum = d.getFullYear() + "-" + (('' + d.getMonth() + 1).length < 2 ? '0' : '') + (d.getMonth() + 1) + "-" + (('' + d.getDate()).length < 2 ? '0' : '') + d.getDate();
            vm.U_datum(datum);
        }
    };

    /**
     *
     *	Funktionen testar en rad och returnerar true eller false beroende på om det finns något fel i just den raden i tabellen.
        *	Funktionen tar hänsyn till tabeller med undertabeller och dess rader och loopar igenom alla ingående poster rekursivt.
        *	Denna funktion ger enbart svar på om en specifik rad har något fel, inte om någon av alla rader i en flik har ett fel.
        *
        */
    vm.testaRad = function (row) {

        if (vm.validate()) {
            var errors = false;

            // Loopa genom alla radens "prop" (funktioner/variabler)
            for (prop in row) {
                // Om det innehåller en .rcc är det en variabel
                if (row[prop].rcc) {
                    // Om variabeln har något fel i sig
                    if (row[prop].rcc.validation.errors().length > 0) {
                        row[prop].rcc.accessed(true);
                        errors = true;
                    }
                    // Om prop är "$$" kan tabellen ha undertabeller med variabler som också måste testas
                } else if (prop == '$$') {
                    var property = prop;
                    // Loopa igenom undertabellerna
                    for (tabell in row[property]) {
                        var undertabell = tabell;
                        // Om det är en undertabell som har något innehåll
                        if (row[property][undertabell]().length > 0) {
                            for (var i = 0; i < row[property][undertabell]().length; i++) {
                                // Testa dessa också och se om de innehåller fel
                                if (vm.testaRad(row[property][undertabell]()[i])) {
                                    errors = true;
                                }
                            }
                        }
                    }
                    // OM det inte är en inca-variabel utan någon funktion eller annat ska inget göras
                }
            }

            if (errors) {
                return true;
            }
            return false;
        } else
            return false;
    };

    vm.avvaktarBedömning = function (row) {
        var avvaktar = false;

        if (vm.validate()) {

            if (row.B_klinbed() && row.B_klinbed().value == '3') {
                avvaktar = true;
            }
        }

        return avvaktar;
    };

    vm.saknarRadBed = function (row) {
        var saknar = false;

        if (vm.validate()) {

            if (!row.BD_radbed()) {
                saknar = true;
            }
        }

        return saknar;
    };

    /**
     *
     *	Validering för flikarna
        *
        */

    // Tab-4 : Besök
    vm.besokFelFinns = ko.computed(function () {
        if (vm.validate()) {
            // Loopa igenom tabellens rader och eventuella undertabeller och deras rader
            for (var i = 0; i < vm.$$.Besök().length; i++) {
                var row = vm.$$.Besök()[i];
                // Om en rad har en prop som är .rcc är det en variabel
                for (prop in row) {
                    // Om variabeln har fel i sig, errors().length > 0, avbryt loop och returnera true
                    if (row[prop].rcc && row[prop].rcc.validation.errors().length > 0) {
                        return true;
                    }
                }
            }
            // Om ingen variabel har fel i sig returneras false
            return false;
        } else
            return false;
    });


    // Tab-5 : Bilddiagnos
    vm.bildDiaFelFinns = ko.computed(function () {
        if (vm.validate()) {
            // Loopa igenom tabellens rader och eventuella undertabeller och deras rader
            for (var i = 0; i < vm.$$.Bilddiagnostik().length; i++) {
                var row = vm.$$.Bilddiagnostik()[i];
                // Om en rad har en prop som är .rcc är det en variabel
                for (prop in row) {
                    // Om variabeln har fel i sig, errors().length > 0, avbryt loop och returnera true
                    if (row[prop].rcc && row[prop].rcc.validation.errors().length > 0) {
                        return true;
                    }
                }
            }
            // Om ingen variabel har fel i sig returneras false
            return false;
        } else
            return false;
    });

    vm.metastasFelFinns = ko.computed(function () {
        if (vm.validate()) {
            // Loopa igenom tabellens rader och eventuella undertabeller och deras rader
            for (var i = 0; i < vm.$$.Bilddiagnostik().length; i++) {
                var undertabell = vm.$$.Bilddiagnostik()[i].$$.BD_Metastas();
                // Loopa igenom undertabellens rader
                for (var n = 0; n < undertabell.length; n++) {
                    var row = undertabell[n];
                    // Om en rad har en prop som är .rcc är det en variabel
                    for (property in row) {
                        // Om variabeln har fel i sig, errors().length > 0, avbryt loop och returnera true
                        if (row[property].rcc && row[property].rcc.validation.errors().length > 0) {
                            return true;
                        }
                    }
                }
            }
            // Om ingen variabel har fel i sig returneras false
            return false;
        } else
            return false;
    });

    // Beräknad variabel för flera tabeller i samma flik
    vm.tab5FelFinns = ko.computed(function () {
        if (vm.validate()) {
            // Om det finns ett fel, antingen bland variablerna i bilddiagnostik eller bland metastaser
            if (vm.metastasFelFinns() || vm.bildDiaFelFinns())
                return true;

            return false;
        } else
            return false;
    });


    // Tab-6 : Strålbehandling
    vm.stralBehFelFinns = ko.computed(function () {
        if (vm.validate()) {
            for (var i = 0; i < vm.$$.Strålbehandling().length; i++) {
                var row = vm.$$.Strålbehandling()[i];
                for (prop in row) {
                    if (row[prop].rcc && row[prop].rcc.validation.errors().length > 0) {
                        return true;
                    }
                }
            }
            return false;
        } else
            return false;
    });

    vm.gruppFelFinns = ko.computed(function () {
        if (vm.validate()) {
            for (var i = 0; i < vm.$$.Strålbehandling().length; i++) {
                var undertabell = vm.$$.Strålbehandling()[i].$$.SB_Grupp();
                for (var n = 0; n < undertabell.length; n++) {
                    var row = undertabell[n];
                    for (property in row) {
                        if (row[property].rcc && row[property].rcc.validation.errors().length > 0) {
                            return true;
                        }
                    }
                }
            }
            return false;
        } else
            return false;
    });

    vm.lokalFelFinns = ko.computed(function () {
        if (vm.validate()) {
            for (var i = 0; i < vm.$$.Strålbehandling().length; i++) {
                for (var n = 0; n < vm.$$.Strålbehandling()[i].$$.SB_Grupp().length; n++) {
                    var undertabell = vm.$$.Strålbehandling()[i].$$.SB_Grupp()[n].$$.SBG_Lokal();
                    for (var m = 0; m < undertabell.length; m++) {
                        var row = undertabell[m];
                        for (property in row) {
                            if (row[property].rcc && row[property].rcc.validation.errors().length > 0) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        } else
            return false;
    });

    vm.tab6FelFinns = ko.computed(function () {
        if (vm.validate()) {
            if (vm.stralBehFelFinns() || vm.gruppFelFinns() || vm.lokalFelFinns())
                return true;

            return false;
        } else
            return false;
    });


    // Tab-7 : SRE
    vm.SREFelFinns = ko.computed(function () {
        if (vm.validate()) {
            for (var i = 0; i < vm.$$.SRE().length; i++) {
                var row = vm.$$.SRE()[i];
                for (prop in row) {
                    if (row[prop].rcc && row[prop].rcc.validation.errors().length > 0) {
                        return true;
                    }
                }
            }
            return false;
        } else
            return false;
    });

    vm.SREHändelseFelFinns = ko.computed(function () {
        if (vm.validate()) {
            for (var i = 0; i < vm.$$.SRE().length; i++) {
                var undertabell = vm.$$.SRE()[i].$$.SRE_Händelse();
                for (var n = 0; n < undertabell.length; n++) {
                    var row = undertabell[n];
                    for (property in row) {
                        if (row[property].rcc && row[property].rcc.validation.errors().length > 0) {
                            return true;
                        }
                    }
                }
            }
            return false;
        } else
            return false;
    });

    vm.tab7FelFinns = ko.computed(function () {
        if (vm.validate()) {
            if (vm.SREFelFinns() || vm.SREHändelseFelFinns())
                return true;

            return false;
        } else
            return false;
    });


    // Tab-8 : Labbprov
    vm.labbprovFelFinns = ko.computed(function () {
        if (vm.validate()) {
            for (var i = 0; i < vm.$$.Labbprov().length; i++) {
                var row = vm.$$.Labbprov()[i];
                for (prop in row) {
                    if (row[prop].rcc && row[prop].rcc.validation.errors().length > 0) {
                        return true;
                    }
                }
            }
            return false;
        } else
            return false;
    });


    // Tab-9 : Läkemedel
    vm.lakemFelFinns = ko.computed(function () {
        if (vm.validate()) {
            for (var i = 0; i < vm.$$.Läkemedel().length; i++) {
                var row = vm.$$.Läkemedel()[i];
                for (prop in row) {
                    if (row[prop].rcc && row[prop].rcc.validation.errors().length > 0) {
                        return true;
                    }
                }
            }
            return false;
        } else
            return false;
    });

    vm.lakemInsattFelFinns = ko.computed(function () {
        if (vm.validate()) {
            for (var i = 0; i < vm.$$.Läkemedel().length; i++) {
                var undertabell = vm.$$.Läkemedel()[i].$$.LM_Insättning();
                for (var n = 0; n < undertabell.length; n++) {
                    var row = undertabell[n];
                    for (property in row) {
                        if (row[property].rcc && row[property].rcc.validation.errors().length > 0) {
                            return true;
                        }
                    }
                }
            }
            return false;
        } else
            return false;
    });

    vm.lakemInsattHandFelFinns = ko.computed(function () {
        if (vm.validate()) {
            for (var i = 0; i < vm.$$.Läkemedel().length; i++) {
                for (var n = 0; n < vm.$$.Läkemedel()[i].$$.LM_Insättning().length; n++) {
                    var undertabell = vm.$$.Läkemedel()[i].$$.LM_Insättning()[n].$$.LMI_Händelse();
                    for (var m = 0; m < undertabell.length; m++) {
                        var row = undertabell[m];
                        for (property in row) {
                            if (row[property].rcc && row[property].rcc.validation.errors().length > 0) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        } else
            return false;
    });

    vm.lakemInsattUtFelFinns = ko.computed(function () {
        if (vm.validate()) {
            for (var i = 0; i < vm.$$.Läkemedel().length; i++) {
                for (var n = 0; n < vm.$$.Läkemedel()[i].$$.LM_Insättning().length; n++) {
                    var undertabell = vm.$$.Läkemedel()[i].$$.LM_Insättning()[n].$$.LMI_Utsättning();
                    for (var m = 0; m < undertabell.length; m++) {
                        var row = undertabell[m];
                        for (property in row) {
                            if (row[property].rcc && row[property].rcc.validation.errors().length > 0) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        } else
            return false;
    });

    vm.tab9FelFinns = ko.computed(function () {
        if (vm.validate()) {
            if (vm.lakemFelFinns() || vm.lakemInsattFelFinns() || vm.lakemInsattHandFelFinns() || vm.lakemInsattUtFelFinns())
                return true;

            return false;
        } else
            return false;
    });

    //  Tab-10 - Biobanksprov
    vm.tab10FelFinns = ko.computed(function () {
        if (vm.validate()) {
            // Loopa igenom tabellens rader och eventuella undertabeller och deras rader
            for (var i = 0; i < vm.$$.Biobank().length; i++) {
                var row = vm.$$.Biobank()[i];
                // Om en rad har en prop som är .rcc är det en variabel
                for (prop in row) {
                    // Om variabeln har fel i sig, errors().length > 0, avbryt loop och returnera true
                    if (row[prop].rcc && row[prop].rcc.validation.errors().length > 0) {
                        return true;
                    }
                }
            }
            // Om ingen variabel har fel i sig returneras false
            return false;
        } else
            return false;
    });

    //  Tab-1 - Basdata
    vm.tab1FelFinns = ko.computed(function () {
        if (vm.validate()) {
            // Kolla på alla variabler som ligger under bas-data-fliken
            if (vm.U_diadat() && vm.U_diadat.rcc.validation.errors().length > 0) {
                return true;
            }
            if (vm.U_spsa() && vm.U_spsa.rcc.validation.errors().length > 0) {
                return true;
            }
            if (vm.U_tstadDia() && vm.U_tstadDia.rcc.validation.errors().length > 0) {
                return true;
            }
            if (vm.U_nstadDia() && vm.U_nstadDia.rcc.validation.errors().length > 0) {
                return true;
            }
            if (vm.U_mstadDia() && vm.U_mstadDia.rcc.validation.errors().length > 0) {
                return true;
            }
            if (vm.U_gleassaGrad1() && vm.U_gleassaGrad1.rcc.validation.errors().length > 0) {
                return true;
            }
            if (vm.U_gleassaGrad2() && vm.U_gleassaGrad2.rcc.validation.errors().length > 0) {
                return true;
            }
            if (vm.U_gleassa() && vm.U_gleassa.rcc.validation.errors().length > 0) {
                return true;
            }
            if (vm.U_AMstartdatum() && vm.U_AMstartdatum.rcc.validation.errors().length > 0) {
                return true;
            }
            if (vm.U_AMslutdatum() && vm.U_AMslutdatum.rcc.validation.errors().length > 0) {
                return true;
            }
            if (vm.U_rtTyp() && vm.U_rtTyp.rcc.validation.errors().length > 0) {
                return true;
            }
            if (vm.U_rtKurStartdat() && vm.U_rtKurStartdat.rcc.validation.errors().length > 0) {
                return true;
            }  
            if (vm.U_rpOpdat() && vm.U_rpOpdat.rcc.validation.errors().length > 0) {
                return true;
            }
            if (vm.U_rtPostopSlutdat() && vm.U_rtPostopSlutdat.rcc.validation.errors().length > 0) {
                return true;
            }     
            if (vm.U_psadatum() && vm.U_psadatum.rcc.validation.errors().length > 0) {
                return true;
            }
            if (vm.U_hormonstartdatum() && vm.U_hormonstartdatum.rcc.validation.errors().length > 0) {
                return true;
            }
            if (vm.U_crpcdatum() && vm.U_crpcdatum.rcc.validation.errors().length > 0) {
                return true;
            }
            if (vm.U_metastasdatum() && vm.U_metastasdatum.rcc.validation.errors().length > 0) {
                return true;
            }
            if (vm.U_pallHemSjkvrd() && vm.U_pallHemSjkvrd.rcc.validation.errors().length > 0) {
                return true;
            }
            if (vm.U_datagranskat() && vm.U_datagranskat.rcc.validation.errors().length > 0) {
                return true;
            }
            // Om ingen variabel har fel i sig returneras false
            return false;
        } else
            return false;
    });

    ko.computed(function () {
        if (vm.U_gleassaGrad1() && vm.U_gleassaGrad1.rcc.validation.errors().length == 0 &&
            vm.U_gleassaGrad2() && vm.U_gleassaGrad2.rcc.validation.errors().length == 0){
            vm.U_gleassa(parseInt(vm.U_gleassaGrad1()) + parseInt(vm.U_gleassaGrad2()));
        }else{
            vm.U_gleassa(undefined);
        }
    });


    /*vm.hormonbehandlingStartad = ko.computed(function () {
        var isStarted = false;
        
        ko.utils.arrayForEach(vm.$$.Läkemedel(), function(lm) {
            if (!isStarted && lm.LM_typ() && lm.LM_typ().value == '1') {
                if (lm.LM_ablatiotestis() && lm.LM_ablatiotestisdatum()){
                    isStarted=true;
                } 
                else{
                    ko.utils.arrayForEach(lm.$$.LM_Insättning(), function (insattning) {
                        if (vm.firstInsEventDate(insattning))
                            isStarted=true;
                    });
                }
            }
        });
        return isStarted;
    });*/


    vm.ejGjordBedömning = ko.computed(function () {
        if (vm.validate()) {
            for (var i = 0; i < vm.$$.Besök().length; i++) {
                if (vm.$$.Besök()[i].B_klinbed() && vm.$$.Besök()[i].B_klinbed().value == 3) {
                    return true;
                }
            }
            return false;
        } else
            return false;
    });


    vm.ejIfylldRadBed = ko.computed(function () {
        if (vm.validate()) {
            for (var i = 0; i < vm.$$.Bilddiagnostik().length; i++) {
                if (!vm.$$.Bilddiagnostik()[i].BD_radbed()) {
                    return true;
                }
            }
            return false;
        } else
            return false;
    });


    vm.patientAge = ko.computed(function () {
        var now;
        if (vm.$env._DATEOFDEATH != "") {
            now = new Date(vm.$env._DATEOFDEATH).getTime();
        } else {
            now = new Date().getTime();
        }
        var birthday = new Date(vm.$env._PERSNR.slice(0, 4), (vm.$env._PERSNR.slice(4, 6) - 1), vm.$env._PERSNR.slice(6, 8));
        var ageDifMs = now - birthday.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    });

    $('#startpagebtn').click(function () {
        parent.document.location.href = 'https://rcc.incanet.se/';
    });


    $('body').parents().on('keydown', function (e) {
        if (e.which === 8 && !$(e.target).is("input, textarea")) {
            e.preventDefault();
        }
    });


    $(window).bind('beforeunload', function () {
        if (vm.formHasChanged()) {
            return 'Det finns osparade förändringar i formuläret. Är du säker på att du vill lämna sidan?';
        }
    });

    $('input[name=scaletype]:first').prop("checked", true);

    $('input[name=scaletype]').change(function () {
        var self = this;
        if (self.value == 'linear') {
            $('#overview-highchart').highcharts().yAxis[5].update({
                type: self.value,
                tickInterval: null,
                min: 0
            });
        }
        if (self.value == 'logarithmic') {
            $('#overview-highchart').highcharts().yAxis[5].update({
                type: self.value,
                tickInterval: 1,
                min: 0.01
            });
        }
    });

    vm.zoomOut = function () {
        if (vm.overviewHighChart) {
            vm.overviewHighChart.zoomOut();
            $('#zoomOutBtn').addClass('hiding');
        }
    };

    // anropa förberedande åtrgärder innan formulärets binds
    vm.runFunctionsOnLoad();

    // applicera knockout bindningar för vymodell
    ko.applyBindings(vm);


    /* BERÄKNANDE FUNKTIONER */

    /**
     *  Kör "afterload" funktioner när formuläret är initializerat.
     */
    var init = false;
    vm.init = ko.computed(function () {
        if (requestCount() == 0 && !init) {
            vm.runFunctionsAfterLoad();
            init = true;
            setTimeout(function () {
                vm.init.dispose();
            }, 10);
        }
    });
});
